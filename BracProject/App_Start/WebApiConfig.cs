﻿using System.Web.Http;

namespace BracProject
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

			// login
            //config.Routes.MapHttpRoute(
            //  name: "Login",
            //  routeTemplate: "login/{id}",
            //  defaults: new { controller = "Login", id = RouteParameter.Optional }
            //);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
