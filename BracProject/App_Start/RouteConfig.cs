﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BracProject
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "WebStaffReport",
                url: "staffreport/{action}/{id}",
                defaults: new { controller = "WebStaffReport", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebDivision",
                url: "division/{action}/{id}",
                defaults: new { controller = "WebDivision", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "WebDistrict",
                url: "district/{action}/{id}",
                defaults: new { controller = "WebDistrict", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "WebUpazila",
                url: "upazila/{action}/{id}",
                defaults: new { controller = "WebUpazila", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "WebUnion",
                url: "union/{action}/{id}",
                defaults: new { controller = "WebUnion", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "WebVillage",
                url: "village/{action}/{id}",
                defaults: new { controller = "WebVillage", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "WebWard",
               url: "ward/{action}/{id}",
               defaults: new { controller = "WebWard", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "WebPara",
               url: "para/{action}/{id}",
               defaults: new { controller = "WebPara", action = "Index", id = UrlParameter.Optional }
           );



            routes.MapRoute(
                name: "ReportMemberAssign",
                url: "report_1/{action}/{id}",
                defaults: new { controller = "ReportMemberAssign", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebAssignSchool",
                url: "school_assign/{action}/{id}",
                defaults: new { controller = "WebAssignSchool", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebSchoolList",
                url: "school_list/{action}/{id}",
                defaults: new { controller = "WebSchoolList", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebAssignSs",
                url: "assign_ss/{action}/{id}",
                defaults: new { controller = "WebAssignSs", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebAssignSk",
                url: "assign_sk/{action}/{id}",
                defaults: new { controller = "WebAssignSk", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebAssignPo",
                url: "assign_po/{action}/{id}",
                defaults: new { controller = "WebAssignPo", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebMember",
                url: "member_list/{action}/{id}",
                defaults: new { controller = "WebMember", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebChildren",
                url: "children_list/{action}/{id}",
                defaults: new { controller = "WebChildren", action = "Index", id = UrlParameter.Optional }
            );
           

            routes.MapRoute(
                name: "WebPo",
                url: "po/{action}/{id}",
                defaults: new { controller = "WebPo", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebSk",
                url: "sk/{action}/{id}",
                defaults: new { controller = "WebSk", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebSs",
                url: "ss/{action}/{id}",
                defaults: new { controller = "WebSs", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebTeam",
                url: "team_mang/{action}/{id}",
                defaults: new { controller = "WebTeam", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "WebReport",
                url: "report/{action}/{id}",
                defaults: new { controller = "WebReport", action = "Index", id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "UserManagement",
                url: "user_management/{action}/{id}",
                defaults: new { controller = "WebUserManagement", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Dashboard",
                url: "dashboard/{action}/{id}",
                defaults: new { controller = "WebDashboard", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Login",
                url: "login/{action}/{id}",
                defaults: new { controller = "WebLogin", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Logout",
                url: "logout/{action}/{id}",
                defaults: new { controller = "WebLogin", action = "Logout", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "Default",
              url: "{controller}/{action}/{id}",
              defaults: new { controller = "WebLogin", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "WebAssignAll",
             url: "WebAssignAll/{action}/{id}",
             defaults: new { controller = "WebAssignAll", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
            name: "WebDewarmingReport",
            url: "WebDewarmingReport/{action}/{id}",
            defaults: new { controller = "WebDewarmingReport", action = "Index", id = UrlParameter.Optional }
          );


        }
    }
}
