﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BracProject.Models
{
    [Table("DIVISION")]
    public class DivisionModel
    {
		[Key]
		public int ID { get; set; }
		public string NAME { get; set; }
        public string BANGLANAME { get; set; }
        public string BBSCODE { get; set; }
        [Display(Name = "Status")]
        public int ISACTIVE { get; set; }
    }
}
