﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("assign_list_po")]
    public class AssignListPoModel
    {

        [Key]
        public int id { get; set; }

        [ForeignKey("AssignPo")]
        public int po_list_id { get; set; }

        [ForeignKey("Upazila")]
        [Display(Name = "Upazila Name*")]
        public int upz_id { get; set; }

        [ForeignKey("Union")]
        [Display(Name = "Union Name*")]
        public int union_id { get; set; }


        [Display(Name = "Ward Name*")]
        public int ward_id { get; set; }

        [ForeignKey("Village")]
        [Display(Name = "Village Name*")]
        public int village_id { get; set; }
        [Display(Name = "Para")]
        public int para_id { get; set; }

        public int status { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime created_at { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime updated_at { get; set; }

        public virtual AssignPoModel AssignPo { get; set; }
        public virtual UpazilaModel Upazila { get; set; }
        public virtual UnionModel Union { get; set; }
        public virtual VillageModel Village { get; set; }


    }
}
