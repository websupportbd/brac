﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("union")]
    public class UnionModel
    {
        public int id { get; set; }

        [ForeignKey("Upazila")]
        public int upz_id { get; set; }
        [Display(Name = "Status")]
        public int ISACTIVE { get; set; }

        public string name { get; set; }

        virtual public UpazilaModel Upazila { get; set; }
    }
}
