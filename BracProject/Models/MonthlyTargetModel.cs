﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("monthly_target")]
    public class MonthlyTargetModel
    {
        public int id { get; set; }
        public int member_type { get; set; }

        public DateTime month_name { get; set; }
        public string target { get; set; }
    }
}