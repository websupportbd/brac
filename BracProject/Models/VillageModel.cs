﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("village_list")]
    public class VillageModel
    {
        public int id { get; set; }
        [ForeignKey("Union")]
        public int union_id { get; set; }
        public string name { get; set; }
        [Display(Name = "Status")]
        public int ISACTIVE { get; set; }

        virtual public UnionModel Union { get; set; }
    }
}
