﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace BracProject.Models
{
	[Table("form_list")]
    public class InsFormModel
    {
		[Key]
		public int ID { get; set; }

        [Display(Name = "Template Name*")]
        [Required(ErrorMessage = "Template name is required.", AllowEmptyStrings = false)]
		public string NAME_ORG { get; set; }

		public string NAME_ENC { get; set; }
		public string JSON { get; set; }
        public string RAW_CODE { get; set; }
        public int IS_GENERIC { get; set; }
        public int IS_DEFAULT { get; set; }
        public int STATUS { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CREATED_AT { get; set; }
        
    }
}
