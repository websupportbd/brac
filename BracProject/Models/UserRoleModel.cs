﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BracProject.Models
{
    [Table("user_roles")]
    public class UserRoleListModel
    {
        [Key]
        public int id { get; set; }
        public int user_id { get; set; }
        public int module_id { get; set; }
        public int u_view { get; set; }
        public int u_add { get; set; }
        public int u_edit { get; set; }
        public int u_delete { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
    }

}
