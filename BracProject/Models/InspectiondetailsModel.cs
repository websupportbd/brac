﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace BracProject.Models
{
	[Table("inspection_details")]
	public class InspectiondetailsModel
	{
		[Key]
		public int ID { get; set; }
		public int FORM_ID { get; set; }
		public int INSPECTION_ID { get; set; }      
        public string JSON_DATA { get; set; }
        public string RAW_CODE { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CREATED_AT { get; set; }
	}
}
