﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("assign_school")]
    public class AssignSchoolModel
    {
        [Key]
        public int id { get; set; }

        [ForeignKey("PO")]
        [Display(Name = "PO Name*")]
        [Required(ErrorMessage = "Please select PO name", AllowEmptyStrings = false)]
        public int po_id { get; set; }

        [ForeignKey("School")]
        [Display(Name = "School Name*")]
        public int school_id { get; set; }

        public string ids { get; set; }

        public int status { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime created_at { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime updated_at { get; set; }

        public virtual MemberModel PO { get; set; }
        public virtual SchoolListModel School { get; set; }



    }
}
