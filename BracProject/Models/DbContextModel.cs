﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace BracProject.Models
{
	public class MySqlCon : DbContext
    {
        public MySqlCon() : base(nameOrConnectionString: "ConString")
        {
           (this as IObjectContextAdapter).ObjectContext.ContextOptions.UseCSharpNullComparisonBehavior = true;
        }

        public virtual DbSet<LoginModel> Login_Data { get; set; }      
		public virtual DbSet<InboxModel> Inbox_Data { get; set; }
		public virtual DbSet<DivisionModel> Division_Data { get; set; }
		public virtual DbSet<DistrictModel> District_Data { get; set; }
		public virtual DbSet<UpazilaModel> Upazila_Data { get; set; }
		public virtual DbSet<VersionModel> Version_Data { get; set; }
		
		public virtual DbSet<ProjectModel> Project_Data { get; set; }
		public virtual DbSet<InsFormModel> InsForm_Data { get; set; }
		public virtual DbSet<InspectiondataModel> Inspectiondata_Data { get; set; }
		public virtual DbSet<InspectiondetailsModel> Inspectiondetails_Data { get; set; }
        
		public virtual DbSet<ChildrenListModel> ChildrenList_Data { get; set; }
		public virtual DbSet<SchoolListModel> SchoolList_Data { get; set; }
		public virtual DbSet<ClassListModel> ClassList_Data { get; set; }
		public virtual DbSet<DrinkingWaterListModel> DrinkingWaterList_Data { get; set; }

		public virtual DbSet<DoseListModel> DoseList_Data { get; set; }
		public virtual DbSet<UpdateCTableModel> UpdateTable_Data { get; set; }

        public virtual DbSet<POModel> PO_Data { get; set; }
        public virtual DbSet<SKModel> SK_Data { get; set; }
        public virtual DbSet<SSModel> SS_Data { get; set; }
        public virtual DbSet<VillageModel> Village_Data { get; set; }
        public virtual DbSet<MemberModel> Member_Data { get; set; }
        public virtual DbSet<MemberTypeModel> MemberType_Data { get; set; }
        public virtual DbSet<AssignPoModel> AssignPo_Data { get; set; }
        public virtual DbSet<AssignListPoModel> AssignListPo_Data { get; set; }
        public virtual DbSet<AssignSkModel> AssignSk_Data { get; set; }
        public virtual DbSet<AssignListSkModel> AssignListSk_Data { get; set; }
        public virtual DbSet<AssignListSSModel> AssignListSS_Data { get; set; }
        public virtual DbSet<AssignSsModel> AssignSs_Data { get; set; }
        public virtual DbSet<UnionModel> Union_Data { get; set; }
        public virtual DbSet<AssignSchoolModel> AssignSchool_Data { get; set; }
        public virtual DbSet<WardListModel> Ward_Data { get; set; }
        public virtual DbSet<ParaListModel> Para_Data { get; set; }
        public virtual DbSet<MonthlyTargetModel> Monthly_Target { get; set; }

        public virtual DbSet<ModuleListModel> ModuleList_Data { get; set; }
        public virtual DbSet<UserRoleListModel> UserRoleList_Data { get; set; }

        public virtual DbSet<VisitListModel> Visit_Data { get; set; }

        public virtual DbSet<ChildrenLogModel> ChildrenLog_Data { get; set; }
    }

}
