﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
	[Table("dose_list")]
    public class DoseListModel
    {
		[Key]
        public int id { get; set; }

		[ForeignKey("ChildrenList")]
		public int children_id { get; set; }

		public string union_id { get; set; }
        public string taken_dose_place { get; set; }
       public int side_effect { get; set; }

        public DateTime date { get; set; }

		public virtual ChildrenListModel ChildrenList { get; set; }
    }
}
