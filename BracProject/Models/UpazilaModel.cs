﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BracProject.Models
{
	[Table("UPAZILA")]
	public class UpazilaModel
	{
		[Key]
		public int ID { get; set; }

        [ForeignKey("District")]
        public int DISTRICTID { get; set; }
        [Display(Name = "Status")]
        public int ISACTIVE { get; set; }
        public string SUB_DISTRICT { get; set; }
        public string NAME { get; set; }
		public string BANGLANAME { get; set; }
		public string BBSCODE { get; set; }

        virtual public DistrictModel District { get; set; }

    }
}
