﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace BracProject.Models
{
	[Table("update_table")]
    public class UpdateCTableModel
    {
		[Key]
        public int id { get; set; }      
		public string title { get; set; }
        public string details { get; set; }
    }
}
