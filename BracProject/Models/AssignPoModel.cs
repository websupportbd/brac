﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("assign_po")]
    public class AssignPoModel
    {

        [Key]
        public int id { get; set; }

        [ForeignKey("PO")]
        [Display(Name = "PO Name")]
        public int po_id { get; set; }

        [ForeignKey("Division")]
        [Display(Name = "Division Name*")]
        public int div_id { get; set; }

        [ForeignKey("District")]
        [Display(Name = "District Name*")]
        public int dis_id { get; set; }

       
        [Display(Name = "Upazila*")]      
        public string uz_ids { get; set; }


      
        [Display(Name = "Union*")]
        public string ui_ids { get; set; }

     
        [Display(Name = "Ward*")]
        public string wa_ids { get; set; }


        public string vi_ids { get; set; }
       
        public string pa_ids { get; set; }

        public int status { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime created_at { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime updated_at { get; set; }

        public virtual MemberModel PO { get; set; }
        public virtual DivisionModel Division { get; set; }
        public virtual DistrictModel District { get; set; }
       // public virtual UpazilaModel Upazila { get; set; }
        //public virtual UnionModel Union { get; set; }
        //public virtual WardListModel Ward { get; set; }




    }
}
