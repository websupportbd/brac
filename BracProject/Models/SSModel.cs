﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace BracProject.Models
{
	[Table("ss_list")]
    public class SSModel
    {
        public int id { get; set; }

        [ForeignKey("SK")]
        [Display(Name = "SK Name*")]
        [Required(ErrorMessage = "Please select SK name", AllowEmptyStrings = false)]
        public int sk_id { get; set; }

        [ForeignKey("Village")]
        [Display(Name = "Village Name*")]
        [Required(ErrorMessage = "Please select Village name", AllowEmptyStrings = false)]
        public int village_id { get; set; }

        [Display(Name = "Name*")]
        [Required(ErrorMessage = "SS name is required.", AllowEmptyStrings = false)]
        public string name { get; set; }

        public int status { get; set; }

        public virtual SKModel SK { get; set; }
        public virtual VillageModel Village { get; set; }
    }
}
