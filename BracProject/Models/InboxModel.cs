﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace BracProject.Models
{
	[Table("INBOX")]
	public class InboxModel
	{
		[Key]
		public int ID { get; set; }
		public string TOUSERNAME { get; set; }
		public string FORMUSERNAME { get; set; }
		public string FORMUSEREMAIL { get; set; }
        public int INSPECTIONID { get; set; }
        public int READSTATUS { get; set; }

		[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
		public DateTime CREATED_AT { get; set; }
	}
}
