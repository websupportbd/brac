﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BracProject.Models
{
    [Table("ward_list")]
    public class WardListModel
    {
        public int id { get; set; }

        [Display(Name = "Upazila*")]
        [ForeignKey("Upazila")]
        public int upz_id { get; set; }
        [Display(Name = "Status")]
        public int ISACTIVE { get; set; }

        public string name { get; set; }

        virtual public UpazilaModel Upazila { get; set; }
    }
}