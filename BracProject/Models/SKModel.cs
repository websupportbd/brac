﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace BracProject.Models
{
	
	[Table("sk_list")]
    public class SKModel
    {
		[Key]
		public int id { get; set; }

        [ForeignKey("PO")]
        [Display(Name = "PO Name*")]
        [Required(ErrorMessage = "Please select PO name", AllowEmptyStrings = false)]
        public int po_id { get; set; }

        [Display(Name = "Division Name*")]
        public int division_id { get; set; }

        [Display(Name = "District Name*")]
        public int district_id { get; set; }

        [ForeignKey("Upazila")]
        [Display(Name = "Upazila Name*")]
        [Required(ErrorMessage = "Please select upazila name", AllowEmptyStrings = false)]
        public int upazila_id { get; set; }

        [Display(Name = "Name*")]
        [Required(ErrorMessage = "SK name is required.", AllowEmptyStrings = false)]
        public string name { get; set; }
		
        public int status { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime created_at { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime updated_at { get; set; }

        public virtual POModel PO { get; set; }
        public virtual UpazilaModel Upazila { get; set; }
    }
}
