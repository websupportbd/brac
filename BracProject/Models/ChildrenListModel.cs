﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
	[Table("children_list")]
	public class ChildrenListModel
    {
		[Key]      
		public int id { get; set; }
		public string unique_id { get; set; }
        [Display(Name = "Union")]
        public string union_id { get; set; }
        [Display(Name = "Village")]
        public string village_id { get; set; }
        [Display(Name = "SS List")]
        public int ss_id { get; set; }
        public int po_id { get; set; }
        public int sk_id { get; set; }
        [Display(Name = "Household No")]
        public string hh { get; set; }
        [Display(Name = "Child No")]
        public string child_num { get; set; }
        [Display(Name = "Household Member")]
        public string household_member { get; set; }
        [Display(Name = "Child Name")]
        public string children_name { get; set; }
        [Display(Name = "Mother Name")]
        public string mother_name { get; set; }
        [Display(Name = "Father Name")]
        public string father_name { get; set; }
        [Display(Name = "Age")]
        public int age { get; set; }
        [Display(Name = "Gender")]
        public string gender { get; set; }
        [Display(Name = "School")]
        public int school_id { get; set; }
        [Display(Name = "Class")]
      
        public string class_id { get; set; }
        [Display(Name = "Taken Medicine Place")]
        public string taken_medicine_place { get; set; }
        [Display(Name = "Last Six Months Deworming Status")]
        public int six_months_medicine { get; set; }
        [Display(Name = "Sanitary Latrine Use")]
        public int is_toylet { get; set; }
        [Display(Name = " Source of Drinking Water")]
        public int drinking_water_id { get; set; }
        [Display(Name = "School verification")]
        public int school_verification { get; set; }
        [Display(Name = "Side Effect")]
        public int medicine_side_effect { get; set; }
        [Display(Name = "Children Category Status")]
        public int children_category_status { get; set; }

        public int is_school_going { get; set; }
         public string create_date { get; set; }

        [Display(Name = "Status")]       
        public int status { get; set; }
        //public DateTime created_at { get; set; } 
        //public DateTime updated_at { get; set; }

        public string r_1 { get; set; }
        public string r_2 { get; set; }
        public string r_3 { get; set; }


    }
}
