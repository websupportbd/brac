﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BracProject.Models
{
	[Table("DISTRICT")]
	public class DistrictModel
	{
		[Key]
		public int ID { get; set; }

        [ForeignKey("Division")]
        public int DIVISIONID { get; set; }

		public string NAME { get; set; }
		public string BANGLANAME { get; set; }
		public string BBSCODE { get; set; }
        [Display(Name = "Status")]
        public int ISACTIVE { get; set; }

        virtual public DivisionModel Division { get; set; }

    }
}
