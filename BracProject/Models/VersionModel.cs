﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BracProject.Models
{
	[Table("VERSION")]
	public class VersionModel
	{
		[Key]
		public int ID { get; set; }
        public int VER { get; set; }
		public string CREATED_AT { get; set; }
	}
}
