﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
	[Table("school_list")]
    public class SchoolListModel
    {
		public int id { get; set; }

        [Display(Name = "Division Name*")]
        public int division_id { get; set; }

        [Display(Name = "District Name*")]
        public int district_id { get; set; }

        [ForeignKey("Upazila")]
        [Display(Name = "Upazila Name*")]
        [Required(ErrorMessage = "Please select upazila name", AllowEmptyStrings = false)]
        public int upazila_id { get; set; }

        [ForeignKey("Union")]
        [Display(Name = "Union*")]
        [Required(ErrorMessage = "Union is required.", AllowEmptyStrings = false)]
        public int union_id { get; set; }

        [Display(Name = "School Name*")]
        [Required(ErrorMessage = "School name is required.", AllowEmptyStrings = false)]
        public string school_name { get; set; }

		public int status { get; set; }

        public virtual UpazilaModel Upazila { get; set; }
        public virtual UnionModel Union { get; set; }
    }
}
