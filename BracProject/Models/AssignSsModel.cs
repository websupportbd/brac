﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("assign_ss")]
    public class AssignSsModel
    {
        [Key]
        public int id { get; set; }

        [ForeignKey("SS")]
        [Display(Name = "SS Name*")]
        [Required(ErrorMessage = "Please select SS name", AllowEmptyStrings = false)]
        public int ss_id { get; set; }

        [Display(Name = "SK Name*")]
        [Required(ErrorMessage = "Please select SK name", AllowEmptyStrings = false)]
        public int sk_id { get; set; }

        [ForeignKey("Village")]
        [Display(Name = "Village Name*")]
        public int vill_id { get; set; }

        public int para_id { get; set; }
        public string ids { get; set; }

        public int status { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime created_at { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime updated_at { get; set; }

        public virtual MemberModel SS { get; set; }
        public virtual VillageModel Village { get; set; }

    }
}
