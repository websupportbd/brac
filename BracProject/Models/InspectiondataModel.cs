﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BracProject.Models
{
	[Table("inspection_data")]
    public class InspectiondataModel
    {
		[Key]
		public int ID { get; set; }
        
		public int USER_ID { get; set; }
        public int DIV { get; set; }
        public int DIS { get; set; }
        public int UPZ { get; set; }
        
		public string LOCATION { get; set; }
		public string LAT { get; set; }
		public string LON { get; set; }

		[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CREATED_AT { get; set; }

        public int STATUS { get; set; }
        public int READ_STATUS { get; set; }
        
    }
}
