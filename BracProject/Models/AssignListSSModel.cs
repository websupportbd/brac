﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("assign_list_ss")]
    public class AssignListSSModel
    {
        [Key]
        public int id { get; set; }

        [ForeignKey("AssignSs")]
        public int ss_list_id { get; set; }
        public string union_id { get; set; }
        public string village_id { get; set; }
        public string upz_id { get; set; }
        public string ward_id { get; set; }
        public string para_id { get; set; }

        public int status { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime created_at { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime updated_at { get; set; }

        public virtual AssignSsModel AssignSs { get; set; }
    }
}