﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("assign_list_sk")]
    public class AssignListSkModel
    {
        [Key]
        public int id { get; set; }

        [ForeignKey("AssignSk")]
        public int sk_list_id { get; set; }

        [ForeignKey("Union")]
        public int union_id { get; set; }

        [ForeignKey("Village")]
        public int village_id { get; set; }

        public int status { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime created_at { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime updated_at { get; set; }

        public virtual AssignSkModel AssignSk { get; set; }
        public virtual UnionModel Union { get; set; }
        public virtual VillageModel Village { get; set; }
    }
}
