﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("children_log")]
    public class ChildrenLogModel
    {
        public int id { get; set; }
        public int sk_id { get; set; }
        public int c_id { get; set; }
        public DateTime udtae { get; set; }
        public int status { get; set; }
    }
}
