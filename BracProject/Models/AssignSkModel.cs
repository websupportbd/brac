﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("assign_sk")]
    public class AssignSkModel
    {
        [Key]
        public int id { get; set; }

        [ForeignKey("SK")]
        [Display(Name = "SK Name*")]
        [Required(ErrorMessage = "Please select SK name", AllowEmptyStrings = false)]
        public int sk_id { get; set; }

        [Display(Name = "PO Name*")]
        [Required(ErrorMessage = "Please select PO name", AllowEmptyStrings = false)]
        public int po_id { get; set; }

        [ForeignKey("Division")]
        [Display(Name = "Division Name*")]
        public int div_id { get; set; }

        [ForeignKey("District")]
        [Display(Name = "District Name*")]
        public int dis_id { get; set; }


        [Display(Name = "Upazila*")]
        public string uz_ids { get; set; }



        [Display(Name = "Union*")]
        public string ui_ids { get; set; }


        [Display(Name = "Ward*")]
        public string wa_ids { get; set; }


        public string vi_ids { get; set; }

        public string pa_ids { get; set; }
        public int status { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime created_at { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime updated_at { get; set; }

        public virtual MemberModel SK { get; set; }
        public virtual DivisionModel Division { get; set; }
        public virtual DistrictModel District { get; set; }


    }
}
