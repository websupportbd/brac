﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("visit")]
    public class VisitListModel
    {
        public int id { get; set; }
        public int po_id { get; set; }
        public int sk_id { get; set; }
        public int ss_id { get; set; }
        public int children_id { get; set; }
        public DateTime visit_date { get; set; }
    }
}
