﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
	[Table("class_list")]
    public class ClassListModel
    {
		public int id { get; set; }
        public string class_name { get; set; }
        public int status { get; set; }
    }
}
