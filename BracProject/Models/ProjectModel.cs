﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BracProject.Models
{
    [Table("PROJECT")]
    public class ProjectModel
    {
		[Key]
		public int ID { get; set; }

		[Display(Name = "Project name*")]
		[Required(ErrorMessage = "Project name is required.", AllowEmptyStrings = false)]
        public string NAME { get; set; }

		[Display(Name = "Project Code")]
		[Required(ErrorMessage = "Project code is required.", AllowEmptyStrings = false)]
        public string CODE_PART1 { get; set; }

		[Display(Name = "Project Code2")]
        public string CODE_PART2 { get; set; }

		[Display(Name = "Project Title")]
		[Required(ErrorMessage = "Project title is required.", AllowEmptyStrings = false)]
        public string PROJECTTITLE { get; set; }

		[Display(Name = "Project Title Bangla")]
        public string PROJECTTITLEBANGLA { get; set; }
    }
}
