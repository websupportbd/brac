﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace BracProject.Models
{
    [Table("member_type")]
    public class MemberTypeModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public int status { get; set; }
    }
}
