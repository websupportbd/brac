﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BracProject.Models
{

    [Table("para_list")]
    public class ParaListModel
    {
        public int id { get; set; }

        [ForeignKey("ward_list")]
        public int ward_id { get; set; }
        public string name { get; set; }
        [Display(Name = "Status")]
        public int ISACTIVE { get; set; }

        virtual public WardListModel ward_list { get; set; }
    }
}