﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BracProject.Models
{
    [Table("member")]
    public class MemberModel

    {
        public int id { get; set; }

        [ForeignKey("MemberType")]
        [Display(Name = "Type*")]
        [Required(ErrorMessage = "Type is required.", AllowEmptyStrings = false)]
        public int type { get; set; }

        [Display(Name = "Member Name*")]
        [Required(ErrorMessage = "Member name is required.", AllowEmptyStrings = false)]
        public string name { get; set; }
        [Display(Name = "User Name*")]
        public string user_name { get; set; }
        [Display(Name = "Mobile No")]
        public string mobile_no { get; set; }
        [Display(Name = "Password*")]
        public string password { get; set; }

        public int status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }

        public virtual MemberTypeModel MemberType { get; set; }
    }
}
