﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace BracProject.Models
{
    [Table("users")]
    public class LoginModel
    {
        [Key]
        public int id { get; set; }

        [Display(Name = "Full Name")]
        [Required(ErrorMessage = "Full name is required.", AllowEmptyStrings = false)]
        public string name { get; set; }

        [Display(Name = "User Name")]
        [Required(ErrorMessage = "User name is required.", AllowEmptyStrings = false)]
        public string user_name { get; set; }

        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Email address is required.", AllowEmptyStrings = false)]
        public string email { get; set; }

        [Display(Name = "Mobile")]
        [Required(ErrorMessage = "Mobile number is required.", AllowEmptyStrings = false)]
        public string mobile { get; set; }

        [Required(ErrorMessage = "Password is required.", AllowEmptyStrings = false)]
        public string password { get; set; }

        public int status { get; set; }
        public int role_id { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime created_at_date { get; set; }

        public DateTime created_at_time { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime updated_at_date { get; set; }

        public DateTime updated_at_time { get; set; }

    }
}
