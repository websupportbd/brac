﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BracProject.Models
{
    [Table("module_list")]
    public class ModuleListModel
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string short_name { get; set; }
        public int status { get; set; }
        public DateTime created_at { get; set; }
    }

}
