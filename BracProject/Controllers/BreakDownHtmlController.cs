﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using BracProject.Models;
using Newtonsoft.Json;

namespace BracProject.Controllers
{
    public class BreakDownHtmlController
    {
        private static MySqlCon db = new MySqlCon();


        public static bool Registration(String html)
        {
            string strHtmlDOM = html;
            XmlDocument webPage = new XmlDocument();
            webPage.LoadXml(strHtmlDOM);
            XmlNode xmlElement;
			//XmlAttributeCollection xmlAttributes;
			//XmlNode nodeValue;

			string khana_no, khana_member, children_name, mother_name, father_name, age;
			int gender, school_id, class_id, six_months_medicine, is_toylet, drinking_water_id;

			string input1="_khana_no";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input1 + "\"]");
			khana_no = xmlElement.InnerText;

			string input2 = "_members";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input2 + "\"]");
			khana_member = xmlElement.InnerText;
            
			string input3 = "_children_name";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input3 + "\"]");
			children_name = xmlElement.InnerText;
            
			string input4 = "_mother_name";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input4 + "\"]");
			mother_name = xmlElement.InnerText;
            
			string input5 = "_father_name";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input5 + "\"]");
			father_name = xmlElement.InnerText;
            
			string input6 = "_age";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input6 + "\"]");
			if(String.IsNullOrEmpty(xmlElement.InnerText))
			{
				//age = "1";
                age = xmlElement.InnerText;
            }
            else{
				age = xmlElement.InnerText;
			}


			string input7 = "_gender";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input7 + "\"]");
			string gender_val = xmlElement.InnerText;
			gender = Convert.ToInt32(gender_val);

			string input8 = "_school";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input8 + "\"]");
            string school_val = xmlElement.InnerText;
			school_id = Convert.ToInt32(school_val);

			string input9 = "_class";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input9 + "\"]");
            string class_val = xmlElement.InnerText;
			class_id = Convert.ToInt32(class_val);

			string input10 = "_eat_capsul";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input10 + "\"]");
            string eat_capsul_val = xmlElement.InnerText;
			six_months_medicine = Convert.ToInt32(eat_capsul_val);

			string input11 = "_toylet";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input11 + "\"]");
            string toylet_val = xmlElement.InnerText;
			is_toylet = Convert.ToInt32(toylet_val);
            
			string input12 = "_water";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input12 + "\"]");
            string water_val = xmlElement.InnerText;
			drinking_water_id = Convert.ToInt32(water_val);

            var transaction = db.Database.BeginTransaction();
            
            try
            {
				string unick_id_p1 = "19-10-147-"+ khana_no;
				var unickIdDetals = db.ChildrenList_Data.Where(ii_ => ii_.union_id == "19" && ii_.village_id == "10" && ii_.ss_id == 147 && ii_.hh == khana_no).ToList();
				int chindrinCount;
				if(unickIdDetals.Count() != 0)
				{
					chindrinCount = unickIdDetals.Count() + 1;
				}else{
					chindrinCount = 1;
				}

				string unick_id = unick_id_p1 + "-" + chindrinCount;

				var insert_khana_details = new ChildrenListModel();
				insert_khana_details.unique_id = unick_id;
				insert_khana_details.union_id = "19";
				insert_khana_details.village_id = "10";
				insert_khana_details.ss_id = 147;
				insert_khana_details.hh = khana_no;
				insert_khana_details.child_num = chindrinCount.ToString();
				insert_khana_details.household_member =khana_member;
				insert_khana_details.children_name = children_name;
				insert_khana_details.mother_name = mother_name;
				insert_khana_details.father_name = father_name;            
				insert_khana_details.age = Convert.ToInt32(age);
				insert_khana_details.gender = gender.ToString();
				insert_khana_details.school_id = school_id;
				insert_khana_details.class_id = class_id.ToString();
				insert_khana_details.six_months_medicine = six_months_medicine;
				insert_khana_details.is_toylet = is_toylet;
				insert_khana_details.drinking_water_id = drinking_water_id;
				//insert_khana_details.created_at = DateTime.Now;
				//insert_khana_details.updated_at = DateTime.Now;
				db.ChildrenList_Data.Add(insert_khana_details);
                db.SaveChanges();
				int chil_id = insert_khana_details.id;

				//var insert_ = new DoseListModel();
				//insert_.children_id = chil_id;
				//insert_.union_id = unick_id;
				//insert_.date = DateTime.Now;
				//db.DoseList_Data.Add(insert_);
				//db.SaveChanges();

                transaction.Commit();
            }
            catch (System.Exception e)
            {
                transaction.Rollback();
                return false;
            }

            return true;
        }

		public static bool KhanaVisit(String html, int khana_id)
        {
            string strHtmlDOM = html;
            XmlDocument webPage = new XmlDocument();
            webPage.LoadXml(strHtmlDOM);
            XmlNode xmlElement;
            //XmlAttributeCollection xmlAttributes;
            //XmlNode nodeValue;

            string khana_no, khana_member, children_name, mother_name, father_name, age;
            int gender, school_id, class_id, six_months_medicine, is_toylet, drinking_water_id;

            string input2 = "_members";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input2 + "\"]");
            khana_member = xmlElement.InnerText;

            string input10 = "_eat_capsul";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input10 + "\"]");
            string eat_capsul_val = xmlElement.InnerText;
            six_months_medicine = Convert.ToInt32(eat_capsul_val);

            string input11 = "_toylet";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input11 + "\"]");
            string toylet_val = xmlElement.InnerText;
            is_toylet = Convert.ToInt32(toylet_val);

            string input12 = "_water";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input12 + "\"]");
            string water_val = xmlElement.InnerText;
            drinking_water_id = Convert.ToInt32(water_val);

         
            var transaction = db.Database.BeginTransaction();

            try
            {
				var update_khana_details = db.ChildrenList_Data.Where(_i => _i.id == khana_id).FirstOrDefault();
				update_khana_details.household_member = khana_member;
				update_khana_details.six_months_medicine = six_months_medicine;
				update_khana_details.is_toylet = is_toylet;
				update_khana_details.drinking_water_id = drinking_water_id;            
				//update_khana_details.updated_at = DateTime.Now;
                db.SaveChanges();

                transaction.Commit();
            }
            catch (System.Exception e)
            {
                transaction.Rollback();
                return false;
            }

            return true;
        }

        public static string getPreviousQualityInspection(String html)
        {
            string strHtmlDOM = html;
            XmlDocument webPage = new XmlDocument();
            webPage.LoadXml(strHtmlDOM);
            XmlNode xmlElement;

            string input = "_defect_list";
            xmlElement = webPage.SelectSingleNode("//div[@id=\"" + input + "\"]");
            string defect_list = xmlElement.InnerText;
            return defect_list;
        }
    }
}
