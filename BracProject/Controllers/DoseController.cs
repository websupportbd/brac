﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
	public class DoseController : ApiController
    {
		private MySqlCon db = new MySqlCon();

		// GET: api/login
        public string Get()
        {
            return "value";
        }

        // GET: api/login/5
        public object Get(int id)
        {
            var results = db.DoseList_Data.Where(x => x.children_id==id).ToList();

            if (results.Count() != 0)
            {
                var obj = new DoseListJson();
                foreach (var entity in results)
                {
                    obj.doseList.Add(new DoseList
                    {
                        id = entity.id,
                        dose_date = entity.date.Date.ToShortDateString(),
                        dose_time= entity.date.ToShortTimeString(),
                        taken_dose = entity.taken_dose_place,
                        side_effect = entity.side_effect
                    });
                }

                var data = new
                {
                    success = true,
                    count = results.Count(),
                    obj.doseList
                };
                return data;
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "Dose List Not found!"
                };
                return data;
            }
        }

        // POST: api/login
        public object Post([FromBody]FormDataCollection formbody)
        {
            try {
                string chil_id = formbody.GetValues("chil_id")[0];
                string dose_place = formbody.GetValues("taken_dose_place")[0];
                string sideeffect = formbody.GetValues("side_effect")[0];
                int id = Convert.ToInt32(chil_id);
                int sideeffectid = Convert.ToInt32(sideeffect);

                var insert_data = new DoseListModel();
                insert_data.children_id = id;
                var u_id = db.ChildrenList_Data.Where(ii_ => ii_.id == id).FirstOrDefault();
                insert_data.union_id = u_id.union_id;
                insert_data.taken_dose_place = dose_place;
                insert_data.side_effect = sideeffectid;
                insert_data.date = DateTime.Now;
                db.DoseList_Data.Add(insert_data);
                int inspec_data_sucess = db.SaveChanges();

                var log_insert = new ChildrenLogModel();
                log_insert.sk_id = Convert.ToInt32(u_id.sk_id);
                log_insert.c_id = Convert.ToInt32(chil_id);
                log_insert.udtae = DateTime.Now;
                log_insert.status = 4;
                db.ChildrenLog_Data.Add(log_insert);
                int log_id = db.SaveChanges();

                var data = new
                {
                    success = true,
                    message = "Data save successfully !!"
                };
                return data;
            } catch (Exception ex)
            {
                var data = new
                {
                    success = false,
                    message = ex.Message
                };
                return data;
            } 
			
            
        }

        // PUT: api/login/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/login/5
        public void Delete(int id)
        {
        }
    }

    public class DoseListJson
    {
        public DoseListJson() { doseList = new List<DoseList>(); }
        public List<DoseList> doseList { get; set; }
    }
    public class DoseList
    {
        public int id { get; set; }
        public string dose_date { get; set; }
        public string dose_time { get; set; }
        public string taken_dose  { get; set; }
        public int side_effect { get; set; }

    }
}
