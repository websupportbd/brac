﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;
using System.Web.ApplicationServices;
using System.Data.Entity;

namespace BracProject.Controllers
{
    [Authorize]
    public class WebProjecttemController : Controller
    {
		private MySqlCon db = new MySqlCon();

		public ActionResult Index()
		{
			return View();
		}


		[HttpPost]
		public ActionResult List(DataTableRequest req)
		{
			int totalRecord = 0;
			int start = req.start;
			int take = req.length;
			string search = req.search.value;

            string CompID = "", ProjID = "", TypeID = "", from_date = "", to_date = "";

            if (!String.IsNullOrEmpty(search))
            {
                string[] isFilter = search.Split(';');
                if (isFilter.Count() > 1)
                {
                    search = "";
                    foreach (var a in isFilter)
                    {
                        string[] filter = a.Split(':');
                        if (filter[0] == "comp")
                        {
                            CompID = filter[1];
                        }
                        if (filter[0] == "pro")
                        {
                            ProjID = filter[1];
                        }
                        if (filter[0] == "typ")
                        {
                            TypeID = filter[1];
                        }
                        if (filter[0] == "from_date")
                        {
                            from_date = filter[1];
                        }
                        if (filter[0] == "to_date")
                        {
                            to_date = filter[1];
                        }
                    }
                }
            }

            var query = db.InsForm_Data.AsQueryable();

            if (!String.IsNullOrEmpty(TypeID))
            {
                if (TypeID == "1")
                {
                    query = query.Where(w => w.IS_GENERIC == 0);
                }else{
                    query = query.Where(w => w.IS_GENERIC == 1);
                }

            }
            if (!String.IsNullOrEmpty(from_date))
            {
                //string[] break_from_date = from_date.Split('-');
                //string from_date_format = break_from_date[2] + "-" + break_from_date[1] + "-" + break_from_date[0];
                //query = query.Where(w => w.CREATED_AT > from_date_format);
            }
            if (!String.IsNullOrEmpty(to_date))
            {
                //string[] break_from_date = from_date.Split('-');
                //string from_date_format = break_from_date[2] + "-" + break_from_date[1] + "-" + break_from_date[0];
                //query = query.Where(w => w.CREATED_AT < from_date_format);
            }

			if (req.order[0].dir == "asc")
			{
				query = query.OrderBy(w => w.NAME_ORG);
			}
			else if (req.order[0].dir == "desc")
            {
				query = query.OrderByDescending(w => w.NAME_ORG);
            }

            var results = new List<InsFormModel>();
			if (search == null || search == "")
			{
                totalRecord = query.Count();
                query = query.Skip(start).Take(take);
                results = query.ToList<InsFormModel>();
			}
			else
			{
                totalRecord = db.InsForm_Data.Where(c => c.NAME_ORG.Contains(search)).Count();
                var Query = db.InsForm_Data.Where(c => c.NAME_ORG.Contains(search)).OrderByDescending(c => c.ID).Skip(start).Take(take);
                results = Query.ToList<InsFormModel>();
			}

			var obj = new ProjectTemWebJson();
			foreach (var entity in results)
			{
                string projectName = "";
                string type = "";
                if (entity.IS_GENERIC == 0)
                {
                    projectName = "";
                    type = "Project";
                }else{
                    projectName = "";
                    type = "Generic";
                }


                obj.data.Add(new ProjectTemWebListJson
                {
                    temp_name = entity.NAME_ORG,
                    type = type,
                    status = entity.STATUS,
					action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                                "<a style = 'width:10px;padding-left:10px;' href='/projecttem/Edit/" + entity.ID + "' class='btn btn-icon btn-success btn-square'>" +
                                "<i class='icon-edit'></i>" +
                                "</a>" +
                                "<button style = 'width:10px;padding-left:10px;' val='" + entity.ID + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                                "<i class='icon-android-delete'></i>" +
                                "</button>" +
                            "</div>"
				});
			}
			var data = new
			{
				recordsTotal = results.Count(),
				recordsFiltered = totalRecord,
				obj.data
			};
			return Json(data, JsonRequestBehavior.AllowGet);

		}

		[HttpGet]
		public ActionResult Add()
		{
			return View();
		}
		
        [HttpGet]
        public ActionResult save()
        {
            return View();
        }

        [HttpPost]
        public ActionResult save(InsFormModel cont)
        {

                     


            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.InsForm_Data.Where(c => c.ID == id).FirstOrDefault<InsFormModel>();
            return View(cont);
        }

        [HttpGet]
        public ActionResult editfrom(int id)
        {
            var cont = db.InsForm_Data.Where(c => c.ID == id).FirstOrDefault<InsFormModel>();
            return View(cont);
        }

        [HttpPost]
        public ActionResult editfrom(InsFormModel cont)
        {
            string html_code = TemplateHeaderFooter.Header() + Session["html_code"].ToString() + TemplateHeaderFooter.Footer() + JavaScript();
            string json_code = Session["json_code"].ToString();

            cont.JSON = json_code;
            cont.RAW_CODE = html_code;
            cont.STATUS = 1;
            if (ModelState.IsValid)
            {
                db.Entry(cont).State = EntityState.Modified;
                db.SaveChanges();
                ModelState.Clear();
                ViewBag.Message = "Update Successfully";
            }

            return View(cont);
        }



        [HttpGet]
        public ActionResult GetProject()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var list = db.Project_Data.OrderBy(d => d.ID).ToList<ProjectModel>();
            foreach (var entity in list)
            {
                items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult setcode(string html_code, string json_code)
        {
            Session.Remove("html_code");
            Session.Remove("json_code");
            Session["html_code"] = html_code;
            Session["json_code"] = json_code;
            return Json("success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult getjsoncode(string form_id)
        {
            int id = Convert.ToInt32(form_id);
            string data = db.InsForm_Data.Where(i => i.ID == id).FirstOrDefault<InsFormModel>().JSON;
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public string JavaScript()
        {
			//string javaScript = db.JavaScript_Data.Where(j => j.ID == 1).FirstOrDefault().SCRIPT;
			string javaScript = "";
			string value = "<script>" + javaScript + "</script></html>";
            return value;
        }

        //delete function. that call from ajax
        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.InsForm_Data.Where(at => at.ID == id).SingleOrDefault<InsFormModel>();
            db.InsForm_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }

	public class ProjectTemWebJson
	{
		public ProjectTemWebJson() { data = new List<ProjectTemWebListJson>(); }
		public List<ProjectTemWebListJson> data { get; set; }
	}
	public class ProjectTemWebListJson
	{
		public int id { get; set; }
        public string project_name { get; set; }
		public string comp_name { get; set; }
        public string temp_name { get; set; }
        public string type { get; set; }
        public int status { get; set; }
		public string action { get; set; }
	}
}
