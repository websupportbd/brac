﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BracProject.Models;
using Newtonsoft.Json;


namespace BracProject.Controllers
{
    public class InspectionController : ApiController
    {
		private MySqlCon db = new MySqlCon();

		// GET: api/inspection

		public string Get()
		{
			return "value";
		}

		// GET: api/inspection/5
		public object Get(int id)
		{         
			return "value";
		}

		// POST: api/inspection
        public object Post([FromBody]FormDataCollection formbody)
		{
			string projuct_id = null;
			if (!string.IsNullOrEmpty(formbody.GetValues("user_name")[0]))
			{
				projuct_id = formbody.GetValues("user_name")[0];
			}


			var Query = db.InsForm_Data.Where(ins => ins.STATUS == 1);
			var results = Query.ToList<InsFormModel>();

			if (results.Count() != 0)
			{
                var obj = new InspectionJson();
				foreach (var entity in results)
				{
					string htmlRawCode = "";
					htmlRawCode = htmlRawCode + entity.RAW_CODE;

                    obj.result.Add(new InspectionListJson
					{
                        inspection_id = entity.ID,
						inspection_title = entity.NAME_ORG,
                        from_details = htmlRawCode
					});
				}

				var data = new
				{
					success = true,
					count = results.Count(),
					obj.result
				};
				return data;
			}
			else
			{
				var data = new
				{
					success = false,
					message = "From list not found!"
				};
				return data;
			}
		}

		// PUT: api/inspection/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE: api/inspection/5
		public void Delete(int id)
		{
		}
    }

	public class InspectionJson
	{
		public InspectionJson() { result = new List<InspectionListJson>(); }
		public List<InspectionListJson> result { get; set; }
	}
	public class InspectionListJson
	{
        public int inspection_id { get; set; }
		public string inspection_title { get; set; }
        public string from_details { get; set; }
	}
}
