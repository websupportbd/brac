﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using BracProject.Models;

using Newtonsoft.Json;

namespace BracProject.Controllers
{
    public class WebLoginController : Controller
    {
        private MySqlCon db = new MySqlCon();

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(LoginModel user)
        {
            string message = string.Empty;
            if (String.IsNullOrEmpty(user.user_name) || String.IsNullOrEmpty(user.password))
            {
                ViewBag.Message = "Username and/or password is incorrect.";
            }
            else
            {

               // String password = FormsAuthentication.HashPasswordForStoringInConfigFile(user.password, "md5");
                var userDetails = db.Member_Data.Where(lo => lo.user_name == user.user_name && lo.password == user.password).FirstOrDefault<MemberModel>();

                if (userDetails == null)
                {
                    ViewBag.Message = "Username and/or password is incorrect.";
                }
                else
                {
                    Session["memberType"] = userDetails.MemberType.ToString();
                    FormsAuthentication.SetAuthCookie(user.email, false);
                    //string roles = UserRoleString(userDetails.id);
                    var authTicket = new FormsAuthenticationTicket(1, user.user_name, DateTime.Now, DateTime.Now.AddMinutes(20), false, "");
                    string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                    var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    HttpContext.Response.Cookies.Add(authCookie);

                    return RedirectToAction("../dashboard");
                }
            }

            return View(user);
        }



        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();


            return RedirectToAction("/");
        }

        //public string UserRoleString(int userID)
        //{
        //    string userRole = null;
        //    var result = db.UserRoleList_Data.Where(ur => ur.USER_ID == userID).ToList<UserRoleListModel>();
        //    if (result.Count() != 0)
        //    {
        //        foreach (var entity in result)
        //        {
        //            string modelName = db.ModuleList_Data.Where(ml => ml.ID == entity.MODULE_ID).FirstOrDefault<ModuleListModel>().SHORT_NAME;
        //            if (entity.U_VIEW == 1) 
        //            {
        //                userRole =modelName+"View"+","+userRole; 
        //            }
        //            if (entity.U_ADD == 1)
        //            {
        //                userRole = modelName + "Add" + "," + userRole;
        //            }
        //            if (entity.U_EDIT == 1)
        //            {
        //                userRole = modelName + "Edit" + "," + userRole;
        //            }
        //            if (entity.U_DELETE == 1)
        //            {
        //                userRole = modelName + "Del" + "," + userRole;
        //            }
        //        }
        //    }

        //    userRole = userRole.TrimEnd(',');
        //    return userRole;
        //}
    }
}
