﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class VisitListController : ApiController
    {
        private MySqlCon db = new MySqlCon();

        // GET: api/khanalist
        public IEnumerable<string> Get()
        {
            return new string[] { "empty" };
        }

        // GET: api/contact/5
        public object Get(int id)
        {
            return "";
        }

        // POST: api/visitlist
        public object Post([FromBody]FormDataCollection formbody)
        {
            //string poId = formbody.GetValues("po_id")[0];
            string skId = formbody.GetValues("sk_id")[0];
            //string ssId = formbody.GetValues("ss_id")[0];
            string childrenId = formbody.GetValues("children_id")[0];

            if (!String.IsNullOrEmpty(skId))
            {
                int id = Convert.ToInt32(skId);
                var results = db.Visit_Data.Where(i => i.sk_id == id).ToList();
                if (results.Count() != 0)
                {
                    var obj = new VJson();
                    foreach (var entity in results)
                    {
                        var mDetails = db.Member_Data.Where(ii => ii.id == entity.sk_id).FirstOrDefault();
                        var cDetails = db.ChildrenList_Data.Where(ii => ii.id == entity.children_id).FirstOrDefault();

                        string schoolName = "";
                        int sixmonthStatus = 0;
                        DateTime lastSixMonth;
                        var schoolDetails = db.SchoolList_Data.Where(_i => _i.id == cDetails.school_id).FirstOrDefault();
                        if (schoolDetails != null)
                        {
                            schoolName = schoolDetails.school_name;
                        }
                        else
                        {
                            schoolName = "";
                        }

                        var sixmonth = db.DoseList_Data.Where(_i => _i.children_id == cDetails.id).OrderByDescending(m => m.date).Select(p => p).FirstOrDefault();
                        if (sixmonth != null)
                        {
                            DateTime today = DateTime.Today;
                            lastSixMonth = sixmonth.date;
                            int days = (today - lastSixMonth).Days;
                            double month = (today - lastSixMonth).Days / 30;

                            if (month <= 6)
                            {
                                sixmonthStatus = 1;
                            }
                            else
                            {
                                sixmonthStatus = 2;
                            }
                        }
                        string sideeffect = "";
                        if (cDetails.medicine_side_effect == 0)
                        {
                            sideeffect = "NO";
                        }
                        else
                        {
                            sideeffect = "YES";
                        }

                        obj.result.Add(new VListJson
                        {
                            id = entity.id,
                            sk_id = entity.sk_id.ToString(),
                            sk_name = mDetails.name,
                            visit_date = entity.visit_date.ToString(),

                            hh_id = cDetails.id.ToString(),
                            unick_id = cDetails.unique_id,
                            union_id = cDetails.union_id,
                            village_id = cDetails.village_id,
                            khana_no = cDetails.hh,
                            chindrinCount = cDetails.child_num,
                            khana_member = cDetails.household_member,
                            children_name = cDetails.children_name,
                            mother_name = cDetails.mother_name,
                            father_name = cDetails.father_name,
                            six_months_status = sixmonthStatus,
                            school_verification = cDetails.school_verification,
                            age = cDetails.age.ToString(),
                            gender = cDetails.gender,
                            school_id = cDetails.school_id,
                            school_name = schoolName,
                            class_id = cDetails.class_id,
                            six_months_medicine = cDetails.six_months_medicine,
                            is_toylet = cDetails.is_toylet,
                            drinking_water_id = cDetails.drinking_water_id,
                            takenMedicinePlace = cDetails.taken_medicine_place,
                            medicineSideEffect = sideeffect,
                            createDate = cDetails.create_date,
                            childrenCategoryStatus = cDetails.children_category_status,
                            isschoolgoing = cDetails.is_school_going
                        });
                    }

                    var data = new
                    {
                        success = true,
                        count = results.Count(),
                        obj.result
                    };
                    return data;
                }
                else
                {
                    var data = new
                    {
                        success = false,
                        message = "visit list not found!"
                    };
                    return data;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(childrenId))
                {
                    int id1 = Convert.ToInt32(childrenId);
                    var results1 = db.Visit_Data.Where(i => i.children_id == id1).ToList();
                    if (results1.Count() != 0)
                    {
                        var obj = new VJson();
                        foreach (var entity in results1)
                        {
                            var mDetails1 = db.Member_Data.Where(ii => ii.id == entity.sk_id).FirstOrDefault();
                            var cDetails1 = db.ChildrenList_Data.Where(ii => ii.id == entity.children_id).FirstOrDefault();

                            string schoolName = "";
                            int sixmonthStatus = 0;
                            DateTime lastSixMonth;
                            var schoolDetails = db.SchoolList_Data.Where(_i => _i.id == cDetails1.school_id).FirstOrDefault();
                            if (schoolDetails != null)
                            {
                                schoolName = schoolDetails.school_name;
                            }
                            else
                            {
                                schoolName = "";
                            }

                            var sixmonth = db.DoseList_Data.Where(_i => _i.children_id == cDetails1.id).OrderByDescending(m => m.date).Select(p => p).FirstOrDefault();
                            if (sixmonth != null)
                            {
                                DateTime today = DateTime.Today;
                                lastSixMonth = sixmonth.date;
                                int days = (today - lastSixMonth).Days;
                                double month = (today - lastSixMonth).Days / 30;

                                if (month <= 6)
                                {
                                    sixmonthStatus = 1;
                                }
                                else
                                {
                                    sixmonthStatus = 2;
                                }
                            }
                            string sideeffect = "";
                            if (cDetails1.medicine_side_effect == 0)
                            {
                                sideeffect = "NO";
                            }
                            else
                            {
                                sideeffect = "YES";
                            }

                            obj.result.Add(new VListJson
                            {
                                id = entity.id,
                                sk_id = entity.sk_id.ToString(),
                                sk_name = mDetails1.name,
                                visit_date = entity.visit_date.ToString(),

                                hh_id = cDetails1.id.ToString(),
                                unick_id = cDetails1.unique_id,
                                union_id = cDetails1.union_id,
                                village_id = cDetails1.village_id,
                                khana_no = cDetails1.hh,
                                chindrinCount = cDetails1.child_num,
                                khana_member = cDetails1.household_member,
                                children_name = cDetails1.children_name,
                                mother_name = cDetails1.mother_name,
                                father_name = cDetails1.father_name,
                                six_months_status = sixmonthStatus,
                                school_verification = cDetails1.school_verification,
                                age = cDetails1.age.ToString(),
                                gender = cDetails1.gender,
                                school_id = cDetails1.school_id,
                                school_name = schoolName,
                                class_id = cDetails1.class_id,
                                six_months_medicine = cDetails1.six_months_medicine,
                                is_toylet = cDetails1.is_toylet,
                                drinking_water_id = cDetails1.drinking_water_id,
                                takenMedicinePlace = cDetails1.taken_medicine_place,
                                medicineSideEffect = sideeffect,
                                createDate = cDetails1.create_date,
                                childrenCategoryStatus = cDetails1.children_category_status,
                                isschoolgoing = cDetails1.is_school_going
                            });
                        }

                        var data = new
                        {
                            success = true,
                            count = results1.Count(),
                            obj.result
                        };
                        return data;
                    }
                    else
                    {
                        var data = new
                        {
                            success = false,
                            message = "visit list not found!"
                        };
                        return data;
                    }
                }
                else
                {
                    var data = new
                    {
                        success = false,
                        message = "sk id is empty !"
                    };
                    return data;
                }
            }
            
        }

        // PUT: api/contact/5
        public void Put(int id, [FromBody]string value)
        {

        }

        // DELETE: api/contact/5
        public void Delete(int id)
        {

        }
    }
}

public class VJson
{
    public VJson() { result = new List<VListJson>(); }
    public List<VListJson> result { get; set; }
}
public class VListJson
{
    public int id { get; set; }
    //public string po_name { get; set; }
    //public string po_id { get; set; }
    //public string ss_name { get; set; }
    //public string ss_id { get; set; }
    public string sk_name { get; set; }
    public string sk_id { get; set; }
    public string visit_date { get; set; }

    public string hh_id { get; set; }
    public string unick_id { get; set; }
    public string union_id { get; set; }
    public string village_id { get; set; }
    public string khana_no { get; set; }
    public string chindrinCount { get; set; }
    public string khana_member { get; set; }
    public string children_name { get; set; }
    public string school_name { get; set; }
    public int six_months_status { get; set; }
    public string mother_name { get; set; }
    public string father_name { get; set; }
    public string age { get; set; }
    public int school_verification { get; set; }
    public string gender { get; set; }
    public int school_id { get; set; }
    public string class_id { get; set; }
    public int six_months_medicine { get; set; }
    public int is_toylet { get; set; }
    public int drinking_water_id { get; set; }
    public string takenMedicinePlace { get; set; }
    public string medicineSideEffect { get; set; }
    public string createDate { get; set; }
    public int childrenCategoryStatus { get; set; }
    public int isschoolgoing { get; set; }
}
