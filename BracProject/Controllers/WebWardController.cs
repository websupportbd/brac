﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebWardController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<WardListModel>();
            if (search == null || search == "")
            {
                totalRecord = db.Ward_Data.Count();
                var Query = db.Ward_Data.OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList();
            }
            else
            {
                totalRecord = db.Ward_Data.Where(c => c.name.Contains(search)).Count();
                var Query = db.Ward_Data.Where(c => c.name.Contains(search)).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList();
            }

            var obj = new WardWebJson();
            foreach (var entity in results)
            {

                string action = null;
                action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/ward/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>";
                var status = "";
                if (entity.ISACTIVE == 1)
                {
                    status = "Active";
                }
                else
                {
                    status = "Inactive";
                }

                obj.data.Add(new WardWebListJson
                {
                    id = entity.id,
                    division = entity.Upazila.District.Division.NAME,
                    district = entity.Upazila.District.NAME,
                    upazila = entity.Upazila.NAME,
                    name = entity.name,
                    Status=status,
                    action = action
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }
        

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Add(WardListModel cont)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Ward_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "Ward add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Session["wa_id"] = id;
            var cont = db.Ward_Data.Where(c => c.id == id).FirstOrDefault();
            return View(cont);
        }


        [HttpPost]
        public ActionResult Edit(WardListModel cont)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Ward update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.Ward_Data.Where(at => at.id == id).SingleOrDefault();
            db.Ward_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
    public class WardWebJson
    {
        public WardWebJson() { data = new List<WardWebListJson>(); }
        public List<WardWebListJson> data { get; set; }
    }
    public class WardWebListJson
    {
        public int id { get; set; }
        public string division { get; set; }
        public string district { get; set; }
        public string upazila { get; set; }
        public string name { get; set; }
        public string Status { get; set; }
        public string action { get; set; }
    }
}
