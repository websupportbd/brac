﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI;
using BracProject.Report;

namespace BracProject.Controllers
{
    public class WebDewarmingReportController : Controller
    {
        private MySqlCon db = new MySqlCon();
        // GET: WebDewarmingReport
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ReportTotalDewarmingChildren()
        {
            var data = from d in db.ChildrenList_Data
                       join ve in db.Village_Data on d.village_id equals ve.id.ToString() into jts
                       from jtResult in jts.DefaultIfEmpty()

                       join un in db.Union_Data on d.union_id equals un.id.ToString() into uns
                       from unsresult in uns.DefaultIfEmpty()
                       where d.six_months_medicine == 1
                       select new
                       {
                           d.unique_id,
                           d.six_months_medicine,
                           d.school_id,
                           d.is_toylet,
                           d.children_name,
                           d.father_name,
                           d.gender,
                           d.age,
                           d.mother_name,
                           villagName = jtResult.name,
                           unionName = unsresult.name

                       };

            // var data = db.ChildrenList_Data.AsQueryable();

            var obj = new List<ChildrenEntity>();
            foreach (var entity in data)
            {
                string gender_text = "";
                if (entity.gender == "1")
                {
                    gender_text = "Male";
                }
                else if (entity.gender == "0")
                {
                    gender_text = "Female";
                }
                else
                {
                    gender_text = entity.gender;
                }

                string isSchool = "";
                if (entity.school_id == null || entity.school_id == 0)
                {
                    isSchool = "Not Going";
                }
                else
                {
                    isSchool = "Going";
                }
                string viname = "";


                string unionname = "";


                obj.Add(new ChildrenEntity()
                {

                    ChildrenId = entity.unique_id,
                    ChildrenName = entity.children_name,
                    FatherName = entity.father_name,
                    MotherName=entity.mother_name,
                    ChildrenAge = entity.age.ToString(),
                    Gender = gender_text,
                    SchoolGoing = isSchool,
                    VillageName = entity.villagName,
                    UnionName = entity.unionName,

                });
            }



            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Report"), "rptTotalDewarming.rpt"));
            rd.SetDataSource(obj.ToList());          
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            try
            {
                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "Application/pdf", "DewarmingChildrenList.pdf");
            }
            catch
            {
                throw;
            }



        }

        public ActionResult ReportTotalNotDewarmingChildren()
        {
            var data = from d in db.ChildrenList_Data
                       join ve in db.Village_Data on d.village_id equals ve.id.ToString() into jts
                       from jtResult in jts.DefaultIfEmpty()

                       join un in db.Union_Data on d.union_id equals un.id.ToString() into uns
                       from unsresult in uns.DefaultIfEmpty()
                       where d.six_months_medicine == 0 || d.six_months_medicine == 2
                       select new
                       {
                           d.unique_id,
                           d.six_months_medicine,
                           d.school_id,
                           d.is_toylet,
                           d.children_name,
                           d.father_name,
                           d.gender,
                           d.age,
                           d.mother_name,
                           villagName = jtResult.name,
                           unionName = unsresult.name

                       };

            // var data = db.ChildrenList_Data.AsQueryable();

            var obj = new List<ChildrenEntity>();
            foreach (var entity in data)
            {
                string gender_text = "";
                if (entity.gender == "1")
                {
                    gender_text = "Male";
                }
                else if (entity.gender == "0")
                {
                    gender_text = "Female";
                }
                else
                {
                    gender_text = entity.gender;
                }

                string isSchool = "";
                if (entity.school_id == null || entity.school_id == 0)
                {
                    isSchool = "Not Going";
                }
                else
                {
                    isSchool = "Going";
                }
                string viname = "";


                string unionname = "";


                obj.Add(new ChildrenEntity()
                {

                    ChildrenId = entity.unique_id,
                    ChildrenName = entity.children_name,
                    FatherName = entity.father_name,
                    MotherName = entity.mother_name,
                    ChildrenAge = entity.age.ToString(),
                    Gender = gender_text,
                    SchoolGoing = isSchool,
                    VillageName = entity.villagName,
                    UnionName = entity.unionName,

                });
            }




            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Report"), "rptTotalNotDewarming.rpt"));
            rd.SetDataSource(obj.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            try
            {
                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "Application/pdf", "NotDewarmingChildrenList.pdf");
            }
            catch
            {
                throw;
            }



        }

        public ActionResult ReportTotalSchoolGoingChildren()
        {
            var data = from d in db.ChildrenList_Data
                       join ve in db.Village_Data on d.village_id equals ve.id.ToString() into jts
                       from jtResult in jts.DefaultIfEmpty()

                       join un in db.Union_Data on d.union_id equals un.id.ToString() into uns
                       from unsresult in uns.DefaultIfEmpty()
                       where d.school_id != 0
                       select new
                       {
                           d.unique_id,
                           d.six_months_medicine,
                           d.school_id,
                           d.is_toylet,
                           d.children_name,
                           d.father_name,
                           d.gender,
                           d.age,
                           d.mother_name,
                           villagName = jtResult.name,
                           unionName = unsresult.name

                       };

            // var data = db.ChildrenList_Data.AsQueryable();

            var obj = new List<ChildrenEntity>();
            foreach (var entity in data)
            {
                string gender_text = "";
                if (entity.gender == "1")
                {
                    gender_text = "Male";
                }
                else if (entity.gender == "0")
                {
                    gender_text = "Female";
                }
                else
                {
                    gender_text = entity.gender;
                }

                string isSchool = "";
                if (entity.school_id == null || entity.school_id == 0)
                {
                    isSchool = "Not Going";
                }
                else
                {
                    isSchool = "Going";
                }
                string viname = "";


                string unionname = "";


                obj.Add(new ChildrenEntity()
                {

                    ChildrenId = entity.unique_id,
                    ChildrenName = entity.children_name,
                    FatherName = entity.father_name,
                    MotherName = entity.mother_name,
                    ChildrenAge = entity.age.ToString(),
                    Gender = gender_text,
                    SchoolGoing = isSchool,
                    VillageName = entity.villagName,
                    UnionName = entity.unionName,

                });
            }



            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Report"), "rptTotalSchoolGoing.rpt"));
            rd.SetDataSource(obj.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            try
            {
                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "Application/pdf", "TotalSchoolGoingChildren.pdf");
            }
            catch
            {
                throw;
            }



        }

        public ActionResult ReportTotalNotSchoolGoingChildren()
        {
            var data = from d in db.ChildrenList_Data
                       join ve in db.Village_Data on d.village_id equals ve.id.ToString() into jts
                       from jtResult in jts.DefaultIfEmpty()

                       join un in db.Union_Data on d.union_id equals un.id.ToString() into uns
                       from unsresult in uns.DefaultIfEmpty()
                       where d.school_id == 0 || d.school_id == 2
                       select new
                       {
                           d.unique_id,
                           d.six_months_medicine,
                           d.school_id,
                           d.is_toylet,
                           d.children_name,
                           d.father_name,
                           d.gender,
                           d.age,
                           d.mother_name,
                           villagName = jtResult.name,
                           unionName = unsresult.name

                       };

            // var data = db.ChildrenList_Data.AsQueryable();

            var obj = new List<ChildrenEntity>();
            foreach (var entity in data)
            {
                string gender_text = "";
                if (entity.gender == "1")
                {
                    gender_text = "Male";
                }
                else if (entity.gender == "0")
                {
                    gender_text = "Female";
                }
                else
                {
                    gender_text = entity.gender;
                }

                string isSchool = "";
                if (entity.school_id == null || entity.school_id == 0)
                {
                    isSchool = "Not Going";
                }
                else
                {
                    isSchool = "Going";
                }
                string viname = "";


                string unionname = "";


                obj.Add(new ChildrenEntity()
                {

                    ChildrenId = entity.unique_id,
                    ChildrenName = entity.children_name,
                    FatherName = entity.father_name,
                    MotherName = entity.mother_name,
                    ChildrenAge = entity.age.ToString(),
                    Gender = gender_text,
                    SchoolGoing = isSchool,
                    VillageName = entity.villagName,
                    UnionName = entity.unionName,

                });
            }






            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Report"), "rptTotalOutOfSchoolGoing.rpt"));
            rd.SetDataSource(obj.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            try
            {
                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "Application/pdf", "TotalOutOfSchoolGoing.pdf");
            }
            catch
            {
                throw;
            }



        }

        public ActionResult ExportToExcelForDeworming()
        {



            var data = from d in db.ChildrenList_Data
                         join ve in db.Village_Data on d.village_id equals ve.id.ToString() into jts
                         from jtResult in jts.DefaultIfEmpty()

                       join un in db.Union_Data on d.union_id equals un.id.ToString() into uns
                       from unsresult in uns.DefaultIfEmpty()
                       where d.six_months_medicine==1
                       select new
                         {
                             d.unique_id,
                             d.six_months_medicine,
                             d.school_id,
                             d.is_toylet,
                             d.children_name,
                             d.father_name,
                             d.gender,
                             d.age,
                             d.mother_name,
                             villagName= jtResult.name,
                             unionName= unsresult.name

                       };
            
              // var data = db.ChildrenList_Data.AsQueryable();

            var obj = new List<ChildrenEntity>();
            foreach (var entity in data)
            {
                string gender_text = "";
                if (entity.gender == "1")
                {
                    gender_text = "Male";
                }
                else if (entity.gender == "0")
                {
                    gender_text = "Female";
                }
                else
                {
                    gender_text = entity.gender;
                }

                string isSchool = "";
                if (entity.school_id == null || entity.school_id == 0)
                {
                    isSchool = "Not Going";
                }
                else
                {
                    isSchool = "Going";
                }
                string viname = "";
               

                string unionname = "";
               

                obj.Add(new ChildrenEntity()
                {

                    ChildrenId = entity.unique_id,
                    ChildrenName = entity.children_name,
                    FatherName = entity.father_name,
                    ChildrenAge = entity.age.ToString(),
                    Gender = gender_text,
                    SchoolGoing = isSchool,
                    VillageName = entity.villagName,
                    UnionName = entity.unionName,

                });
            }


            GridView gv = new GridView();
            gv.DataSource = obj.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            string strDateFormat = string.Empty;
            strDateFormat = string.Format("{0:yyyy-MMM-dd-hh-mm-ss}", DateTime.Now);
            Response.AddHeader("content-disposition", "attachment; filename=UserDetails_" + strDateFormat + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return RedirectToAction("Index", "WebDewarmingReport");
        }
     

    }
}