﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class ChildrenListBySkController : ApiController
    {
        private MySqlCon db = new MySqlCon();

        // GET: api/ChildrenListBySk
        public IEnumerable<string> Get()
        {
            return new string[] { "empty" };
        }

        // GET: api/contact/5
        public object Get(int id)
        {
            var Query = db.ChildrenList_Data.Where(c => c.id == id);
            var details = Query.FirstOrDefault<ChildrenListModel>();

            string gender_text, school_name, class_name, drinking_water_name, six_months_medicine_text, is_toylet_text;

            //string from_details_html = db.InsForm_Data.Where(_i => _i.ID == 2).FirstOrDefault().RAW_CODE;

            string from_details_html = TemplateDetails.Header();

            from_details_html += "<input type='hidden' id='chilId' value='" + id + "'>";

            from_details_html += "<div class='container' style='background: #fd375d; height: 110px;'><div class='center-block' style='height: 50px; margin-top: 25px'><div class='text-center' style='color:#ffff; padding-top: 15px;'>" + details.children_name + "</div></div></div><div id='btn' class='text-center dot' style='background: #fabe0a; margin-left: 40%; margin-top: -32px;padding-top: 15px; margin-bottom: 5px;'><div style='color: #ffff;font-size:11px;'>ঔষধ খাওয়ান</div></div>";

            from_details_html += "<table class='table text-center'><thead></thead><tbody>";

            var dosDetails = db.DoseList_Data.Where(i_ => i_.children_id == details.id).ToList();
            if (dosDetails != null)
            {
                foreach (var en in dosDetails)
                {
                    from_details_html += "<tr><td>";
                    from_details_html += en.date.ToString("dd-MM-yyyy") + " তারিখে কৃমিনাশক বড়ি খেয়েছে";
                    from_details_html += "</td></tr>";
                }
            }

            from_details_html += "</tbody></table>";

            from_details_html += "<div class='form-group'><div class='col-xs-12 text-center'>";
            from_details_html += "</div><div class='clearfix'></div></div>";

            if (details != null)
            {
                from_details_html += TemplateDetails.TableStart();

                if (details.gender == "1")
                {
                    gender_text = "Male";
                }
                else if (details.gender == "0")
                {
                    gender_text = "Female";
                }
                else
                {
                    gender_text = details.gender;
                }

                var schoolDetails = db.SchoolList_Data.Where(_i => _i.id == details.school_id).FirstOrDefault();
                if (schoolDetails != null)
                {
                    school_name = schoolDetails.school_name;
                }
                else
                {
                    school_name = "";
                }
                class_name = details.class_id;
                drinking_water_name = db.DrinkingWaterList_Data.Where(_i => _i.id == details.drinking_water_id).FirstOrDefault().name;

                if (details.six_months_medicine == 1)
                {
                    six_months_medicine_text = "Yes";
                }
                else
                {
                    six_months_medicine_text = "No";
                }

                if (details.is_toylet == 1)
                {
                    is_toylet_text = "Yes";
                }
                else
                {
                    is_toylet_text = "No";
                }

                from_details_html += "<tr><td>নং</td><td>" + details.unique_id + "</td></tr>";
                from_details_html += "<tr><td>১. খানা নং</td><td>" + details.hh + "</td></tr>";
                from_details_html += "<tr><td>২. খানা সদস্য সংখ্যা</td><td>" + details.household_member.ToString() + "</td></tr>";
                from_details_html += "<tr><td>৩. শিশুর নাম</td><td>" + details.children_name + "</td></tr>";
                from_details_html += "<tr><td>৪. মাতার নাম</td><td>" + details.mother_name + "</td></tr>";
                from_details_html += "<tr><td>৫. পিতার নাম</td><td>" + details.father_name + "</td></tr>";
                from_details_html += "<tr><td>৬. বয়স</td><td>" + details.age.ToString() + "</td></tr>";
                from_details_html += "<tr><td>৭. লিঙ্গ</td><td>" + gender_text + "</td></tr>";
                from_details_html += "<tr><td>৮. প্রতিষ্ঠানের</td><td>" + school_name + "</td></tr>";
                from_details_html += "<tr><td>৯. শ্রেণী</td><td>" + class_name + "</td></tr>";
                from_details_html += "<tr><td>১০. গত ৬ মাসে কৃমিনাশক বড়ি খেয়েছে কি না ?</td><td>" + six_months_medicine_text + "</td></tr>";
                from_details_html += "<tr><td>১১. স্যানিটারি টয়লেট আছে কিনা ?</td><td>" + is_toylet_text + "</td></tr>";
                from_details_html += "<tr><td>১২. খাবার পানির উৎস</td><td>" + drinking_water_name + "</td></tr>";

                from_details_html += TemplateDetails.TableEnd();

                from_details_html += TemplateDetails.Footer();

                var data = new
                {
                    success = true,
                    result = new[]
                    {
                        new { name = "১. খানা নং", value = details.hh },
                        new { name = "২. খানা সদস্য সংখ্যা", value = details.household_member },
                        new { name = "৩. শিশুর নাম", value = details.children_name },
                        new { name = "৪. মাতার নাম", value=details.mother_name },
                        new { name = "৫. পিতার নাম", value=details.father_name },
                        new { name = "৬. বয়স", value=details.age.ToString() },
                        new { name = "৭. লিঙ্গ", value=gender_text },
                        new { name = "৮. প্রতিষ্ঠানের নাম", value=school_name },
                        new { name = "৯. শ্রেণী", value=class_name },
                        new { name = "১০. গত ৬ মাসে কৃমিনাশক বড়ি খেয়েছে কি না ?", value=six_months_medicine_text },
                        new { name = "১১. স্যানিটারি টয়লেট আছে কিনা ?", value=is_toylet_text },
                        new { name = "১২. খাবার পানির উৎস", value=drinking_water_name }
                    },
                    from_details = from_details_html
                };
                return data;
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "Khana Details not found!"
                };
                return data;
            }
        }


        // POST: api/contact
        public object Post([FromBody]FormDataCollection formbody)
        {         

            string skId =formbody.GetValues("sk_id")[0];
            

            var results = new List<ChildrenListModel>();
            IEnumerable<ChildrenListModel> cld;
           
            if(skId!="")
            {
                int _skid = int.Parse(skId);
                cld = db.ChildrenList_Data.Where(i=>i.sk_id== _skid).OrderByDescending(_i => _i.id);
                try
            {
                foreach (var item in cld)
                {
                    var view = new ChildrenListModel();
                    view = item;

                    results.Add(view);
                }

            }
            catch (Exception ex) { }
            }

            string gender_text, school_name, class_name, drinking_water_name;

            if (results.Count() != 0)
            {
                var obj = new KhanaListJson();
                foreach (var entity in results)
                {
                    if (entity.gender == "1")
                    {
                        gender_text = "Male";
                    }
                    else if (entity.gender == "0")
                    {
                        gender_text = "Female";
                    }
                    else
                    {
                        gender_text = entity.gender;
                    }

                    var schoolDetails = db.SchoolList_Data.Where(_i => _i.id == entity.school_id).FirstOrDefault();
                    if (schoolDetails != null)
                    {
                        school_name = schoolDetails.school_name;
                    }
                    else
                    {
                        school_name = "";
                    }
                    class_name = entity.class_id;
                    var drinking_water_details = db.DrinkingWaterList_Data.Where(_i => _i.id == entity.drinking_water_id).FirstOrDefault();
                    if (drinking_water_details != null)
                    {
                        drinking_water_name = drinking_water_details.name;
                    }
                    else
                    {
                        drinking_water_name = "";
                    }

                    int dose = db.DoseList_Data.Where(i_ => i_.children_id == entity.id).Count();
                    int visitCount = db.ChildrenLog_Data.Where(i_ => i_.c_id == entity.id && i_.status == 3).Count();

                    DateTime today = DateTime.Now.Date;
    
                    var visitDetails = db.ChildrenLog_Data.Where(i_ => i_.c_id == entity.id && i_.udtae >= today && i_.udtae <= today).FirstOrDefault();
                    int todayVisit = 0;
                    if(visitDetails != null)
                    {
                        todayVisit = 1;
                    }

                    obj.result.Add(new KhanaListListJson
                    {
                        id = entity.id,
                        khana_no = entity.hh,
                        khana_member = entity.household_member,
                        children_name = entity.children_name,
                        mother_name = entity.mother_name,
                        father_name = entity.father_name,
                        age = entity.age.ToString(),
                        gender = entity.gender,
                        gender_text = gender_text,
                        school_id = entity.school_id,
                        school_name = school_name,
                        class_id = entity.class_id,
                        class_name = class_name,
                        six_months_medicine = entity.six_months_medicine,
                        is_toylet = entity.is_toylet,
                        drinking_water_id = entity.drinking_water_id,
                        drinking_water_name = drinking_water_name,
                        dose = dose.ToString(),
                        status = entity.status,
                        visit_count = visitCount.ToString(),
                        today_visit = todayVisit,
                        dev = today.ToString()
                        //created_at = entity.created_at.ToString("dd MMM yyyy"),
                        //bupdated_at = entity.updated_at.ToString("dd MMM yyyy")
                    });
                }

                var data = new
                {
                    success = true,
                    count = results.Count(),
                    obj.result
                };
                return data;
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "Khana list not found!"
                };
                return data;
            }

        }


        // PUT: api/contact/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/contact/5
        public void Delete(int id)
        {
        }

    }


    //public class KhanaListJson
    //{
    //    public KhanaListJson() { result = new List<KhanaListListJson>(); }
    //    public List<KhanaListListJson> result { get; set; }
    //}
    //public class KhanaListListJson
    //{
    //    public int id { get; set; }
    //    public string khana_no { get; set; }
    //    public string khana_member { get; set; }
    //    public string children_name { get; set; }
    //    public string mother_name { get; set; }
    //    public string father_name { get; set; }
    //    public string age { get; set; }
    //    public string gender { get; set; }
    //    public string gender_text { get; set; }
    //    public int school_id { get; set; }
    //    public string school_name { get; set; }
    //    public string class_id { get; set; }
    //    public string class_name { get; set; }
    //    public int six_months_medicine { get; set; }
    //    public int is_toylet { get; set; }
    //    public int drinking_water_id { get; set; }
    //    public string drinking_water_name { get; set; }
    //    public string dose { get; set; }
    //    public int status { get; set; }
    //    public string created_at { get; set; }
    //    public string updated_at { get; set; }
    //}


}

