﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data.Entity;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace BracProject.Controllers
{
    public class WebChildrenController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index2(int id)
        {
            Session["ssid"] = id.ToString();
            return View();
        }
        public FileResult DownloadAsPDF(int id)
        {
            try
            {
                string html_code = InspectionReportGenWeb.ReportGen(id);
                
         using (MemoryStream ms = new MemoryStream())
                {
                    var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html_code, PdfSharp.PageSize.A4);
                    pdf.Save(ms);                   
                    return File(ms.ToArray(), "application/pdf", "" + id + ".pdf");
                }
               
            } catch (Exception ex) { return null; }
        }
        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            string name = "", children_name = "", father_name = "", age = "", gender = "";

            if (!String.IsNullOrEmpty(search))
            {
                string[] isFilter = search.Split(';');
                if (isFilter.Count() > 1)
                {
                    search = "";
                    foreach (var a in isFilter)
                    {
                        string[] filter = a.Split(':');
                        if (filter[0] == "cn")
                        {
                            children_name = filter[1];
                        }
                        if (filter[0] == "fn")
                        {
                            father_name = filter[1];
                        }
                        if (filter[0] == "age")
                        {
                            age = filter[1];
                        }
                        if (filter[0] == "gender")
                        {
                            gender = filter[1];
                        }
                    }
                }
                else
                {
                    name = search;
                }
            }

            var query = db.ChildrenList_Data.AsQueryable();

            if (!String.IsNullOrEmpty(name))
            {
                query = query.Where(w => w.unique_id.Contains(name));
            }
            if (!String.IsNullOrEmpty(children_name))
            {
                query = query.Where(w => w.children_name.Contains(children_name));
            }
            if (!String.IsNullOrEmpty(father_name))
            {
                query = query.Where(w => w.father_name.Contains(father_name));
            }
            if (!String.IsNullOrEmpty(age))
            {
                //query = query.Where(w => w.age.Contains(age));
            }
            if (!String.IsNullOrEmpty(gender))
            {
                query = query.Where(w => w.gender == gender);
            }


          //  query = query.Where(w => w.status == 1);

            var results = new List<ChildrenListModel>();
            if (search == null || search == "")
            {
                totalRecord = query.Count();
                query = query.OrderByDescending(c => c.id).Skip(start).Take(take);
                results = query.ToList<ChildrenListModel>();
            }
            else
            {
                totalRecord = db.ChildrenList_Data.Where(c => c.unique_id.Contains(search) && c.status == 1).Count();
                var Query = db.ChildrenList_Data.Where(c => c.unique_id.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<ChildrenListModel>();
            }
            string schoolVerification = string.Empty;

            var obj = new ChildrenWebJson();
            foreach (var entity in results)
            {
                string gender_text = "";
                if (entity.gender == "1")
                {
                    gender_text = "Male";
                }
                else if (entity.gender == "0")
                {
                    gender_text = "Female";
                }
                else
                {
                    gender_text = entity.gender;
                }

                string isSchool = "";
                if (entity.school_id == null || entity.school_id == 0)
                {
                    isSchool = "Not Going";
                }
                else
                {
                    isSchool = "Going";
                }

                if(entity.school_verification==1)
                {
                    schoolVerification = "Yes";
                }
                else
                {
                    schoolVerification = "No";
                }
                var schoolName = new SchoolListModel();
                if (entity.school_id!=0)
                {
                    schoolName = db.SchoolList_Data.Where(sc => sc.id == entity.school_id).FirstOrDefault();
                }

                string schoolN = "";
                if(schoolName!=null)
                {
                    schoolN = schoolName.school_name;
                }

                var union_Name = new UnionModel();
                if(entity.union_id!="0")
                {
                    int unid = Convert.ToInt32(entity.union_id);
                    union_Name = db.Union_Data.Where(un => un.id == unid).FirstOrDefault();
                }
                string unName = "";
                if (union_Name!=null)
                {
                    unName = union_Name.name;
                }
               
                var villege_Name = new VillageModel();
                if (entity.village_id != "0")
                {
                    int unid = Convert.ToInt32(entity.village_id);
                    villege_Name = db.Village_Data.Where(un => un.id == unid).FirstOrDefault();
                }
                string villName = "";
                if(villege_Name!=null)
                {
                    villName = villege_Name.name;
                }


                obj.data.Add(new ChildrenWebListJson
                {

                    unique_id = entity.unique_id,
                    children_name = entity.children_name,
                    father_name = entity.father_name,
                    children_age = entity.age.ToString(),
                    gender = gender_text,
                    school_going = isSchool,
                    villegeName = villName,
                    unionName = unName,//union_Name.name,
                    householdMember = entity.household_member,
                    childrenNumber = entity.child_num,
                    schoolName = schoolN,
                    school_verification=schoolVerification,
                   
                    takenMedicinePlace=entity.taken_medicine_place,
                    mother_name=entity.mother_name,
                   
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                                "<a style = 'width:10px;padding-left:10px;' href='/children_list/Reportview/" + entity.id + "' class='btn btn-icon btn-primary btn-square'>" +
                                "<i class='icon-eye'></i>" +
                                "</a>" +
                                "<a style = 'width:10px;padding-left:10px;' href='/children_list/DownloadAsPDF/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                                "<i class='icon-file-pdf'></i>" +
                                "</a>" +
                                 "<a href='/children_list/Edit/" + entity.id + "' class='btn btn-icon btn-warning btn-square'>" +
                                  "<i class='icon-edit'></i>" +
                                  "</a>" +
                              "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            Session["model"] = data;
            return Json(data, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult ListById(DataTableRequest req)
        {
            int ssid = Convert.ToInt32(Session["ssid"].ToString());
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            string name = "", children_name = "", father_name = "", age = "", gender = "";

            if (!String.IsNullOrEmpty(search))
            {
                string[] isFilter = search.Split(';');
                if (isFilter.Count() > 1)
                {
                    search = "";
                    foreach (var a in isFilter)
                    {
                        string[] filter = a.Split(':');
                        if (filter[0] == "cn")
                        {
                            children_name = filter[1];
                        }
                        if (filter[0] == "fn")
                        {
                            father_name = filter[1];
                        }
                        if (filter[0] == "age")
                        {
                            age = filter[1];
                        }
                        if (filter[0] == "gender")
                        {
                            gender = filter[1];
                        }
                    }
                }
                else
                {
                    name = search;
                }
            }

            var query = db.ChildrenList_Data.AsQueryable();

            if (!String.IsNullOrEmpty(name))
            {
                query = query.Where(w => w.unique_id.Contains(name));
            }
            if (!String.IsNullOrEmpty(children_name))
            {
                query = query.Where(w => w.children_name.Contains(children_name));
            }
            if (!String.IsNullOrEmpty(father_name))
            {
                query = query.Where(w => w.father_name.Contains(father_name));
            }
            if (!String.IsNullOrEmpty(age))
            {
                //query = query.Where(w => w.age.Contains(age));
            }
            if (!String.IsNullOrEmpty(gender))
            {
                query = query.Where(w => w.gender == gender);
            }


            //  query = query.Where(w => w.status == 1);

            var results = new List<ChildrenListModel>();
            if (search == null || search == "")
            {
                totalRecord = query.Count();
                query = query.Where(ch=>ch.ss_id==ssid).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = query.ToList<ChildrenListModel>();
            }
            else
            {
                totalRecord = db.ChildrenList_Data.Where(c => c.unique_id.Contains(search) && c.status == 1).Count();
                var Query = db.ChildrenList_Data.Where(c => c.ss_id==ssid && c.unique_id.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<ChildrenListModel>();
            }
            string schoolVerification = string.Empty;

            var obj = new ChildrenWebJson();
            foreach (var entity in results)
            {
                string gender_text = "";
                if (entity.gender == "1")
                {
                    gender_text = "Male";
                }
                else if (entity.gender == "0")
                {
                    gender_text = "Female";
                }
                else
                {
                    gender_text = entity.gender;
                }

                string isSchool = "";
                if (entity.school_id == null || entity.school_id == 0)
                {
                    isSchool = "Not Going";
                }
                else
                {
                    isSchool = "Going";
                }

                if (entity.school_verification == 1)
                {
                    schoolVerification = "Yes";
                }
                else
                {
                    schoolVerification = "No";
                }
                var schoolName = new SchoolListModel();
                if (entity.school_id != 0)
                {
                    schoolName = db.SchoolList_Data.Where(sc => sc.id == entity.school_id).FirstOrDefault();
                }

                string schoolN = "";
                if (schoolName != null)
                {
                    schoolN = schoolName.school_name;
                }

                var union_Name = new UnionModel();
                if (entity.union_id != "0")
                {
                    int unid = Convert.ToInt32(entity.union_id);
                    union_Name = db.Union_Data.Where(un => un.id == unid).FirstOrDefault();
                }
                string unName = "";
                if (union_Name != null)
                {
                    unName = union_Name.name;
                }

                var villege_Name = new VillageModel();
                if (entity.village_id != "0")
                {
                    int unid = Convert.ToInt32(entity.village_id);
                    villege_Name = db.Village_Data.Where(un => un.id == unid).FirstOrDefault();
                }
                string villName = "";
                if (villege_Name != null)
                {
                    villName = villege_Name.name;
                }


                obj.data.Add(new ChildrenWebListJson
                {

                    unique_id = entity.unique_id,
                    children_name = entity.children_name,
                    father_name = entity.father_name,
                    children_age = entity.age.ToString(),
                    gender = gender_text,
                    school_going = isSchool,
                    villegeName = villName,
                    unionName = unName,//union_Name.name,
                    householdMember = entity.household_member,
                    childrenNumber = entity.child_num,
                    schoolName = schoolN,
                    school_verification = schoolVerification,

                    takenMedicinePlace = entity.taken_medicine_place,
                    mother_name = entity.mother_name,

                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                                "<a style = 'width:10px;padding-left:10px;' href='/children_list/Reportview/" + entity.id + "' class='btn btn-icon btn-primary btn-square'>" +
                                "<i class='icon-eye'></i>" +
                                "</a>" +
                                "<a style = 'width:10px;padding-left:10px;' href='/children_list/GetPdf/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                                "<i class='icon-file-pdf'></i>" +
                                "</a>" +
                                 "<a href='/children_list/Edit/" + entity.id + "' class='btn btn-icon btn-warning btn-square'>" +
                                  "<i class='icon-edit'></i>" +
                                  "</a>" +
                              "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            Session["model"] = data;
            return Json(data, JsonRequestBehavior.AllowGet);

        }
     
            [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.ChildrenList_Data.Where(c => c.id == id).FirstOrDefault();
            Session["Chidren_id_k"] = cont.id;
            Session["ss_id_k"] = cont.ss_id;
            return View(cont);
        }

        [HttpPost]
        public ActionResult Edit(ChildrenListModel cont)
        {


            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.Entry(cont).Property(x => x.status).IsModified = false;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpPost]
        public ActionResult GetSchoolList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var div_list = db.SchoolList_Data.OrderBy(d => d.status==1).ToList<SchoolListModel>();
            foreach (var entity in div_list)
            {
                items.Add(new SelectListItem { Text = entity.school_name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetClassList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var div_list = db.ClassList_Data.OrderBy(d => d.id).ToList<ClassListModel>();
            foreach (var entity in div_list)
            {
                items.Add(new SelectListItem { Text = entity.class_name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetUnion(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var div_list = db.Union_Data.OrderBy(d =>d.ISACTIVE==1).ToList<UnionModel>();
            foreach (var entity in div_list)
            {
                items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetVillage(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var div_list = db.Village_Data.OrderBy(d => d.ISACTIVE==1).ToList<VillageModel>();
            foreach (var entity in div_list)
            {
                items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSSList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            //var div_list = db.SS_Data.OrderBy(d => d.status == 1).ToList<SSModel>();
            var div_list = db.Member_Data.Where(ii_ => ii_.type == 3).OrderBy(d => d.name).ToList();
            foreach (var entity in div_list)
            {
                items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Reportview(int id)
        {

            string html_code = InspectionReportGenWeb.ReportGen(id);
            ViewBag.Details = html_code;
            return View();
        }

        public ActionResult ExportToExcel()
        {

            var data = db.ChildrenList_Data.AsQueryable();

            //string schoolVerification = string.Empty;

            //var obj = new List<ChildrenList>();
            //int i = 0;
            //foreach (var entity in data)
            //{
               
            //    i++;
            //    if (i == 4299)
            //    {
            //        string ss = "";
            //    }
            //    string gender_text = "";
            //    if (entity.gender == "1")
            //    {
            //        gender_text = "Male";
            //    }
            //    else if (entity.gender == "0")
            //    {
            //        gender_text = "Female";
            //    }
            //    else
            //    {
            //        gender_text = entity.gender;
            //    }

            //    string isSchool = "";
            //    if (entity.school_id == null || entity.school_id == 0)
            //    {
            //        isSchool = "Not Going";
            //    }
            //    else
            //    {
            //        isSchool = "Going";
            //    }
            //    string viname = "";
            //    if (entity.village_id != "0" && entity.village_id!="")
            //    {

            //        int villageid = Convert.ToInt32(entity.village_id);
            //        db = new MySqlCon();
            //        var mn = db.Village_Data.Where(xx => xx.id == villageid).FirstOrDefault();
            //        if(mn!=null)
            //        viname = mn.name;
            //    }

            //    string unionname = "";
            //    if (entity.union_id != "0" && entity.union_id!="")
            //    {
            //        int villageid = Convert.ToInt32(entity.union_id);
            //        var mn = db.Union_Data.Where(xx => xx.id == villageid).FirstOrDefault();
            //        if(mn!=null)
            //        unionname = mn.name;
            //    }
            //    var schoolName = new SchoolListModel();
            //    if (entity.school_id != 0)
            //    {
            //        schoolName = db.SchoolList_Data.Where(sc => sc.id == entity.school_id).FirstOrDefault();
            //    }

            //    string schoolN = "";
            //    if (schoolName != null)
            //    {
            //        schoolN = schoolName.school_name;
            //    }
            //    if (entity.school_verification == 1)
            //    {
            //        schoolVerification = "Yes";
            //    }
            //    else
            //    {
            //        schoolVerification = "No";
            //    }
            //    obj.Add(new ChildrenList()
            //    {

            //        unique_id = entity.unique_id,
            //        Children_name = entity.children_name,
            //        Father_name = entity.father_name,
                  
            //        Children_age = entity.age,
            //        Gender = gender_text,
            //        School_going = isSchool,
            //        Village_name = viname,
            //        Union_name = unionname,
            //        childNumber=entity.child_num,
            //        householdMember=entity.household_member,
            //        schoolName=schoolN,
            //        school_verification=schoolVerification,
            //        takenMedicinePlace=entity.taken_medicine_place,
            //        mother_name = entity.mother_name

            //    });
            //}
            var obj = (from u in db.ChildrenList_Data
                        select new ChildrenList()
                        {
                            unique_id = u.unique_id,
                            Children_name = u.children_name,
                            Father_name = u.father_name,

                            Children_age = u.age.ToString(),
                            Gender =(u.gender == "1" ? "Male" : "Female"),
                            School_going = (u.school_id == 0 ? "Not Going" : "Going"),
                            Village_name = (u.village_id!="0" && u.village_id!=""? db.Village_Data.Where(xx => xx.id.ToString() == u.village_id).FirstOrDefault().name : ""),
                            Union_name = (u.union_id != "0" && u.union_id != "" ? db.Union_Data.Where(xx => xx.id.ToString() ==u.union_id).FirstOrDefault().name : ""),
                            childNumber = u.child_num,
                            householdMember = u.household_member,
                            schoolName = (u.school_id !=0 ? db.SchoolList_Data.Where(xx => xx.id == u.school_id).FirstOrDefault().school_name : ""),
                            school_verification = (u.school_verification == 1 ? "Yes" : "No"),
                            takenMedicinePlace = u.taken_medicine_place,
                            mother_name = u.mother_name
                        }).ToList();


            GridView gv = new GridView();
            gv.DataSource = obj.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            string strDateFormat = string.Empty;
            strDateFormat = string.Format("{0:yyyy-MMM-dd-hh-mm-ss}", DateTime.Now);
            Response.AddHeader("content-disposition", "attachment; filename=UserDetails_" + strDateFormat + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return RedirectToAction("Index", "WebChildren");
        }





    }

    public class ChildrenWebJson
    {
        public ChildrenWebJson() { data = new List<ChildrenWebListJson>(); }
        public List<ChildrenWebListJson> data { get; set; }
    }
    public class ChildrenWebListJson
    {
        public string id { get; set; }
        public string unique_id { get; set; }
        public string children_name { get; set; }
        public string father_name { get; set; }
        public string children_age { get; set; }
        public string gender { get; set; }
        public string school_going { get; set; }
        public string school_verification { get; set; }
        public string action { get; set; }

        public string childrenNumber{ get; set; }
        public string householdMember { get; set; }
        public string takenMedicinePlace { get; set; }
        public string villegeName { get; set; }
        public string unionName { get; set; }
        public string schoolName { get; set; }
        public string className { get; set; }
        public string mother_name { get; set; }




    }

    public class ChildrenList
    {

        public string unique_id { get; set; }
        public string Children_name { get; set; }
        public string Father_name { get; set; }
        public string Children_age { get; set; }
        public string Gender { get; set; }
        public string School_going { get; set; }
        public string Village_name { get; set; }

        public string Union_name { get; set; }
        public string childNumber { get; set; }
        public string householdMember { get; set; }
        public string schoolName { get; set; }
        public string school_verification { get; set; }
        public string takenMedicinePlace { get; set; }
        public string mother_name { get; set; }



    }
}
