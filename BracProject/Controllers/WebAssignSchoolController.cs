﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebAssignSchoolController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<AssignSchoolModel>();
            if (search == null || search == "")
            {
                totalRecord = db.AssignSchool_Data.Where(ii_ => ii_.status == 1).Count();
                var Query = db.AssignSchool_Data.Where(ii_ => ii_.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<AssignSchoolModel>();
            }
            else
            {
                totalRecord = db.AssignSchool_Data.Where(c => c.PO.name.Contains(search) && c.status == 1).Count();
                var Query = db.AssignSchool_Data.Where(c => c.PO.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<AssignSchoolModel>();
            }


            var obj = new SchoolAssignWebJson();
            foreach (var entity in results)
            {
                //string PO_NAME = db.Member_Data.Where(ii_ => ii_.id == entity.po_id).FirstOrDefault().name;
                obj.data.Add(new SchoolAssignWebListJson
                {
                    id = entity.id,
                    po_name = entity.PO.name,
                    school_name = entity.School.school_name,
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/school_assign/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(AssignSchoolModel cont)
        {

            if (ModelState.IsValid)
            {
                string strHeader = cont.ids;
                string[] strTempIDS = strHeader.Split(',');
                foreach (var a in strTempIDS)
                {
                    var insert_data = new AssignSchoolModel();
                    insert_data.po_id = cont.po_id;
                    insert_data.school_id = Convert.ToInt32(a);
                    insert_data.status = 1;
                    insert_data.ids = cont.ids;
                    insert_data.created_at = DateTime.Now;
                    insert_data.updated_at = DateTime.Now;
                    db.AssignSchool_Data.Add(insert_data);
                    db.SaveChanges();
                }

                ModelState.Clear();
                cont = null;
                ViewBag.Message = "Assign Successfully";
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.AssignSchool_Data.Where(c => c.id == id).FirstOrDefault();
            Session["po_id"] = cont.po_id;
            return View(cont);
        }

        [HttpPost]
        public ActionResult Edit(AssignSchoolModel cont)
        {

            cont.updated_at = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.Entry(cont).Property(x => x.status).IsModified = false;
                    db.Entry(cont).Property(x => x.created_at).IsModified = false;
                    db.Entry(cont).Property(x => x.ids).IsModified = false;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.AssignSchool_Data.Where(at => at.id == id).SingleOrDefault();
            db.AssignSchool_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult poList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (Session["po_id"] == null)
            {
                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 1).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                }
            }
            else
            {
                int poID = Convert.ToInt32(Session["po_id"]);
                //Session.Remove("po_id");

                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 1).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    if (poID == entity.id)
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSchool(string po_id)
        {
            int DisID = Convert.ToInt32(po_id);
            List<SelectListItem> items = new List<SelectListItem>();
            var upz_list = db.SchoolList_Data.OrderBy(d => d.id).ToList();
            foreach (var entity in upz_list)
            {
                items.Add(new SelectListItem { Text = entity.school_name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

    }

    public class SchoolAssignWebJson
    {
        public SchoolAssignWebJson() { data = new List<SchoolAssignWebListJson>(); }
        public List<SchoolAssignWebListJson> data { get; set; }
    }
    public class SchoolAssignWebListJson
    {
        public int id { get; set; }
        public string po_name { get; set; }
        public string school_name { get; set; }
        public string action { get; set; }
    }
}
