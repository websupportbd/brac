﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class VisitController : ApiController
    {
        private MySqlCon db = new MySqlCon();


        // GET: api/khanalist
        public IEnumerable<string> Get()
        {
            return new string[] { "empty" };
        }

        // GET: api/contact/5
        public object Get(int id)
        {
            return "";
        }

        // POST: api/contact
        public object Post([FromBody]FormDataCollection formbody)
        {
            //string poId = formbody.GetValues("po_id")[0];
            string skId = formbody.GetValues("sk_id")[0];
            //string ssId = formbody.GetValues("ss_id")[0];
            string childrenId = formbody.GetValues("children_id")[0];

            try
            {
                var insert_data = new VisitListModel();
                //insert_data.po_id = Convert.ToInt32(poId);
                insert_data.sk_id = Convert.ToInt32(skId);
                //insert_data.ss_id = Convert.ToInt32(ssId);
                insert_data.children_id = Convert.ToInt32(childrenId);
                insert_data.visit_date = DateTime.Now;
                db.Visit_Data.Add(insert_data);
                int inspec_data_sucess = db.SaveChanges();

                var log_insert = new ChildrenLogModel();
                log_insert.sk_id = Convert.ToInt32(skId);
                log_insert.c_id = Convert.ToInt32(childrenId);
                log_insert.udtae = DateTime.Now;
                log_insert.status = 3;
                db.ChildrenLog_Data.Add(log_insert);
                int log_id = db.SaveChanges();

                var data = new
                {
                    success = true,
                    message = "visit sussfully!"
                };
                return data;
            } catch (Exception ex)
            {
                var data = new
                {
                    success = false,
                    message = ex.Message
                };
                return data;
            }
        }

        // PUT: api/contact/5
        public void Put(int id, [FromBody]string value)
        {

        }

        // DELETE: api/contact/5
        public void Delete(int id)
        {

        }
    }
}
