﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using BracProject.Models;
using Newtonsoft.Json;

namespace BracProject.Controllers
{

	public class InspectionsavedataController : ApiController
	{
		private MySqlCon db = new MySqlCon();

		int user_id, project_id, khana_id, contact_id, quality_from, scheme_id, form_id, inspection_data_id, type, dis, div, upz, des, off;
		string _lat, _long, raw_data, quality_data, loc_name, loc_lat, loc_long, imageRemarks;
		bool is_data_save = false;

		[Route("api/ispectionsavedata")]
		[HttpPost]
		public async Task<HttpResponseMessage> PostFile()
		{
			// Check if the request contains multipart/form-data.
			if (!Request.Content.IsMimeMultipartContent())
			{
				throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
			}

			string root = HttpContext.Current.Server.MapPath(string.Format("~/{0}", "Storage"));
			var provider = new CustomMultipartFormDataStreamProvider(root);

			try
			{
				StringBuilder sb = new StringBuilder(); // Holds the response body

				// Read the form data and return an async task.
				await Request.Content.ReadAsMultipartAsync(provider);

				// This illustrates how to get the form data. Inspection Data
				foreach (var key in provider.FormData.AllKeys)
				{
					foreach (var val in provider.FormData.GetValues(key))
					{
						if (key == "user_id") { user_id = Convert.ToInt32(val); }
						if (key == "khana_id") { khana_id = Convert.ToInt32(val); }
                        if (key == "div") { div = Convert.ToInt32(val); }
                        if (key == "dis") { dis = Convert.ToInt32(val); }
                        if (key == "upz") { upz = Convert.ToInt32(val); }
                        
						if (key == "loc_name") { loc_name = val; }
						if (key == "loc_lat") { loc_lat = val; }
						if (key == "loc_long") { loc_long = val; }                  
					}
				}

				var transaction = db.Database.BeginTransaction();


				// Inspection Data save
				var insert_inspec_data = new InspectiondataModel();
				insert_inspec_data.USER_ID = user_id;
                insert_inspec_data.DIV = div;
                insert_inspec_data.DIS = dis;
                insert_inspec_data.UPZ = upz;            
                int quality_scheme_id = scheme_id;
				insert_inspec_data.LOCATION = loc_name;
				insert_inspec_data.LAT = loc_lat;
				insert_inspec_data.LON = loc_long;            
				insert_inspec_data.CREATED_AT = DateTime.Now.Date;
				db.Inspectiondata_Data.Add(insert_inspec_data);
				int inspec_data_sucess = db.SaveChanges();
				inspection_data_id = insert_inspec_data.ID;
				if (inspec_data_sucess != 0) { is_data_save = true; } else { is_data_save = false; }



				// This illustrates how to get the form data. Inspection Details
				int insp_data_count = 1;
				foreach (var key in provider.FormData.AllKeys)
				{
					foreach (var val in provider.FormData.GetValues(key))
					{
						if (key == "form_id" + insp_data_count)
						{
							form_id = Convert.ToInt32(val);
						}
						if (key == "raw_data" + insp_data_count)
						{
							raw_data = val;                     
							if (form_id == 1)
                            {
								bool breakdown = BreakDownHtmlController.Registration(raw_data);
                            }
							if (form_id == 2)
                            {
								bool breakdown = BreakDownHtmlController.KhanaVisit(raw_data, khana_id);
                            }
						}
					}

					if (form_id != 0 && raw_data != null)
					{
						var insert_inspec_details = new InspectiondetailsModel();
						insert_inspec_details.INSPECTION_ID = inspection_data_id;
						insert_inspec_details.FORM_ID = form_id;
                        insert_inspec_details.JSON_DATA = "";
                        insert_inspec_details.RAW_CODE = raw_data;
                        insert_inspec_details.CREATED_AT = DateTime.Now;
						db.Inspectiondetails_Data.Add(insert_inspec_details);
						int inspec_details_sucess = db.SaveChanges();
						insp_data_count = insp_data_count + 1;
						if (insp_data_count != 0) { is_data_save = true; } else { is_data_save = false; }
						form_id = 0;
						raw_data = null;
					}
				}
                            

				if (is_data_save == true)
				{
					transaction.Commit();
					var data = new
					{
						success = true,
						message = "Data upload successfully!"
					};
					return Request.CreateResponse(data);
				}
				else
				{
					transaction.Rollback();
					var data = new
					{
						success = false,
						message = "Data upload faild!"
					};
					return Request.CreateResponse(data);
				}
			}
			catch (System.Exception e)
			{
				//return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
				var data = new
				{
					success = false,
					message = "Upload faild!",
					details = e
				};
				return Request.CreateResponse(data);
			}
		}
	}

	public class CustomMultipartFormDataStreamProvider
        : MultipartFormDataStreamProvider
	{
		public CustomMultipartFormDataStreamProvider(string path)
			: base(path)
		{
		}

		public override string GetLocalFileName(HttpContentHeaders headers)
		{
			//return headers.ContentDisposition.FileName.Trim('\"');
			string file_name = headers.ContentDisposition.FileName.Trim('\"');
			string[] words = file_name.TrimStart('.').Split('.');
			if (words[1] == "mp4")
			{
				return String.Format(CultureInfo.InvariantCulture, "BodyPart_{0}{1}", Guid.NewGuid(), ".mp4");
			}
			else
			{
				return String.Format(CultureInfo.InvariantCulture, "BodyPart_{0}{1}", Guid.NewGuid(), ".jpg");
			}

		}
	}

}


