﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebDistrictController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<DistrictModel>();
            if (search == null || search == "")
            {
                totalRecord = db.District_Data.Count();
                var Query = db.District_Data.OrderByDescending(c => c.ID).Skip(start).Take(take);
                results = Query.ToList();
            }
            else
            {
                totalRecord = db.District_Data.Where(c => c.NAME.Contains(search)).Count();
                var Query = db.District_Data.Where(c => c.NAME.Contains(search)).OrderByDescending(c => c.ID).Skip(start).Take(take);
                results = Query.ToList();
            }

            var obj = new DistrictWebJson();
            foreach (var entity in results)
            {

                string action = null;
                action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/district/Edit/" + entity.ID + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.ID + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>";

                var status = "";
                if (entity.ISACTIVE == 1)
                {
                    status = "Active";
                }
                else
                {
                    status = "Inactive";
                }

                obj.data.Add(new DistrictWebListJson
                {
                    id = entity.ID,
                    division = entity.Division.NAME,
                    name = entity.NAME,
                    Status=status,
                    action = action
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }





        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Add(DistrictModel cont)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.District_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "District add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Session["dis_id"] = id;
            var cont = db.District_Data.Where(c => c.ID == id).FirstOrDefault();
            return View(cont);
        }


        [HttpPost]
        public ActionResult Edit(DistrictModel cont)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "District update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.District_Data.Where(at => at.ID == id).SingleOrDefault();
            db.District_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDivision(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var div_list = db.Division_Data.OrderBy(d => d.ID).ToList<DivisionModel>();
            foreach (var entity in div_list)
            {
                items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

    }


    public class DistrictWebJson
    {
        public DistrictWebJson() { data = new List<DistrictWebListJson>(); }
        public List<DistrictWebListJson> data { get; set; }
    }
    public class DistrictWebListJson
    {
        public int id { get; set; }
        public string division { get; set; }
        public string name { get; set; }
        public string Status { get; set; }
        public string action { get; set; }
    }
}
