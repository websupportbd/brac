﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using BracProject.Models;

namespace BracProject.Controllers
{
	public class SkListController : ApiController
	{
		private MySqlCon db = new MySqlCon();

		// GET: api/khanalist
		public IEnumerable<string> Get()
		{
			return new string[] { "empty" };
		}

		// GET: api/contact/5
		public object Get(int id)
		{
            return "";       
		}

		// POST: api/contact
        public object Post([FromBody]FormDataCollection formbody)
		{
            //int userid = Convert.ToInt32(formbody.GetValues("user_id")[0]);
		

			//var results = db.SK_Data.OrderByDescending(_i => _i.id).ToList();
			var results = db.AssignSk_Data.OrderByDescending(_i => _i.id).ToList();

            if (results.Count() != 0)
			{
                var obj = new SkListJson();
                foreach (var entity in results)
                {

                    obj.result.Add(new SkListListJson
                    {
                        id = entity.sk_id,
                        sk_name = entity.SK.name
                    });
                }

				var data = new
                {
                    success = true,
                    count = results.Count(),
                    obj.result
				};
				return data;
			}
			else
			{
				var data = new
				{
					success = false,
					message = "Sk list not found!"
				};
				return data;
			}

		}

		// PUT: api/contact/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE: api/contact/5
		public void Delete(int id)
		{
		}
	}

	public class SkListJson
	{
		public SkListJson() { result = new List<SkListListJson>(); }
		public List<SkListListJson> result { get; set; }
	}
	public class SkListListJson
	{
		public int id { get; set; }
        public string sk_name { get; set; }
	}


}

