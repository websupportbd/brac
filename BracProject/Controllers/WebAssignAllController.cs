﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebAssignAllController : Controller
    {

        private MySqlCon db = new MySqlCon();
        // GET: WebAssignAll
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddPo()
        {
            return View();
        }

        // GET: WebAssignAll/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: WebAssignAll/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WebAssignAll/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: WebAssignAll/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: WebAssignAll/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: WebAssignAll/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: WebAssignAll/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
