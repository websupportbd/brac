﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebStaffReportController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View ();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            string skID = "";
            string Fromdate = "";
            string Todate = "";

            if (!String.IsNullOrEmpty(search))
            {
                string[] isFilter = search.Split(';');
                if (isFilter.Count() > 1)
                {
                    search = "";
                    foreach (var a in isFilter)
                    {
                        string[] filter = a.Split(':');
                        if (filter[0] == "sk")
                        {
                            skID = filter[1];
                        }
                        if (filter[0] == "from_date")
                        {
                            Fromdate = filter[1];
                        }
                        if (filter[0] == "to_date")
                        {
                            Todate = filter[1];
                        }
                    }
                }
            }

            int skID_INT = Convert.ToInt32(skID);
            DateTime cFromDate = Convert.ToDateTime(Fromdate);
            DateTime cToDate = Convert.ToDateTime(Todate);

            var results = new List<ChildrenLogModel>();
            totalRecord = db.ChildrenLog_Data.Where(ii_ => ii_.sk_id == skID_INT && ii_.udtae >= cFromDate && ii_.udtae <= cToDate).Count();
            var Query = db.ChildrenLog_Data.Where(ii_ => ii_.sk_id == skID_INT && ii_.udtae >= cFromDate && ii_.udtae <= cToDate).OrderByDescending(c => c.id).Skip(start).Take(take);
            results = Query.ToList();

            var obj = new cWebJson();
            int s = 1;
            foreach (var entity in results)
            {
                string cName = "";
                string hh = "";
                string ssName = "";
                var cDetails = db.ChildrenList_Data.Where(ii => ii.id == entity.c_id).FirstOrDefault();
                if(cDetails != null)
                {
                    cName = cDetails.children_name;
                    hh = cDetails.hh;
                }
                var sDetails = db.Member_Data.Where(ii => ii.id == cDetails.ss_id).FirstOrDefault();
                if(sDetails != null)
                {
                    ssName = sDetails.name;
                }

                string status = "";
                if(entity.status == 1)
                {
                    status = "<lable style='background-color: #4CAF50; color:#ffffff'>Registration</lable>";
                }else if (entity.status == 2)
                {
                    status = "<lable style='background-color: #2196F3; color:#ffffff'>Update</lable>";
                }else if (entity.status == 3)
                {
                    status = "<lable style='background-color: #ff9800; color:#ffffff'>HH Visit</lable>";
                }else if (entity.status == 4)
                {
                    status = "<lable style='background-color: #f44336; color:#ffffff'>Deworming</lable>";
                }

                

                obj.data.Add(new cListJson
                {
                    sl = s.ToString(),
                    date = entity.udtae.ToString("dd/MM/yyyy"),
                    ss_name = ssName,
                    type = status,
                    update_field = "N/A",
                    hh_no = hh,
                    children_name = cName
                }) ;

                s = s + 1;
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult Summary(string skID, string Fromdate, string Todate)
        {

            int skID_INT = Convert.ToInt32(skID);
            DateTime cFromDate = Convert.ToDateTime(Fromdate);
            DateTime cToDate = Convert.ToDateTime(Todate);

            int totalReg = db.ChildrenLog_Data.Where(ii_ => ii_.sk_id == skID_INT && ii_.udtae >= cFromDate && ii_.udtae <= cToDate && ii_.status == 1).Count();
            int totalVisit = db.ChildrenLog_Data.Where(ii_ => ii_.sk_id == skID_INT && ii_.udtae >= cFromDate && ii_.udtae <= cToDate && ii_.status == 3).Count();
            int totalDewo = db.ChildrenLog_Data.Where(ii_ => ii_.sk_id == skID_INT && ii_.udtae >= cFromDate && ii_.udtae <= cToDate && ii_.status == 4).Count();
            var data = new
            {
                sdate = Fromdate,
                totalReg = totalReg,
                totalVisit = totalVisit,
                totalDewo = totalDewo
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetSKList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var list = db.Member_Data.Where(ii => ii.type == 2).ToList();
            foreach (var entity in list)
            {
                items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }
    }

    public class cWebJson
    {
        public cWebJson() { data = new List<cListJson>(); }
        public List<cListJson> data { get; set; }
    }
    public class cListJson
    {
        public int id { get; set; }
        public string sl { get; set; }
        public string date { get; set; }
        public string ss_name { get; set; }
        public string type { get; set; }
        public string update_field { get; set; }
        public string hh_no { get; set; }
        public string children_name { get; set; }
    }
}
