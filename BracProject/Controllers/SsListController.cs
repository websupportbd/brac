﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class SsListController : ApiController
    {
        private MySqlCon db = new MySqlCon();

        // GET: api/khanalist
        public IEnumerable<string> Get()
        {
            return new string[] { "empty" };
        }

        // GET: api/contact/5
        public object Get(int id)
        {
            return "";
        }

        // POST: api/contact
        public object Post([FromBody]FormDataCollection formbody)
        {
            string skId = formbody.GetValues("sk_id")[0];
            int sk_id = int.Parse(skId);
            var results = db.AssignSs_Data.Where(i => i.sk_id == sk_id).Select(s =>
                                new
                                {
                                    id = s.ss_id,
                                    name = s.SS.name
                                }).Distinct().ToList();

           
            if (results.Count() != 0)
            {
                var obj = new SsListJson();
                foreach (var entity in results)
                {

                    obj.result.Add(new SsListListJson
                    {
                        id = entity.id,
                        ss_name = entity.name

                    });
                }

                var data = new
                {
                    success = true,
                    count = results.Count(),
                    obj.result
                };
                return data;
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "Ss list not found!"
                };
                return data;
            }

        }

        // PUT: api/contact/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/contact/5
        public void Delete(int id)
        {
        }
    }

    public class SsListJson
    {
        public SsListJson() { result = new List<SsListListJson>(); }
        public List<SsListListJson> result { get; set; }
    }
    public class SsListListJson
    {
        public int id { get; set; }
        public string ss_name { get; set; }
    }


}