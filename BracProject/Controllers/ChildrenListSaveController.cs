﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BracProject.Models;
using System.Net.Http.Formatting;
using System.Data.Entity;

namespace BracProject.Controllers
{
    public class ChildrenListSaveController : ApiController
    {
        private static MySqlCon db = new MySqlCon();
        // GET: api/ChildrenListSave
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ChildrenListSave/5
        public object GetChildrenList(int id)
        {
            var results = db.ChildrenList_Data.Where(m => m.id == id).ToList();

            if (results.Count() != 0)
            {
                var obj = new ChildrenListJson();
                string schoolName = "";
                int sixmonthStatus = 0;
                DateTime lastSixMonth;
                foreach (var entity in results)
                {
                    var schoolDetails = db.SchoolList_Data.Where(_i => _i.id == entity.school_id).FirstOrDefault();
                    if (schoolDetails != null)
                    {
                        schoolName = schoolDetails.school_name;
                    }
                    else
                    {
                        schoolName = "";
                    }

                    var sixmonth = db.DoseList_Data.Where(_i => _i.children_id == entity.id).OrderByDescending(m => m.date).Select(p => p).FirstOrDefault();
                    if (sixmonth != null)
                    {
                        DateTime today = DateTime.Today;
                        lastSixMonth = sixmonth.date;
                        int days = (today - lastSixMonth).Days;
                        double month = (today - lastSixMonth).Days / 30;

                        if (month <= 6)
                        {
                            sixmonthStatus = 1;
                        }
                        else
                        {
                            sixmonthStatus = 2;
                        }
                    }
                    string sideeffect = "";
                    if(entity.medicine_side_effect==0)
                    {
                        sideeffect = "NO";
                    }
                    else
                    {
                        sideeffect = "YES";
                    }


                    obj.childrenList.Add(new ChildrenListListJson
                    {
                        id = entity.id,
                        unick_id = entity.unique_id,
                        union_id = entity.union_id,
                        village_id = entity.village_id,
                        ss_id = entity.ss_id,
                        khana_no = entity.hh,
                        chindrinCount = entity.child_num,
                        khana_member = entity.household_member,
                        children_name = entity.children_name,
                        mother_name = entity.mother_name,
                        father_name = entity.father_name,
                        six_months_status = sixmonthStatus,
                        school_verification = entity.school_verification,
                        age = entity.age.ToString(),
                        gender = entity.gender,
                        school_id = entity.school_id,
                        school_name = schoolName,
                        class_id = entity.class_id,
                        six_months_medicine = entity.six_months_medicine,
                        is_toylet = entity.is_toylet,
                        drinking_water_id = entity.drinking_water_id,
                        takenMedicinePlace = entity.taken_medicine_place,
                        medicineSideEffect= sideeffect,
                        createDate=entity.create_date,
                        childrenCategoryStatus=entity.children_category_status,
                        isschoolgoing=entity.is_school_going


                    });
                }

                var data = new
                {
                    success = true,
                    count = results.Count(),

                    obj.childrenList
                };
                return data;
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "Children List Not found!"
                };
                return data;
            }
        }


        // POST: api/ChildrenListSave
        public object Post([FromBody]FormDataCollection formbody)
        {
            string message = "";
            string khana_no = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("khana_no")[0]))
            {
                khana_no = formbody.GetValues("khana_no")[0];
            }
            string union_id = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("union_id")[0]))
            {
                union_id = formbody.GetValues("union_id")[0];
            }
            string village_id = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("village_id")[0]))
            {
                village_id = formbody.GetValues("village_id")[0];
            }
            int ss_id = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("ss_id")[0]))
            {
                ss_id = Convert.ToInt32(formbody.GetValues("ss_id")[0]);
            }
            int sk_id = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("sk_id")[0]))
            {
                sk_id = Convert.ToInt32(formbody.GetValues("sk_id")[0]);
            }
            int po_id = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("po_id")[0]))
            {
                po_id = Convert.ToInt32(formbody.GetValues("po_id")[0]);
            }
            string khana_member = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("khana_member")[0]))
            {
                khana_member = formbody.GetValues("khana_member")[0];
            }
            string children_name = null;
            //if (!string.IsNullOrEmpty(formbody.GetValues("children_name")[0]))
            //{
                children_name = formbody.GetValues("children_name")[0];
            //}

            string mother_name = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("mother_name")[0]))
            {
                mother_name = formbody.GetValues("mother_name")[0];
            }

            string father_name = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("father_name")[0]))
            {
                father_name = formbody.GetValues("father_name")[0];
            }
            string age = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("age")[0]))
            {
                age = formbody.GetValues("age")[0];
            }
            int isSchoolGoing = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("is_school_going")[0]))
            {
                isSchoolGoing = Convert.ToInt32(formbody.GetValues("is_school_going")[0]);
            }
            string gender = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("gender")[0]))
            {
                gender = formbody.GetValues("gender")[0];
            }
            int school_id = 0;
            //if (!string.IsNullOrEmpty(formbody.GetValues("school_id")[0]))
            //{
                school_id = Convert.ToInt32(formbody.GetValues("school_id")[0]);
            //}
            string class_id = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("class_id")[0]))
            {
                class_id = formbody.GetValues("class_id")[0];
            }

            int six_months_medicine = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("six_months_medicine")[0]))
            {
                six_months_medicine = Convert.ToInt32(formbody.GetValues("six_months_medicine")[0]);
            }

            int is_toylet = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("is_toylet")[0]))
            {
                is_toylet = Convert.ToInt32(formbody.GetValues("is_toylet")[0]);
            }

            int drinking_water_id = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("drinking_water_id")[0]))
            {
                drinking_water_id = Convert.ToInt32(formbody.GetValues("drinking_water_id")[0]);
            }
            string takenMedicinePlace = null;
            //if (!string.IsNullOrEmpty(formbody.GetValues("taken_medicine_place")[0]))
            //{
                takenMedicinePlace = formbody.GetValues("taken_medicine_place")[0];
           // }
            //int schoolVerification = 0;
            //if (!string.IsNullOrEmpty(formbody.GetValues("school_verification")[0]))
            //{
            //    schoolVerification = Convert.ToInt32(formbody.GetValues("school_verification")[0]);
            //}
            int childCategory = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("children_category_status")[0]))
            {
                childCategory = Convert.ToInt32(formbody.GetValues("children_category_status")[0]);
            }
            int medicinesideeffect = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("medicine_side_effect")[0]))
            {
                medicinesideeffect = Convert.ToInt32(formbody.GetValues("medicine_side_effect")[0]);
            }




            try
            {
                string unick_id_p1 = "19-10-147-" + khana_no;
                var unickIdDetals = db.ChildrenList_Data.Where(ii_ => ii_.union_id == "19" && ii_.village_id == "10" && ii_.ss_id == 147 && ii_.hh == khana_no).ToList();
                int chindrinCount;
                if (unickIdDetals.Count() != 0)
                {
                    chindrinCount = unickIdDetals.Count() + 1;
                }
                else
                {
                    chindrinCount = 1;
                }

                string unick_id = unick_id_p1 + "-" + chindrinCount;

                var insert_khana_details = new ChildrenListModel();
                insert_khana_details.unique_id = unick_id;
                insert_khana_details.union_id = union_id;
                insert_khana_details.village_id = village_id;
                insert_khana_details.ss_id = ss_id;
                insert_khana_details.hh = khana_no;
                insert_khana_details.child_num = chindrinCount.ToString();
                insert_khana_details.household_member = khana_member;
                insert_khana_details.children_name = children_name;
                insert_khana_details.mother_name = mother_name;
                insert_khana_details.father_name = father_name;
                insert_khana_details.age = Convert.ToInt32(age);
                insert_khana_details.gender = gender.ToString();
                insert_khana_details.school_id = school_id;
                insert_khana_details.class_id = class_id.ToString();
                insert_khana_details.six_months_medicine = six_months_medicine;
                insert_khana_details.is_toylet = is_toylet;
                insert_khana_details.children_category_status = childCategory;
                insert_khana_details.medicine_side_effect = medicinesideeffect;
                // insert_khana_details.school_verification = schoolVerification;
                insert_khana_details.drinking_water_id = drinking_water_id;
                insert_khana_details.taken_medicine_place = takenMedicinePlace;
                insert_khana_details.create_date = DateTime.Now.ToString();
                insert_khana_details.sk_id = sk_id;
                insert_khana_details.po_id = po_id;
                insert_khana_details.is_school_going = isSchoolGoing;

                //insert_khana_details.updated_at = DateTime.Now;
                db.ChildrenList_Data.Add(insert_khana_details);
                db.SaveChanges();
                int chil_id = insert_khana_details.id;

                var log_insert = new ChildrenLogModel();
                log_insert.sk_id = Convert.ToInt32(sk_id);
                log_insert.c_id = Convert.ToInt32(chil_id);
                log_insert.udtae = DateTime.Now;
                log_insert.status = 1;
                db.ChildrenLog_Data.Add(log_insert);
                int log_id = db.SaveChanges();

                //insert_.union_id = unick_id;
                //insert_.date = DateTime.Now;
                //db.DoseList_Data.Add(insert_);
                //db.SaveChanges();
                var data = new
                {
                    success = true,
                    message = "Success"
                };
                return data;




            }
            catch (System.Exception e)
            {

                var data = new
                {
                    success = false,
                    message = e.Message
                };
                return data;
            }


        }

        // PUT: api/ChildrenListSave/5
        public object Put(int id, [FromBody]FormDataCollection formbody)
        {
            string message = "";
            //string khana_no = null;
            //if (!string.IsNullOrEmpty(formbody.GetValues("khana_no")[0]))
            //{
            //    khana_no = formbody.GetValues("khana_no")[0];
            //}
            //string union_id = null;
            //if (!string.IsNullOrEmpty(formbody.GetValues("union_id")[0]))
            //{
            //    union_id = formbody.GetValues("union_id")[0];
            //}
            //string village_id = null;
            //if (!string.IsNullOrEmpty(formbody.GetValues("village_id")[0]))
            //{
            //    village_id = formbody.GetValues("village_id")[0];
            //}
            //int ss_id = 0;
            //if (!string.IsNullOrEmpty(formbody.GetValues("ss_id")[0]))
            //{
            //    ss_id = Convert.ToInt32(formbody.GetValues("ss_id")[0]);
            //}
            //string khana_member = null;
            //if (!string.IsNullOrEmpty(formbody.GetValues("khana_member")[0]))
            //{
            //    khana_member = formbody.GetValues("khana_member")[0];
            //}
            //string children_name = null;
            //if (!string.IsNullOrEmpty(formbody.GetValues("children_name")[0]))
            //{
            //    khana_no = formbody.GetValues("children_name")[0];
            //}

            //string mother_name = null;
            //if (!string.IsNullOrEmpty(formbody.GetValues("mother_name")[0]))
            //{
            //    mother_name = formbody.GetValues("mother_name")[0];
            //}

            //string father_name = null;
            //if (!string.IsNullOrEmpty(formbody.GetValues("father_name")[0]))
            //{
            //    father_name = formbody.GetValues("father_name")[0];
            //}
            //string age = null;
            //if (!string.IsNullOrEmpty(formbody.GetValues("age")[0]))
            //{
            //    age = formbody.GetValues("age")[0];
            //}
            int schoolVerification = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("school_verification")[0]))
            {
                schoolVerification = Convert.ToInt32(formbody.GetValues("school_verification")[0]);
            }
            int school_id = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("school_id")[0]))
            {
                school_id = Convert.ToInt32(formbody.GetValues("school_id")[0]);
            }
            string class_id = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("class_id")[0]))
            {
                class_id = formbody.GetValues("class_id")[0];
            }

            int six_months_medicine = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("six_months_medicine")[0]))
            {
                six_months_medicine = Convert.ToInt32(formbody.GetValues("six_months_medicine")[0]);
            }

            int is_toylet = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("is_toylet")[0]))
            {
                is_toylet = Convert.ToInt32(formbody.GetValues("is_toylet")[0]);
            }

            int drinking_water_id = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("drinking_water_id")[0]))
            {
                drinking_water_id = Convert.ToInt32(formbody.GetValues("drinking_water_id")[0]);
            }
            string takenMedicinePlace = null;
            if (!string.IsNullOrEmpty(formbody.GetValues("taken_medicine_place")[0]))
            {
                takenMedicinePlace = formbody.GetValues("taken_medicine_place")[0];
            }

            int childCategory = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("children_category_status")[0]))
            {
                childCategory = Convert.ToInt32(formbody.GetValues("children_category_status")[0]);
            }
            int medicinesideeffect = 0;
            if (!string.IsNullOrEmpty(formbody.GetValues("medicine_side_effect")[0]))
            {
                medicinesideeffect = Convert.ToInt32(formbody.GetValues("medicine_side_effect")[0]);
            }
            try
            {
                ChildrenListModel result = new ChildrenListModel();

                result = db.ChildrenList_Data.Find(id);

                //  result.union_id = union_id;
                //  result.village_id = village_id;
                //  result.ss_id = ss_id;
                //  result.hh = khana_no;
                ////  result.child_num = chindrinCount;
                //  result.household_member = khana_member;
                //  result.children_name = children_name;
                //  result.mother_name = mother_name;
                //  result.father_name = father_name;
                //  result.age = age;
                result.school_verification = schoolVerification;
                result.school_id = school_id;
                result.class_id = class_id.ToString();
                result.six_months_medicine = six_months_medicine;
                result.is_toylet = is_toylet;
                result.drinking_water_id = drinking_water_id;
                result.taken_medicine_place = takenMedicinePlace;
                result.medicine_side_effect = medicinesideeffect;
                result.children_category_status = childCategory;
                db.Entry(result).State = EntityState.Modified;

                db.SaveChanges();


                var log_insert = new ChildrenLogModel();
                log_insert.sk_id = Convert.ToInt32(result.sk_id);
                log_insert.c_id = Convert.ToInt32(result.id);
                log_insert.udtae = DateTime.Now;
                log_insert.status = 2;
                db.ChildrenLog_Data.Add(log_insert);
                int log_id = db.SaveChanges();


                var data = new
                {
                    success = true,
                    message = "Success"
                };
                return data;

            }
            catch (System.Exception e)
            {

                var data = new
                {
                    success = false,
                    message = e.Message
                };
                return data;
            }


        }

        // DELETE: api/ChildrenListSave/5
        public void Delete(int id)
        {

        }

    }
    public class ChildrenListJson
    {
        public ChildrenListJson() { childrenList = new List<ChildrenListListJson>(); }
        public List<ChildrenListListJson> childrenList { get; set; }
    }
    public class ChildrenListListJson
    {
        public int id { get; set; }
        public string unick_id { get; set; }
        public string union_id { get; set; }
        public string village_id { get; set; }
        public int ss_id { get; set; }
        public string khana_no { get; set; }
        public string chindrinCount { get; set; }
        public string khana_member { get; set; }
        public string children_name { get; set; }
        public string school_name { get; set; }
        public int six_months_status { get; set; }
        public string mother_name { get; set; }
        public string father_name { get; set; }
        public string age { get; set; }
        public int school_verification { get; set; }
        public string gender { get; set; }
        public int school_id { get; set; }
        public string class_id { get; set; }
        public int six_months_medicine { get; set; }
        public int is_toylet { get; set; }
        public int drinking_water_id { get; set; }
        public string takenMedicinePlace { get; set; }
        public string medicineSideEffect { get; set; }
        public string createDate { get; set; }
        public int childrenCategoryStatus { get; set; }
        public int isschoolgoing { get; set; }


    }
}
