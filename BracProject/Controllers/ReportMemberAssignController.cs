﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class ReportMemberAssignController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View ();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            string type = "", member_id = "";

            if (!String.IsNullOrEmpty(search))
            {
                string[] isFilter = search.Split(';');
                if (isFilter.Count() > 1)
                {
                    search = "";
                    foreach (var a in isFilter)
                    {
                        string[] filter = a.Split(':');
                        if (filter[0] == "ty")
                        {
                            type = filter[1];
                        }
                        if (filter[0] == "me")
                        {
                            member_id = filter[1];
                        }
                    }
                }
                else
                {
                    //member = search;
                }
            }

            int memberID = Convert.ToInt32(member_id);


            if(type == "1")
            {
                var query = db.AssignSk_Data.AsQueryable();
                if (!String.IsNullOrEmpty(member_id))
                {
                    query = query.Where(w => w.po_id == memberID);
                }
                var results = new List<AssignSkModel>();
                if (search == null || search == "")
                {
                    totalRecord = query.Count();
                    query = query.OrderByDescending(c => c.id).Skip(start).Take(take);
                    results = query.ToList<AssignSkModel>();
                }
                var obj = new Report1WebJson();
                foreach (var entity in results)
                {
                    string mainName = "";
                    var PoDetails = db.Member_Data.Where(ii => ii.id == entity.po_id).FirstOrDefault();
                    if (PoDetails != null)
                    {
                        mainName = PoDetails.name;
                    }
                    obj.data.Add(new Report1WebListJson
                    {
                        id = entity.id.ToString(),
                        Member = mainName,
                        Assign = entity.SK.name
                    });
                }


                var data = new
                {
                    recordsTotal = results.Count(),
                    recordsFiltered = totalRecord,
                    obj.data
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else if (type == "2")
            {
                var query = db.AssignSs_Data.AsQueryable();
                if (!String.IsNullOrEmpty(member_id))
                {
                    query = query.Where(w => w.sk_id == memberID);
                }

                var Query = query.ToArray().GroupBy(i_ => i_.sk_id).Select(i_ => i_.FirstOrDefault()).OrderByDescending(i_ => i_.id);

                var results = new List<AssignSsModel>();
                if (search == null || search == "")
                {
                    totalRecord = Query.Count();
                    //query = query.OrderByDescending(c => c.id).Skip(start).Take(take);
                    results = Query.ToList<AssignSsModel>();
                }
                var obj = new Report1WebJson();
                foreach (var entity in results)
                {
                    string mainName = "";
                    var PoDetails = db.Member_Data.Where(ii => ii.id == entity.sk_id).FirstOrDefault();
                    if (PoDetails != null)
                    {
                        mainName = PoDetails.name;
                    }
                    obj.data.Add(new Report1WebListJson
                    {
                        id = entity.id.ToString(),
                        Member = mainName,
                        Assign = entity.SS.name
                    });
                }


                var data = new
                {
                    recordsTotal = results.Count(),
                    recordsFiltered = totalRecord,
                    obj.data
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var obj = new Report1WebJson();
                obj.data.Add(new Report1WebListJson
                {

                });

                var data = new
                {
                    recordsTotal = 0,
                    recordsFiltered = totalRecord,
                    obj.data
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }




        }

        [HttpPost]
        public ActionResult GetMember (string _id)
        {
            int ID = Convert.ToInt32(_id);

            List<SelectListItem> items = new List<SelectListItem>();

            var upz_list = db.Member_Data.Where(up => up.type == ID).OrderBy(d => d.id).ToList();
            foreach (var entity in upz_list)
            {
                items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }
    }



    public class Report1WebJson
    {
        public Report1WebJson() { data = new List<Report1WebListJson>(); }
        public List<Report1WebListJson> data { get; set; }
    }
    public class Report1WebListJson
    {
        public string id { get; set; }
        public string Member { get; set; }
        public string Assign { get; set; }
    }
}
