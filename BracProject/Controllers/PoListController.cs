﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using BracProject.Models;


namespace BracProject.Controllers
{
    public class PoListController : ApiController
    {
        private MySqlCon db = new MySqlCon();

        // GET: api/khanalist
        public IEnumerable<string> Get()
        {
            return new string[] { "empty" };
        }

        // GET: api/contact/5
        public object Get(int id)
        {
            return "";
        }

        // POST: api/contact
        public object Post([FromBody]FormDataCollection formbody)
        {
            var results = db.AssignPo_Data.OrderByDescending(_i => _i.id).ToList();

            if (results.Count() != 0)
            {
                var obj = new PoListJson();
                foreach (var entity in results)
                {

                    obj.result.Add(new PoListListJson
                    {
                        id = entity.po_id,
                        po_name = entity.PO.name

                    });
                }

                var data = new
                {
                    success = true,
                    count = results.Count(),
                    obj.result
                };
                return data;
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "Po list not found!"
                };
                return data;
            }

        }

        // PUT: api/contact/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/contact/5
        public void Delete(int id)
        {
        }
    }

    public class PoListJson
    {
        public PoListJson() { result = new List<PoListListJson>(); }
        public List<PoListListJson> result { get; set; }
    }
    public class PoListListJson
    {
        public int id { get; set; }
        public string po_name { get; set; }
    }


}
