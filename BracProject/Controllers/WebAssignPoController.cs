﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebAssignPoController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<AssignPoModel>();
            if (search == null || search == "")
            {
                totalRecord = db.AssignPo_Data.Where(ii_ => ii_.status == 1).Count();
                var Query = db.AssignPo_Data.Where(ii_ => ii_.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<AssignPoModel>();
            }
            else
            {
                totalRecord = db.AssignPo_Data.Where(c => c.PO.name.Contains(search) && c.status == 1).Count();
                var Query = db.AssignPo_Data.Where(c => c.PO.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<AssignPoModel>();
            }


            var obj = new POAssignWebJson();
            foreach (var entity in results)
            {
                string upazila = "";
                if (entity.uz_ids != null)
                {
                   
                    string strHeader_Upz = entity.uz_ids;
                    string[] strTempIDS_Upz = strHeader_Upz.Split(',');
                    foreach (var a in strTempIDS_Upz)
                    {
                        int id = Convert.ToInt32(a);
                        var upz = db.Upazila_Data.Where(ii => ii.ID == id).FirstOrDefault();
                        if (upz != null)
                        {
                            upazila += "Upazila: " + upz.NAME + ",";
                        }
                    }
                }

                string union = "";
                if (entity.ui_ids != null)
                {

                    string strHeader_uni = entity.ui_ids;
                    string[] strTempIDS_uni = strHeader_uni.Split(',');
                    foreach (var b in strTempIDS_uni)
                    {
                        int id = Convert.ToInt32(b);
                        var upz = db.Union_Data.Where(ii => ii.id == id).FirstOrDefault();
                        if (upz != null)
                        {
                            union += "Union: " + upz.name + ",";
                        }
                    }
                }

                string ward = "";
                if (entity.wa_ids != null)
                {

                    string strHeader_uni = entity.wa_ids;
                    string[] strTempIDS_uni = strHeader_uni.Split(',');
                    foreach (var b in strTempIDS_uni)
                    {
                        int id = Convert.ToInt32(b);
                        var upz = db.Ward_Data.Where(ii => ii.id == id).FirstOrDefault();
                        if (upz != null)
                        {
                            ward += "Ward: " + upz.name + ",";
                        }
                    }
                }

                string village = "";
                if (entity.vi_ids != null)
                {

                    string strHeader_uni = entity.vi_ids;
                    string[] strTempIDS_uni = strHeader_uni.Split(',');
                    foreach (var b in strTempIDS_uni)
                    {
                        int id = Convert.ToInt32(b);
                        var upz = db.Village_Data.Where(ii => ii.id == id).FirstOrDefault();
                        if (upz != null)
                        {
                            union += "Village: " + upz.name + ",";
                        }
                    }
                }
                string para = "";
                if (entity.pa_ids != null)
                {

                    string strHeader_uni = entity.pa_ids;
                    string[] strTempIDS_uni = strHeader_uni.Split(',');
                    foreach (var b in strTempIDS_uni)
                    {
                        int id = Convert.ToInt32(b);
                        var upz = db.Para_Data.Where(ii => ii.id == id).FirstOrDefault();
                        if (upz != null)
                        {
                            para += "Para: " + upz.name + ",";
                        }
                    }
                }

                string areas = "";
                if(upazila != "")
                {
                    areas = upazila;
                }
                if (union != "")
                {
                    areas = union;
                }
                if(village != "")
                {
                    areas = village;
                }
                if (ward != "")
                {
                    areas = ward;
                }
                if (para != "")
                {
                    areas = para;
                }


                obj.data.Add(new POAssignWebListJson
                {
                    id = entity.id,
                    po_name = entity.PO.name,
                    division_name = entity.Division.NAME,
                    distric_name = entity.District.NAME,
                    upazila_name = areas,
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/assign_po/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'> Edit</i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'> Delete</i>" +
                        "</button>" +
                         "<a href='/assign_sk/Index2/" + entity.po_id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-forward'> Assigned</i>" +
                        "</a>" +
                        "</div>"

                       

            });

            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(AssignPoModel cont)
        {

            if (ModelState.IsValid)
            {

                var insert_data = new AssignPoModel();
                insert_data.po_id = cont.po_id;
                insert_data.div_id = cont.div_id;
                insert_data.dis_id = cont.dis_id;
                insert_data.uz_ids = cont.uz_ids;
                insert_data.ui_ids = cont.ui_ids;
                insert_data.vi_ids = cont.vi_ids;
                insert_data.wa_ids = cont.wa_ids;
                insert_data.pa_ids = cont.pa_ids;
                insert_data.status = 1;
                insert_data.created_at = DateTime.Now;
                insert_data.updated_at = DateTime.Now;
                db.AssignPo_Data.Add(insert_data);
                db.SaveChanges();

                if (cont.uz_ids != null)
                {
                    string strHeader_Upz = cont.uz_ids;
                    string[] strTempIDS_Upz = strHeader_Upz.Split(',');
                    foreach (var a in strTempIDS_Upz)
                    {
                        var insert_data_list = new AssignListPoModel();
                        insert_data_list.po_list_id = insert_data.id;
                        insert_data_list.upz_id = Convert.ToInt32(a);
                        insert_data_list.status = 1;
                        insert_data_list.created_at = DateTime.Now;
                        insert_data_list.updated_at = DateTime.Now;
                        db.AssignListPo_Data.Add(insert_data_list);
                        db.SaveChanges();
                    }
                }

                if (cont.ui_ids != null)
                {
                    string strHeader_Uni = cont.ui_ids;
                    string[] strTempIDS_Uni = strHeader_Uni.Split(',');
                    foreach (var b in strTempIDS_Uni)
                    {
                        var insert_data_list = new AssignListPoModel();
                        insert_data_list.po_list_id = insert_data.id;
                        insert_data_list.union_id = Convert.ToInt32(b);
                        insert_data_list.status = 1;
                        insert_data_list.created_at = DateTime.Now;
                        insert_data_list.updated_at = DateTime.Now;
                        db.AssignListPo_Data.Add(insert_data_list);
                        db.SaveChanges();
                    }
                }
                else if(cont.wa_ids != null)
                {
                    string strHeader_Uni = cont.wa_ids;
                    string[] strTempIDS_Uni = strHeader_Uni.Split(',');
                    foreach (var b in strTempIDS_Uni)
                    {
                        var insert_data_list = new AssignListPoModel();
                        insert_data_list.po_list_id = insert_data.id;
                        insert_data_list.ward_id = Convert.ToInt32(b);
                        insert_data_list.status = 1;
                        insert_data_list.created_at = DateTime.Now;
                        insert_data_list.updated_at = DateTime.Now;
                        db.AssignListPo_Data.Add(insert_data_list);
                        db.SaveChanges();
                    }
                }

                if (cont.vi_ids != null)
                {
                    string strHeader_Vil = cont.vi_ids;
                    string[] strTempIDS_Vil = strHeader_Vil.Split(',');
                    foreach (var c in strTempIDS_Vil)
                    {
                        var insert_data_list = new AssignListPoModel();
                        insert_data_list.po_list_id = insert_data.id;
                        insert_data_list.village_id = Convert.ToInt32(c);
                        insert_data_list.status = 1;
                        insert_data_list.created_at = DateTime.Now;
                        insert_data_list.updated_at = DateTime.Now;
                        db.AssignListPo_Data.Add(insert_data_list);
                        db.SaveChanges();
                    }
                }
                else if(cont.pa_ids != null)
                {
                    string strHeader_Vil = cont.pa_ids;
                    string[] strTempIDS_Vil = strHeader_Vil.Split(',');
                    foreach (var c in strTempIDS_Vil)
                    {
                        var insert_data_list = new AssignListPoModel();
                        insert_data_list.po_list_id = insert_data.id;
                        insert_data_list.para_id = Convert.ToInt32(c);
                        insert_data_list.status = 1;
                        insert_data_list.created_at = DateTime.Now;
                        insert_data_list.updated_at = DateTime.Now;
                        db.AssignListPo_Data.Add(insert_data_list);
                        db.SaveChanges();
                    }
                }


                ModelState.Clear();
                cont = null;
                ViewBag.Message = "Assign Successfully";
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.AssignPo_Data.Where(c => c.id == id).FirstOrDefault();
            Session["po_id"] = cont.po_id;
            Session["po_ass_id"] = cont.id;
            return View(cont);
        }

        [HttpPost]
        public ActionResult Edit(AssignPoModel cont)
        {
            cont.updated_at = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.Entry(cont).Property(x => x.status).IsModified = false;
                    db.Entry(cont).Property(x => x.created_at).IsModified = false;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.AssignPo_Data.Where(at => at.id == id).SingleOrDefault();
            db.AssignPo_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult poList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (Session["po_id"] == null)
            {
                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 1).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                }
            }
            else
            {
                int poID = Convert.ToInt32(Session["po_id"]);

                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 1).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    if (poID == entity.id)
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDivision(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var div_list = db.Division_Data.OrderBy(d => d.ID).ToList<DivisionModel>();
            foreach (var entity in div_list)
            {
                items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDistrict(string div_id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (div_id != "")
            {
                int DivID = Convert.ToInt32(div_id);
              
                var dis_list = db.District_Data.Where(di => di.DIVISIONID == DivID).OrderBy(d => d.ID).ToList<DistrictModel>();
                foreach (var entity in dis_list)
                {
                    items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetUpozilla(string dis_id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (dis_id != "")
            {
                int DisID = Convert.ToInt32(dis_id);

                var upz_list = db.Upazila_Data.Where(up => up.DISTRICTID == DisID).OrderBy(d => d.ID).ToList<UpazilaModel>();
                foreach (var entity in upz_list)
                {
                    items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetUnionListV(string dis_id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (dis_id != "")
            {
                int DisID = Convert.ToInt32(dis_id);
               
                var upz_list = db.Union_Data.Where(up => up.upz_id == DisID).OrderBy(d => d.id).ToList();
                foreach (var entity in upz_list)
                {

                    items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        public class upozela
        {
            //public string union_id { get; set; }
            public string[] upozela_id { get; set; }

        }

        [HttpPost]
        public ActionResult GetUnion(List<upozela> obj)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (obj != null)
            {

                foreach (var id in obj)
                {
                    if (id.upozela_id != null)
                    {
                        for (var i = 0; i < id.upozela_id.Length; i++)
                        {
                            int IDS = Convert.ToInt32(id.upozela_id[i]);

                            var unionList = db.Union_Data.Where(ii => ii.upz_id == IDS).ToList();
                            foreach (var ent_u in unionList)
                            {
                                items.Add(new SelectListItem { Text = ent_u.name, Value = ent_u.id.ToString() });
                            }
                        }

                    }
                }
            }
           
            return Json(items, JsonRequestBehavior.AllowGet);
        }

      

        [HttpPost]
        public ActionResult GetWard(List<upozela> obj)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (obj != null)
            {
                foreach (var id in obj)
                {
                    if (id.upozela_id != null)
                    {
                        for (var i = 0; i < id.upozela_id.Length; i++)
                        {
                            int IDS = Convert.ToInt32(id.upozela_id[i]);

                            var wardList = db.Ward_Data.Where(ii => ii.upz_id == IDS).ToList();
                            foreach (var ent_u in wardList)
                            {
                                items.Add(new SelectListItem { Text = ent_u.name, Value = ent_u.id.ToString() });
                            }
                        }

                    }
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }


        public class unions
        {
            //public string union_id { get; set; }
            public string[] union_id { get; set; }

        }
        [HttpPost]
        public ActionResult GetVillage(List<unions> obj)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (obj != null)
            {
                foreach (var id in obj)
                {
                    if (id.union_id != null)
                    {
                        for (var i = 0; i < id.union_id.Length; i++)
                        {

                            int ID = Convert.ToInt32(id.union_id[i]);
                            var villageList = db.Village_Data.Where(ii => ii.union_id == ID).ToList();
                            foreach (var ent_v in villageList)
                            {
                                items.Add(new SelectListItem { Text = ent_v.name, Value = ent_v.id.ToString() });
                            }

                        }
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        public class ward
        {
            //public string union_id { get; set; }
            public string[] ward_id { get; set; }

        }
        [HttpPost]
        public ActionResult GetPara(List<ward> obj)
        {

            List<SelectListItem> items = new List<SelectListItem>();
            if (obj != null)
            {
                foreach (var id in obj)
                {
                    if (id.ward_id != null)
                    {
                        for (var i = 0; i < id.ward_id.Length; i++)
                        {

                            int ID = Convert.ToInt32(id.ward_id[i]);
                            var paraList = db.Para_Data.Where(ii => ii.ward_id == ID).ToList();
                            foreach (var ent_v in paraList)
                            {
                                items.Add(new SelectListItem { Text = ent_v.name, Value = ent_v.id.ToString() });
                            }

                        }
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetUpazilaList(string dis) 
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (dis != "")
            {
                int DisID = Convert.ToInt32(dis);
                int poID = Convert.ToInt32(Session["po_id"]);
                int poAssID = Convert.ToInt32(Session["po_ass_id"]);

                string[] strTempIDS_Upz = null;
                if (poAssID != 0)
                {
                    var details = db.AssignPo_Data.Where(ii => ii.id == poAssID).FirstOrDefault();
                    if (details.uz_ids != null)
                    {
                        if (details.uz_ids != null)
                        {
                            string strHeader_Upz = details.uz_ids;
                            strTempIDS_Upz = strHeader_Upz.Split(',');
                        }
                    }
                }

              

                var upz_list = db.Upazila_Data.Where(up => up.DISTRICTID == DisID).OrderBy(d => d.ID).ToList<UpazilaModel>();
                foreach (var entity in upz_list)
                {
                    bool select = false;
                    if (strTempIDS_Upz != null)
                    {
                        foreach (var a in strTempIDS_Upz)
                        {
                            int id = Convert.ToInt32(a);
                            if (id == entity.ID)
                            {
                                select = true;
                            }
                        }
                    }

                    if (select)
                    {
                        items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
                    }
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetUnionList(string dis)
        {
            int DisID = Convert.ToInt32(dis);
            int poID = Convert.ToInt32(Session["po_id"]);
            int poAssID = Convert.ToInt32(Session["po_ass_id"]);

            string[] strTempIDS = null;
            if (poAssID != 0)
            {
                var details = db.AssignPo_Data.Where(ii => ii.id == poAssID).FirstOrDefault();
                if (details.ui_ids != null)
                {
                    if (details != null)
                    {
                        string strHeader = details.ui_ids;
                        strTempIDS = strHeader.Split(',');
                    }
                }
            }
            List<SelectListItem> items = new List<SelectListItem>();
            var upz_list = db.Upazila_Data.Where(up => up.DISTRICTID == DisID).OrderBy(d => d.ID).ToList<UpazilaModel>();
            foreach (var entity in upz_list)
            {
                var unionList = db.Union_Data.Where(ii => ii.upz_id == entity.ID).ToList();
                foreach (var ent_u in unionList)
                {
                    bool select = false;
                    if (strTempIDS != null)
                    {
                        foreach (var a in strTempIDS)
                        {
                            int id = Convert.ToInt32(a);
                            if (id == ent_u.id)
                            {
                                select = true;
                            }
                        }
                    }

                    if (select)
                    {
                        items.Add(new SelectListItem { Text = ent_u.name, Value = ent_u.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = ent_u.name, Value = ent_u.id.ToString() });
                    }
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetVillageList(string dis)
        {
            int DisID = Convert.ToInt32(dis);
            int poID = Convert.ToInt32(Session["po_id"]);
            int poAssID = Convert.ToInt32(Session["po_ass_id"]);

            string[] strTempIDS = null;
            if (poAssID != 0)
            {
                var details = db.AssignPo_Data.Where(ii => ii.id == poAssID).FirstOrDefault();
                if (details.vi_ids != null)
                {
                    if (details != null)
                    {
                        string strHeader = details.vi_ids;
                        strTempIDS = strHeader.Split(',');
                    }
                }
            }
            List<SelectListItem> items = new List<SelectListItem>();
            var upz_list = db.Upazila_Data.Where(up => up.DISTRICTID == DisID).OrderBy(d => d.ID).ToList<UpazilaModel>();
            foreach (var entity in upz_list)
            {
                var unionList = db.Union_Data.Where(ii => ii.upz_id == entity.ID).ToList();
                foreach (var ent_u in unionList)
                {
                    var villageList = db.Village_Data.Where(ii => ii.union_id == ent_u.id).ToList();
                    foreach (var ent_v in villageList)
                    {
                        bool select = false;
                        if (strTempIDS != null)
                        {
                            foreach (var a in strTempIDS)
                            {
                                int id = Convert.ToInt32(a);
                                if (id == ent_v.id)
                                {
                                    select = true;
                                }
                            }
                        }

                        if (select)
                        {
                            items.Add(new SelectListItem { Text = ent_v.name, Value = ent_v.id.ToString(), Selected = true });
                        }
                        else
                        {
                            items.Add(new SelectListItem { Text = ent_v.name, Value = ent_v.id.ToString() });
                        }
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

    }

    public class POAssignWebJson
    {
        public POAssignWebJson() { data = new List<POAssignWebListJson>(); }
        public List<POAssignWebListJson> data { get; set; }
    }
    public class POAssignWebListJson
    {
        public int id { get; set; }
        public string po_name { get; set; }
        public string division_name { get; set; }
        public string distric_name { get; set; }
        public string upazila_name { get; set; }
        public string action { get; set; }
    }
}
