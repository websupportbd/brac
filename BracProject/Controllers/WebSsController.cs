﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebSsController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View ();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<SSModel>();
            if (search == null || search == "")
            {
                totalRecord = db.SS_Data.Where(ii_ => ii_.status == 1).Count();
                var Query = db.SS_Data.Where(ii_ => ii_.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<SSModel>();
            }
            else
            {
                totalRecord = db.SS_Data.Where(c => c.name.Contains(search) && c.status == 1).Count();
                var Query = db.SS_Data.Where(c => c.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<SSModel>();
            }


            var obj = new SsWebJson();
            foreach (var entity in results)
            {
                obj.data.Add(new SsWebListJson
                {
                    id = entity.id,
                    sk_name = entity.SK.name,
                    village_name = entity.Village.name,
                    name = entity.name,
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/ss/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        public ActionResult Add(SSModel cont)
        {
            cont.status = 1;
            if (ModelState.IsValid)
            {
                try
                {
                    db.SS_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "SS add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.SS_Data.Where(c => c.id == id).FirstOrDefault();
            Session["sk_id"] = cont.sk_id;
            return View(cont);
        }

        [HttpPost]
        public ActionResult Edit(SSModel cont)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.Entry(cont).Property(x => x.status).IsModified = false;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "SS update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }



        [HttpPost]
        public ActionResult skList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (Session["sk_id"] == null)
            {
                var _list = db.SK_Data.Where(v => v.status == 1).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                }
            }
            else
            {
                int skID = Convert.ToInt32(Session["sk_id"]);
                //Session.Remove("sk_id");

                var _list = db.SK_Data.Where(v => v.status == 1).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    if (skID == entity.id)
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetVillage(string div_id)
        {
            int DivID = Convert.ToInt32(div_id);
            List<SelectListItem> items = new List<SelectListItem>();
            var skDetails = db.SK_Data.Where(ii_ => ii_.id == DivID).FirstOrDefault();
            var dis_list = db.Village_Data.Where(di => di.union_id == skDetails.upazila_id).OrderBy(d => d.id).ToList();
            foreach (var entity in dis_list)
            {
                items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.SS_Data.Where(at => at.id == id).SingleOrDefault();
            db.SS_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }

    public class SsWebJson
    {
        public SsWebJson() { data = new List<SsWebListJson>(); }
        public List<SsWebListJson> data { get; set; }
    }
    public class SsWebListJson
    {
        public int id { get; set; }
        public string sk_name { get; set; }
        public string village_name { get; set; }
        public string name { get; set; }
        public string action { get; set; }
    }
}
