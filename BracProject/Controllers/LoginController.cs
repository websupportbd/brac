﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Security;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class LoginController : ApiController
    {
        private MySqlCon db = new MySqlCon();

        // GET: api/login
        public string Get()
        {
            return "value";
        }

        // GET: api/login/5
        public string Get(int id)
        {
            return "Empty";
        }

        // POST: api/login
        public object Post([FromBody]FormDataCollection formbody)
        {
            string userName = formbody.GetValues("user_name")[0];
            string password = formbody.GetValues("password")[0];
            string fcm_token = formbody.GetValues("fcm_token")[0];

           // string decode_password = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "md5");

            bool isUserAval = db.Member_Data.Where(us => us.user_name == userName).Any();

            string[] UserLocations = { };

            if (isUserAval)
            {
                var Query = db.Member_Data.Where(l => l.user_name == userName && l.password == password);
                var result = Query.FirstOrDefault<MemberModel>();
                if (result != null)
                {
                    string usertype = string.Empty;
                    if(result.type==1)
                    {
                        usertype = "1";
                    }
                    else if(result.type==2)
                    {
                        usertype = "0";
                    }

                    var data = new
                    {
                        success = true,
                        user = new
                        {
                            id = result.id,
                            name = result.name,
                            user_name = result.user_name,
                            user_type= usertype,
                            email = "",
                            mobile = result.mobile_no,
                            created_at = result.created_at.ToString("dd/MM/yyyy") + " " + result.created_at.ToString("HH:m"),
                            updated_at = result.updated_at.ToString("dd/MM/yyyy") + " " + result.updated_at.ToString("HH:m")
                        },
                        UserLocations,
                        token = "test"
                    };
                    return data;
                }
                else
                {
                    var data = new
                    {
                        success = false,
                        message = "Login faild. Please check the email address and password!"
                    };
                    return data;
                }
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "Login faild. Please check your user name!"
                };
                return data;
            }
        }

        // PUT: api/login/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/login/5
        public void Delete(int id)
        {
        }
    }
}
