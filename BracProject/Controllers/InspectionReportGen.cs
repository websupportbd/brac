﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class InspectionReportGen
    {
        private static MySqlCon db = new MySqlCon();



        public static string ReportGen (int id)
        {

            string rootPath = ConfigurationManager.AppSettings["baseURL"];
			string lat = "", lon = "", location = "", lo_date = "";

			var retult = db.Inspectiondata_Data.Where(inp => inp.ID == id).FirstOrDefault<InspectiondataModel>();
			//String SchemeName = db.Scheme_Data.Where(sk => sk.ID == retult.SCHEME_ID).FirstOrDefault<SchemeModel>().SCHEME_NAME;
			String SchemeDateTime = retult.CREATED_AT.ToString("dd/MM/yyy HH:m:s ttt");
			if (retult != null)
			{
				lat = retult.LAT;
				lon = retult.LON;
				location = retult.LOCATION;
				lo_date = retult.CREATED_AT.ToString("dd/MM/yyy HH:m:s ttt");
			}


			var Query = db.Inspectiondetails_Data.Where(inp_d => inp_d.INSPECTION_ID == id);
			var results = Query.ToList<InspectiondetailsModel>();
			string html_format = "<!DOCTYPE html>" +
				"<html lang='en'>" +
				"<head>" +
                "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />" +
				//"<link type='text/css' rel='stylesheet' href='file:///android_asset/bootstrap.min.css'>" +
                "<link type='text/css' rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>" +
				"<style> " +
				".ans{ font-weight: normal; } " +
				"textarea{ resize: none; } " +
				".checkbox-group, .radio-group{ padding-left: 30px; } " +
				"h3, .fb-text-label, .fb-textarea-label{ font-size: 16px; font-weight: bold; color: #3c763d; }" +
				".fb-radio-group-label{ font-size: 14px; font-weight: bold; } " +
				".fb-checkbox-group-label { font-weight: normal; font-size: 14px; } " +
				" </style>" +
				"<title>Report</title>" +
				"</head>" +

				"<body style='background: #fff3d6'>" +

				"<div class='container-fluid' style='padding-top: 10px; background: #fff3d6'>" +
				"<div class='container' style='padding-left:0px; padding-right: 0px;'>" +

				"<div class='row'>" +
                "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>" +
                "<div class='text-center' style='font-size:130%'>";

			
			html_format = html_format + "<p><b></b></p>";
			html_format = html_format + "</div>";

			html_format = html_format + "<div style='font-size:80%'>";
			html_format = html_format + "<p class='text-center'><b>Date/Time: " + SchemeDateTime + "</b></p>";
			html_format = html_format + "</div></div></div>";

			foreach (var entity in results)
			{
				string insp_name = db.InsForm_Data.Where(inspf => inspf.ID == entity.FORM_ID).FirstOrDefault<InsFormModel>().NAME_ORG;
				html_format = html_format + "<div class='row'>" +
                    "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>" +
                    "<div style='font-size:130%'>";
				html_format = html_format + "<p class='text-center'><strong style='border-bottom: 1px solid #000;'>" + insp_name + "</strong></p>";
				html_format = html_format + "</div>";

				string raw_data = entity.RAW_CODE;
				html_format = html_format + raw_data;

                html_format = html_format + "</div></div>";
			}


            html_format = html_format + "<div class='row'>" +
                "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";



			if (retult != null)
			{
                html_format = html_format + "<div id='mapview' style='width: 100%;'>" +
                    "<img style='position:relative;width: 100%;overflow:hidden;' src='http://maps.google.com/maps/api/staticmap?size=800x300&amp;markers=label:1|"+lat+","+lon+"&key=AIzaSyCK05ja9a2eQdLQSUgaNFzY0qhBfxU0ui8'>" +
                    "</div>";
				html_format = html_format + "<div style='margin-top:5px; margin-bottom: 5px;'><b><br />Location: </b>" + location + "<br />Latitude: " + lat + "<br />Longitude: " + lon + "<br />Date: " + lo_date + "</div>";
			}

			html_format = html_format + "</div></div></div></div>";



			//html_format = html_format + " <script>\n            function initMap() {\n                var uluru = {lat: " + lat + ", lng: " + lon + "};\n                var map = new google.maps.Map(document.getElementById('mapview'), {\n                    zoom: 20,\n                    center: uluru\n                });\n                var marker = new google.maps.Marker({\n                    position: uluru,\n                    map: map\n                });\n      map.setOptions({draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: false});   \n   }\n        </script>\n        <script async defer\n                src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCK05ja9a2eQdLQSUgaNFzY0qhBfxU0ui8&callback=initMap'>\n        </script>";

			html_format = html_format + "</body></html>";

			return html_format;
        }
    }
}
