﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class InspectionReportGenWeb : Controller
    {
        private static MySqlCon db = new MySqlCon();

        public static string ReportGen(int id)
        {
            string rootPath = ConfigurationManager.AppSettings["baseURL"];
       
            string gender_text, school_name, class_name, drinking_water_name, six_months_medicine_text, is_toylet_text;
            string union_name = string.Empty;
            string village_name = string.Empty;

            var details = db.ChildrenList_Data.Where(inp => inp.id == id).FirstOrDefault();


            if (details.gender == "1")
            {
                gender_text = "Male";
            }
            else if (details.gender == "0")
            {
                gender_text = "Female";
            }
            else
            {
                gender_text = details.gender;
            }

            var schoolDetails = db.SchoolList_Data.Where(_i => _i.id == details.school_id).FirstOrDefault();
            if (schoolDetails != null)
            {
                school_name = schoolDetails.school_name;
            }
            else
            {
                school_name = "";
            }


            if (details.union_id != "")
            {
                int unionid = Convert.ToInt32(details.union_id);
                var unionDetails = db.Union_Data.Where(_i => _i.id == unionid).FirstOrDefault();
                if (unionDetails != null)
                {
                    union_name = unionDetails.name;
                }
                else
                {
                    union_name = "";
                }
            }
            if (details.village_id != "")
            {
                int villageid = Convert.ToInt32(details.village_id);
                var viDetails = db.Village_Data.Where(_i => _i.id == villageid).FirstOrDefault();
                if (viDetails != null)
                {
                    village_name = viDetails.name;
                }
                else
                 {
                    village_name = "";
                }
            }





            class_name = details.class_id;
            //  drinking_water_name = db.DrinkingWaterList_Data.Where(_i => _i.id == details.drinking_water_id).FirstOrDefault().name;
            var drinkingwaterDetails = db.DrinkingWaterList_Data.Where(_i => _i.id == details.drinking_water_id).FirstOrDefault();
            if (drinkingwaterDetails != null)
            {
                drinking_water_name = drinkingwaterDetails.name;
            }
            else
            {
                drinking_water_name = "";
            }
            
            if (details.six_months_medicine == 1)
            {
                six_months_medicine_text = "Yes";
            }
            else
            {
                six_months_medicine_text = "No";
            }

            if (details.is_toylet == 1)
            {
                is_toylet_text = "Yes";
            }
            else
            {
                is_toylet_text = "No";
            }

            string html_format = "";
            string date = "";
            int countDewarming = 0;

            html_format = html_format + "<div class='row'>" +
                "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";

            //html_format += "<table class='table table-bordered'><thead></thead><tbody>";
            var dosDetails = db.DoseList_Data.Where(i_ => i_.children_id == details.id).ToList();
            if (dosDetails != null)
            {
                foreach (var en in dosDetails)
                {
                    // html_format += "<tr><td>";
                    date += en.date.ToString("dd-MM-yyyy") + ", ";
                    countDewarming++;
                   // html_format += "</td></tr>";
                }
            }
            //  html_format += "</tbody></table>";

          


            html_format += TemplateDetails.TableStart();

            html_format += "<tr><td>No</td><td>" + details.unique_id + "</td></tr>";
            html_format += "<tr><td>1. Union Name</td><td>" + union_name + "</td></tr>";
            html_format += "<tr><td>2. Village/Para Name</td><td>" + village_name + "</td></tr>";
            html_format += "<tr><td>3. Household No</td><td>" + details.hh + "</td></tr>";
            html_format += "<tr><td>4. Household Member</td><td>" + details.household_member.ToString() + "</td></tr>";
            html_format += "<tr><td>5. Child Name</td><td>" + details.children_name + "</td></tr>";
            html_format += "<tr><td>6. Mother's Name</td><td>" + details.mother_name + "</td></tr>";
            html_format += "<tr><td>7. Father's Name</td><td>" + details.father_name + "</td></tr>";
            html_format += "<tr><td>8. Age</td><td>" + details.age.ToString() + "</td></tr>";
            html_format += "<tr><td>9. Gender</td><td>" + gender_text + "</td></tr>";
            html_format += "<tr><td>10. School Name</td><td>" + school_name + "</td></tr>";
            html_format += "<tr><td>11. Class Name</td><td>" + class_name + "</td></tr>";
            // html_format += "<tr><td>10. Whether the worm pills has eaten in the last 6 months ?</td><td>" + six_months_medicine_text + "</td></tr>";
            html_format += "<tr><td>12. Last Six Months Deworming Status</td><td>" + date + "</td></tr>";
            html_format += "<tr><td>13. Number of deworming</td><td>" + countDewarming + "</td></tr>";
            html_format += "<tr><td>14. Sanitary Latrine Use</td><td>" + is_toylet_text + "</td></tr>";
            html_format += "<tr><td>15. Source of Drinking Water</td><td>" + drinking_water_name + "</td></tr>";
            html_format += "<tr><td>16. Taken Medicine Place</td><td>" + details.taken_medicine_place + "</td></tr>";


            //html_format += "<tr><td>নং</td><td>" + details.unique_id + "</td></tr>";
            //html_format += "<tr><td>১. খানা নং</td><td>" + details.hh + "</td></tr>";
            //html_format += "<tr><td>২. খানা সদস্য সংখ্যা</td><td>" + details.household_member.ToString() + "</td></tr>";
            //html_format += "<tr><td>৩. শিশুর নাম</td><td>" + details.children_name + "</td></tr>";
            //html_format += "<tr><td>৪. মাতার নাম</td><td>" + details.mother_name + "</td></tr>";
            //html_format += "<tr><td>৫. পিতার নাম</td><td>" + details.father_name + "</td></tr>";
            //html_format += "<tr><td>৬. বয়স</td><td>" + details.age.ToString() + "</td></tr>";
            //html_format += "<tr><td>৭. লিঙ্গ</td><td>" + gender_text + "</td></tr>";
            //html_format += "<tr><td>৮. প্রতিষ্ঠানের</td><td>" + school_name + "</td></tr>";
            //html_format += "<tr><td>৯. শ্রেণী</td><td>" + class_name + "</td></tr>";
            //html_format += "<tr><td>১০. গত ৬ মাসে কৃমিনাশক বড়ি খেয়েছে কি না ?</td><td>" + six_months_medicine_text + "</td></tr>";
            //html_format += "<tr><td>১১. স্যানিটারি টয়লেট আছে কিনা ?</td><td>" + is_toylet_text + "</td></tr>";
            //html_format += "<tr><td>১২. খাবার পানির উৎস</td><td>" + drinking_water_name + "</td></tr>";

            html_format += TemplateDetails.TableEnd();

            html_format = html_format + "</div></div>";

            return html_format;
        }
    }
}
