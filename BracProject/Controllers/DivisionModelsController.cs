﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class DivisionModelsController : ApiController
    {
        private MySqlCon db = new MySqlCon();

        // GET: api/DivisionModels
        public IQueryable<DivisionModel> GetDivision_Data()
        {
            return db.Division_Data;
        }

        // GET: api/DivisionModels/5
        [ResponseType(typeof(DivisionModel))]
        public IHttpActionResult GetDivisionModel(int id)
        {
            DivisionModel divisionModel = db.Division_Data.Find(id);
            if (divisionModel == null)
            {
                return NotFound();
            }

            return Ok(divisionModel);
        }

        // PUT: api/DivisionModels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDivisionModel(int id, DivisionModel divisionModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != divisionModel.ID)
            {
                return BadRequest();
            }

            db.Entry(divisionModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DivisionModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DivisionModels
        [ResponseType(typeof(DivisionModel))]
        public IHttpActionResult PostDivisionModel(DivisionModel divisionModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Division_Data.Add(divisionModel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = divisionModel.ID }, divisionModel);
        }

        // DELETE: api/DivisionModels/5
        [ResponseType(typeof(DivisionModel))]
        public IHttpActionResult DeleteDivisionModel(int id)
        {
            DivisionModel divisionModel = db.Division_Data.Find(id);
            if (divisionModel == null)
            {
                return NotFound();
            }

            db.Division_Data.Remove(divisionModel);
            db.SaveChanges();

            return Ok(divisionModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DivisionModelExists(int id)
        {
            return db.Division_Data.Count(e => e.ID == id) > 0;
        }
    }
}