﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Web.Helpers;
using System.Web.Http;
using BracProject.Models;
using Newtonsoft.Json;

namespace BracProject.Controllers
{
	public class InspectiondatareportController : ApiController
	{
		private MySqlCon db = new MySqlCon();

		// GET: api/inspectionreport
		public string Get()
		{
			return "empty";
		}

		// GET: api/inspectionreport/5
		public HttpResponseMessage Get(int id)
		{
            String html_format = InspectionReportGen.ReportGen(id);
			var response = new HttpResponseMessage();
			response.Content = new StringContent(html_format);
			response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
			return response;
		}

		// POST: api/inspectionreport
		public object Post([FromBody]FormDataCollection formbody)
		{
			//int projuct_id = Convert.ToInt32(formbody.GetValues("project_id")[0]);
			//int contact_id = Convert.ToInt32(formbody.GetValues("contact_id")[0]);
			//int scheme_id = Convert.ToInt32(formbody.GetValues("scheme_id")[0]);
            string from_id = formbody.GetValues("from_id")[0];
			string from_date = formbody.GetValues("from_date")[0];
			string to_date = formbody.GetValues("to_date")[0];
			int offset = Convert.ToInt32(formbody.GetValues("offset")[0]);

            var results = new List<InspectiondataModel>();
            if (from_date != "" && to_date != "")
            {
                //var Query = db.Inspectiondata_Data.SqlQuery("SELECT * FROM `inspection_data` WHERE `SCHEME_ID` = " + scheme_id + " && STR_TO_DATE(`CREATED_AT`,'%Y-%m-%d') BETWEEN '" + from_date + "' AND '" + to_date + "' ORDER BY ID DESC LIMIT " + offset +", 20");

                DateTime fromDate = CommanFunction.DateFromate(from_date).Date;
                DateTime toDate = CommanFunction.DateFromate(to_date);
                var Query = db.Inspectiondata_Data.Where(i_ => i_.CREATED_AT > fromDate && i_.CREATED_AT < toDate).OrderByDescending(i_ => i_.ID).Skip(offset).Take(20);

                results = Query.ToList<InspectiondataModel>();
            } else {
                var Query = db.Inspectiondata_Data.OrderByDescending(ins_ord => ins_ord.ID).Skip(offset).Take(20);
                results = Query.ToList<InspectiondataModel>();
            }


			if (results.Count() != 0)
			{
				var obj = new InspectionReportJson();

				foreach (var entity in results)
				{
                    if (from_id == "0")
                    {
                        string title = "";
                        var form_ids = db.Inspectiondetails_Data.Where(insd => insd.INSPECTION_ID == entity.ID).ToList<InspectiondetailsModel>();
                        foreach (var entity_fi in form_ids)
                        {
                            string form_name = db.InsForm_Data.Where(insp_f => insp_f.ID == entity_fi.FORM_ID).FirstOrDefault<InsFormModel>().NAME_ORG;
                            title = form_name + ", " + title;
                        }

                        title = title + "Location";

                        obj.result.Add(new InspectionReportListJson
                        {
                            inspection_id = entity.ID,
                            date = entity.CREATED_AT.ToString("dd MMM yyyy HH:m:s ttt"),
                            inspecs_title = title
                        });
                    }else{
                        bool isAvalable = false;
                        string title = "";
                        var form_ids = db.Inspectiondetails_Data.Where(insd => insd.INSPECTION_ID == entity.ID).ToList<InspectiondetailsModel>();
                        foreach (var entity_form_id in form_ids)
                        {
                            if (entity_form_id.FORM_ID == Convert.ToInt32(from_id))
                            {
                                isAvalable = true;
                            }
                        }

                        if (isAvalable)
                        {
                            foreach (var entity_fi in form_ids)
                            {
                                string form_name = db.InsForm_Data.Where(insp_f => insp_f.ID == entity_fi.FORM_ID).FirstOrDefault<InsFormModel>().NAME_ORG;
                                title = form_name + ", " + title;
                            }

                            title = title + "Location";

                            obj.result.Add(new InspectionReportListJson
                            {
                                inspection_id = entity.ID,
                                date = entity.CREATED_AT.ToString("dd MMM yyyy HH:m:s ttt"),
                                inspecs_title = title
                            });
                        }
                    }
				}

				var data = new
				{
					success = true,
					count = results.Count(),
					obj.result
				};
				return data;
			}
			else
			{
				var data = new
				{
					success = false,
					message = "Inspection report list not found!"
				};
				return data;
			}
		}

		// PUT: api/inspectionreport/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE: api/inspectionreport/5
		public void Delete(int id)
		{
		}
	}

	public class InspectionReportJson
	{
		public InspectionReportJson() { result = new List<InspectionReportListJson>(); }
		public List<InspectionReportListJson> result { get; set; }
	}
	public class InspectionReportListJson
	{
		public int inspection_id { get; set; }
		public string date { get; set; }
		public string inspecs_title { get; set; }
	}

	public class JsonreportModel
	{
		public int inspectionId { get; set; }
		public List<ItemList> items { get; set; }
	}

	public class ItemList
	{
		public string column_name { get; set; }
		public string lable { get; set; }
		public string lable_color { get; set; }
		public string lable_text_size { get; set; }
		public string lable_text_width { get; set; }
		public string description { get; set; }
		public string type { get; set; }
		public string type_option { get; set; }
		public string type_length { get; set; }
		public string your_value { get; set; }
		public List<OptionsList> options { get; set; }
	}

	public class OptionsList
	{
		public string option_name { get; set; }
		public string option_value { get; set; }
	}

}
