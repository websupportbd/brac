﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebPoController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View ();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<POModel>();
            if (search == null || search == "")
            {
                totalRecord = db.PO_Data.Where(ii_ => ii_.status == 1).Count();
                var Query = db.PO_Data.Where(ii_ => ii_.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<POModel>();
            }
            else
            {
                totalRecord = db.PO_Data.Where(c => c.name.Contains(search) && c.status == 1).Count();
                var Query = db.PO_Data.Where(c => c.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<POModel>();
            }


            var obj = new PoWebJson();
            foreach (var entity in results)
            {
                obj.data.Add(new PoWebListJson
                {
                    id = entity.id,
                    name = entity.name,
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/po/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        public ActionResult Add(POModel cont)
        {
            cont.status = 1;
            cont.created_at = DateTime.Now;
            cont.updated_at = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    db.PO_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "PO add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.PO_Data.Where(c => c.id == id).FirstOrDefault<POModel>();
            return View(cont);
        }

        [HttpPost]
        public ActionResult Edit(POModel cont)
        {

            cont.updated_at = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.Entry(cont).Property(x => x.status).IsModified = false;
                    db.Entry(cont).Property(x => x.created_at).IsModified = false;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Po update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.PO_Data.Where(at => at.id == id).SingleOrDefault<POModel>();
            db.PO_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }



    }


    public class PoWebJson
    {
        public PoWebJson() { data = new List<PoWebListJson>(); }
        public List<PoWebListJson> data { get; set; }
    }
    public class PoWebListJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string action { get; set; }
    }
}
