﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebUpazilaController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<UpazilaModel>();
            if (search == null || search == "")
            {
                totalRecord = db.Upazila_Data.Count();
                var Query = db.Upazila_Data.OrderByDescending(c => c.ID).Skip(start).Take(take);
                results = Query.ToList();
            }
            else
            {
                totalRecord = db.Upazila_Data.Where(c => c.NAME.Contains(search)).Count();
                var Query = db.Upazila_Data.Where(c => c.NAME.Contains(search)).OrderByDescending(c => c.ID).Skip(start).Take(take);
                results = Query.ToList();
            }

            var obj = new UpazilaWebJson();
            foreach (var entity in results)
            {

                string action = null;
                action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/upazila/Edit/" + entity.ID + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.ID + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>";

                var status = "";
                if (entity.ISACTIVE == 1)
                {
                    status = "Active";
                }
                else
                {
                    status = "Inactive";
                }

                obj.data.Add(new UpazilaWebListJson
                {
                    id = entity.ID,
                    division = entity.District.Division.NAME,
                    district = entity.District.NAME,
                    subdistrict=entity.SUB_DISTRICT,
                    name = entity.NAME,
                    Status=status,
                    action = action
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }





        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Add(UpazilaModel cont)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Upazila_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "Upazila add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Session["upz_id"] = id;
            var cont = db.Upazila_Data.Where(c => c.ID == id).FirstOrDefault();
            return View(cont);
        }


        [HttpPost]
        public ActionResult Edit(UpazilaModel cont)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Upazila update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.Upazila_Data.Where(at => at.ID == id).SingleOrDefault();
            db.Upazila_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }



    }


    public class UpazilaWebJson
    {
        public UpazilaWebJson() { data = new List<UpazilaWebListJson>(); }
        public List<UpazilaWebListJson> data { get; set; }
    }
    public class UpazilaWebListJson
    {
        public int id { get; set; }
        public string division { get; set; }
        public string district { get; set; }
        public string subdistrict { get; set; }

        public string name { get; set; }
        public string Status { get; set; }

        public string action { get; set; }
    }
}
