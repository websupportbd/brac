﻿using System;
using System.Collections.Generic;
using System.Linq;
using BracProject.Models;
using Newtonsoft.Json;

namespace BracProject.Controllers
{
	public class DataTableRequest
	{
		public int draw { get; set; }

		public int start { get; set; }
		public int length { get; set; }
		public List<Column> columns { get; set; }
		public Search search { get; set; }
		public List<Order> order { get; set; }

	}

	public class Column
	{
		public string data { get; set; }
		public string name { get; set; }
		public bool searchable { get; set; }
		public bool orderable { get; set; }
		public Search search { get; set; }
	}

	public class Search
	{
		public string value { get; set; }
		public string regex { get; set; }
	}

	public class Order
	{
		public int column { get; set; }
		public string dir { get; set; }
	}

	public class LgedUserDetailsJson
	{
		public string Id { get; set; }
		public string DisplayName { get; set; }
		public string Email { get; set; }
		public string IsActive { get; set; }
		public string Username { get; set; }
		public string UserId { get; set; }
		public UserOffice UserOffice { get; set; }
		public string[] UserProjects { get; set; }
		public List<UserLocations> UserLocations { get; set; }
	}

	public class UserLocations
	{
		public string RegionId { get; set; }
		public string DivisionId { get; set; }
		public string DistrictId { get; set; }
		public string UpazilaId { get; set; }
	}

	public class UserOffice
	{
		public string DesignationId { get; set; }
		public string OfficeTypeId { get; set; }
		public string DivisionId { get; set; }
		public string DistrictId { get; set; }
	}

	public class TemplateHeaderFooter
	{
		public static string Header()
		{
			string value = "<!DOCTYPE html><html lang = 'en'> <head>" +
				"<meta http - equiv = 'Content-Type' content = 'text/html charset=UTF-8' />" +
				"<link type = 'text/css' rel = 'stylesheet' href = 'file:///android_asset/bootstrap.min.css'>" +
				"<script src = 'file:///android_asset/jquery.min.js'></script>" +
				"<title> Inspection </title> <style> textarea{ resize: none; } .checkbox-group, .radio-group{ padding-left: 30px; } h3{ font-size: 16px; font-weight: bold; color: #3c763d; } .fb-radio-group-label{ font-size: 14px; font-weight: bold; } .fb-checkbox-group-label, .fb-text-label{ font-weight: normal; font-size: 14px;} </style>" +
				"</head>" +
				"<body style = 'background: #fff3d6' >" +
				"<div class='container-fluid' style='padding-top: 10px; padding-left: 0px; padding-right: 0px; background: #fff3d6'>" +
				"<div class='container'>" +
				"<div class='row'>" +
				"<form id = 'rendered-form' > <div class='rendered-form'>";
			return value;
		}

		public static string Footer()
		{
			string value = "</div></form></div></div><div class='clearfix'></div></div><div id='jsonvalue' style='visibility: hidden; display:inline;'></div></body>";
			return value;
		}
	}

	public class TemplateDetails
	{
		public static string Header()
		{
			string value = "<!DOCTYPE html><html lang='en'><head><meta http-equiv='Content-Type' content='text/html charset=UTF-8' /><link type='text/css' rel='stylesheet' href='file:///android_asset/bootstrap.min.css'><script src='file:///android_asset/jquery.min.js'></script><style>textarea {resize: none;}.checkbox-group,.radio-group {padding-left: 30px;}h3 {font-size: 16px;font-weight: bold;color: #3c763d;}.fb-radio-group-label {font-size: 14px;font-weight: bold;}.fb-checkbox-group-label,.fb-text-label {font-weight: normal;font-size: 14px;}.wizard>.content {min-height: 7em;}.steps {font-size: 10px;}.wizard>.steps>ul>li {width: 15%;} .dot {height: 60px;width: 60px;background-color: #bbb;border-radius: 100%;display: inline-block;}</style></head><body style='background: #ffff'><div class='container-fluid' style='padding-top: 10px; padding-left: 0px; padding-right: 0px; background: #ffff'><div class='container'><div class='row'><form id='rendered-form'><div class='rendered-form'><div class='content clearfix'>";
			return value;
		}
		public static string TableStart()
		{
			string value = "<table class='table table-bordered'><tbody>";
			return value;
		}
		public static string TableEnd()
		{
			string value = "</tbody></table>";
			return value;
		}
		public static string Footer()
        {
			string value = "</div></div></form></div></div><div class='clearfix'></div></div></body><script>$('#btn').click(function() {var chID = $('#chilId').val();alert('কৃমিনাশক বড়ি খাওয়ানো হয়েছে');Android.updateDose(chID);});function doseSuccess() {alert('কৃমিনাশক বড়ি খাওয়ানো হয়েছে');}function doseNotSuccess() {alert('কৃমিনাশক বড়ি খাওয়ানো হয়নি');}</script></html>";
            return value;
        }
	}


	public class MemberListJson
	{
		public string Convenar { get; set; }
		public string Name { get; set; }
		public string Designation { get; set; }
		public string ProjectOffice { get; set; }
		public string _id { get; set; }
	}
	public class DefectListJson
	{
		public string Item { get; set; }
		public string Design { get; set; }
		public string Field { get; set; }
		public string Comments { get; set; }
	}
	public class DefectListJson_P
	{
		public string Item_p { get; set; }
		public string Design_p { get; set; }
		public string Field_p { get; set; }
		public bool Solve { get; set; }
		public string Comments_p { get; set; }
	}

	public class CommanFunction
	{
		public static DateTime DateFromate(string date)
		{
			string[] break_from_date = date.Split('-');
			string from_date_format = break_from_date[2] + "-" + break_from_date[1] + "-" + break_from_date[0];
			DateTime DateTimeF = Convert.ToDateTime(from_date_format, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

			return DateTimeF;
		}
        public static String GetOfficePost(int date)
        {
            String sReturn = "";
            if(date == 1)
            {
                sReturn = "PO";
            }
            if (date == 2)
            {
                sReturn = "SK";
            }
            if (date == 3)
            {
                sReturn = "SS";
            }

            return sReturn;
        }
    }

	public class TeamlistAPIJson
	{
		public TeamlistAPIJson() { result = new List<TeamlistAPIListJson>(); }
		public List<TeamlistAPIListJson> result { get; set; }
	}
	public class TeamlistAPIListJson
	{
		public int id { get; set; }
		public string name { get; set; }
		public string designation { get; set; }
		public string area { get; set; }
	}

	public class TeamMemberlistAPIJson
	{
		public TeamMemberlistAPIJson() { result = new List<TeamMemberlistAPIListJson>(); }
		public List<TeamMemberlistAPIListJson> result { get; set; }
	}
	public class TeamMemberlistAPIListJson
	{
		public int id { get; set; }
		public int team_id { get; set; }
		public string convenar { get; set; }
		public string name { get; set; }
		public string designation { get; set; }
		public string projectOffice { get; set; }
	}
    
	public class LocationPermission
	{
		public static bool Permission(String getJsonString, string type, int ID)
		{
			var results = JsonConvert.DeserializeObject<LgedUserDetailsJson>(getJsonString);
			bool _permission = false;

			if (type == "Division")
			{
				if (results.UserLocations.Count == 0)
				{
					_permission = true;
				}
				else
				{
					foreach (var entity in results.UserLocations)
					{
						int locDiv = Convert.ToInt32(entity.DivisionId);
						if (locDiv == ID)
						{
							_permission = true;
						}
						else
						{
							_permission = false;
						}
					}
				}
			}

			if (type == "District")
            {
				if (results.UserLocations.Count == 0)
				{
					_permission = true;
				}
				else
				{
					foreach (var entity in results.UserLocations)
					{
						int locDiv = Convert.ToInt32(entity.DistrictId);
						if (locDiv == ID)
						{
							_permission = true;
						}
						else
						{
							_permission = false;
						}
					}
				}
            }

			if (type == "Upazila")
            {
				if (results.UserLocations.Count == 0)
				{
					_permission = true;
				}
				else
				{
					foreach (var entity in results.UserLocations)
					{
						int locDiv = Convert.ToInt32(entity.UpazilaId);
						if (locDiv == ID)
						{
							_permission = true;
						}
						else
						{
							_permission = false;
						}
					}
				}
            }

			if (type == "Projuct")
            {
				if (results.UserProjects.Count() == 0)
				{
					_permission = true;
				}
				else
				{
					foreach (var entity in results.UserProjects)
					{
						int locDiv = Convert.ToInt32(entity);
						if (locDiv == ID)
						{
							_permission = true;
						}
						else
						{
							_permission = false;
						}
					}
				}
            }

			return _permission;
		}

		internal static bool Permission(LgedUserDetailsJson result, string v, int iD)
		{
			throw new NotImplementedException();
		}
	}

}
