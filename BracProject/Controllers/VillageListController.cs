﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using BracProject.Models;


namespace BracProject.Controllers
{
    public class VillageListController : ApiController
    {
        private MySqlCon db = new MySqlCon();

        // GET: api/khanalist
        public IEnumerable<string> Get()
        {
            return new string[] { "empty" };
        }

        // GET: api/contact/5
        public object Get(int id)
        {
            return "";
        }

        // POST: api/contact
        public object Post([FromBody]FormDataCollection formbody)
        {
            string ssId = formbody.GetValues("ss_id")[0];
            int ss_id = int.Parse(ssId);
            var results = db.AssignSs_Data.Where(i => i.ss_id == ss_id).Select(s =>
                                new
                                {
                                    id = s.vill_id,
                                    name = s.Village.name
                                }).Distinct().ToList();


            if (results.Count() != 0)
            {
                var obj = new VillListJson();
                foreach (var entity in results)
                {

                    obj.result.Add(new VillListListJson
                    {
                        id = entity.id,
                        vill_name = entity.name

                    });
                }

                var data = new
                {
                    success = true,
                    count = results.Count(),
                    obj.result
                };
                return data;
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "Ss list not found!"
                };
                return data;
            }

        }

        // PUT: api/contact/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/contact/5
        public void Delete(int id)
        {
        }
    }

    public class VillListJson
    {
        public VillListJson() { result = new List<VillListListJson>(); }
        public List<VillListListJson> result { get; set; }
    }
    public class VillListListJson
    {
        public int id { get; set; }
        public string vill_name { get; set; }
    }


}