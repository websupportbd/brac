﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebDivisionController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View ();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<DivisionModel>();
            if (search == null || search == "")
            {
                totalRecord = db.Division_Data.Count();
                var Query = db.Division_Data.OrderByDescending(c => c.ID).Skip(start).Take(take);
                results = Query.ToList();
            }
            else
            {
                totalRecord = db.Division_Data.Where(c => c.NAME.Contains(search)).Count();
                var Query = db.Division_Data.Where(c => c.NAME.Contains(search)).OrderByDescending(c => c.ID).Skip(start).Take(take);
                results = Query.ToList();
            }

            var obj = new DivisionWebJson();
            foreach (var entity in results)
            {

                string action = null;
                action =  "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/division/Edit/" + entity.ID + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.ID + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>";
                var status = "";
                if (entity.ISACTIVE == 1)
                {
                    status = "Active";
                }
                else
                {
                    status = "Inactive";
                }

                obj.data.Add(new DivisionWebListJson
                {
                    id = entity.ID,
                    name = entity.NAME,
                     Status=status,
                    action = action
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }





        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Add(DivisionModel cont)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Division_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "Division add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Session["div_id"] = id;
            var cont = db.Division_Data.Where(c => c.ID == id).FirstOrDefault();
            return View(cont);
        }


        [HttpPost]
        public ActionResult Edit(DivisionModel cont)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Division update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.Division_Data.Where(at => at.ID == id).SingleOrDefault();
            db.Division_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }


    public class DivisionWebJson
    {
        public DivisionWebJson() { data = new List<DivisionWebListJson>(); }
        public List<DivisionWebListJson> data { get; set; }
    }
    public class DivisionWebListJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string action { get; set; }
        public string Status { get; set; }
    }
}
