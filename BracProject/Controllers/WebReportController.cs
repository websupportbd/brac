﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace BracProject.Controllers
{
    public class WebReportController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View ();
        }

        public ActionResult ReportDewarmingChildren()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            string divID = "", disID = "", upzID = "", projID = "", contID = "", 
            schemeID = "" , officeID = "", designationID = "", userID = "", 
            from_date = "", to_date = "";

            if (!String.IsNullOrEmpty(search))
            {
                string[] isFilter = search.Split(';');
                if (isFilter.Count() > 1)
                {
                    search = "";
                    foreach (var a in isFilter)
                    {
                        string[] filter = a.Split(':');
                        if (filter[0] == "div")
                        {
                            divID = filter[1];
                        }
                        if (filter[0] == "dis")
                        {
                            disID = filter[1];
                        }
                        if (filter[0] == "upz")
                        {
                            upzID = filter[1];
                        }
                        if (filter[0] == "pro")
                        {
                            projID = filter[1];
                        }
                        if (filter[0] == "cont")
                        {
                            contID = filter[1];
                        }
                        if (filter[0] == "scheme")
                        {
                            schemeID = filter[1];
                        }
                        if (filter[0] == "office")
                        {
                            officeID = filter[1];
                        }
                        if (filter[0] == "dsignation")
                        {
                            designationID = filter[1];
                        }
                        if (filter[0] == "users")
                        {
                            userID = filter[1];
                        }
                        if (filter[0] == "from_date")
                        {
                            from_date = filter[1];
                        }
                        if (filter[0] == "to_date")
                        {
                            to_date = filter[1];
                        }
                    }
                }
            }

            var query = db.Inspectiondata_Data.AsQueryable();

            if (!String.IsNullOrEmpty(divID))
            {
                int _id = Convert.ToInt32(divID);
                query = query.Where(w => w.DIV == _id);
            }
            if (!String.IsNullOrEmpty(disID))
            {
                int _id = Convert.ToInt32(disID);
                query = query.Where(w => w.DIS == _id);
            }
            if (!String.IsNullOrEmpty(upzID))
            {
                int _id = Convert.ToInt32(upzID);
                query = query.Where(w => w.UPZ == _id);
            }

            if (!String.IsNullOrEmpty(userID))
            {
                int _id = Convert.ToInt32(userID);
                query = query.Where(w => w.USER_ID == _id);
            }
            if (!String.IsNullOrEmpty(from_date))
            {
                //string[] break_from_date = from_date.Split('-');
                //string from_date_format = break_from_date[2] + "-" + break_from_date[1] + "-" + break_from_date[0];
                //query = query.Where(w => w.CREATED_AT > from_date_format);
            }
            if (!String.IsNullOrEmpty(to_date))
            {
                //string[] break_from_date = from_date.Split('-');
                //string from_date_format = break_from_date[2] + "-" + break_from_date[1] + "-" + break_from_date[0];
                //query = query.Where(w => w.CREATED_AT < from_date_format);
            }

            var results = new List<InspectiondataModel>();
            if (search == null || search == "")
            {
                totalRecord = query.Count();
                query = query.OrderByDescending(c => c.ID).Skip(start).Take(take);
                results = query.ToList<InspectiondataModel>();
            }
            else
            {
                //totalRecord = db.Inspectiondata_Data.Where(c => c.USER_ID.Contains(search)).Count();
                //var Query = db.Inspectiondata_Data.Where(c => c.Scheme.SCHEME_NAME.Contains(search)).OrderByDescending(c => c.ID).Skip(start).Take(take);
                //results = Query.ToList<InspectiondataModel>();
            }

            var obj = new ReportWebJson();
            foreach (var entity in results)
            {
				string user_name = db.Login_Data.Where(u => u.id == entity.USER_ID).FirstOrDefault().name;
                string id = entity.ID.ToString();
                string date_c = entity.CREATED_AT.ToString("dd/MM/yyyy HH:m");
                string visit_s = "1";
                if (entity.READ_STATUS == 0 )
                {
                    id = "<b class='unread'>" + id + "</b>";
                    user_name = "<b class='unread'>" + user_name + "</b>";
                    date_c = "<b class='unread'>" + date_c + "</b>";
                    visit_s = "<b class='unread'>" + visit_s + "</b>";
                }
                obj.data.Add(new ReportWebListJson
                {
                    
                    user_name = user_name,
                    scheme_name = "",
                    date_ = date_c,
                    visit = visit_s,
					action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                                "<a style = 'width:10px;padding-left:10px;' href='/report/Reportview/" + entity.ID + "' class='btn btn-icon btn-primary btn-square'>" +
                                "<i class='icon-eye'></i>" +
                                "</a>" +
                                "<a style = 'width:10px;padding-left:10px;' href='/report/GetPdf/" + entity.ID + "' class='btn btn-icon btn-success btn-square'>" +
                                "<i class='icon-file-pdf'></i>" +
                                "</a>" +
                              "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);
            
        }

        //public ActionResult GetPdf(int id)
        //{
        //    String siteContent = InspectionReportGen.ReportGen(id);
        //    var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
        //    var parth = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/{0}", "Storage/Pdf/"));
        //    htmlToPdf.GeneratePdf(siteContent, null, parth + "FIMS_Report_" + id + ".pdf");

        //    string fullPath = parth + "FIMS_Report_" + id + ".pdf";

        //    return File(fullPath, "application/pdf", Server.UrlEncode("FIMS_Report_" + id + ".pdf"));
        //}

        //[HttpGet]
        //public ActionResult Reportview(int id)
        //{
        //    var details = db.Inspectiondata_Data.Where(i => i.ID == id).FirstOrDefault();
        //    details.READ_STATUS = 1;
        //    db.SaveChanges();
            
        //    string html_code = InspectionReportGenWeb.ReportGen(id);
        //    ViewBag.Report = html_code;
        //    return View();
        //}

        [HttpGet]
        public ActionResult GetDivision()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            //var div_list = db.Division_Data.OrderBy(d => d.ID).ToList<DivisionModel>();
            //foreach (var entity in div_list)
            //{
            //    items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
            //}
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDistrict(string div_id)
        {
            int DivID = Convert.ToInt32(div_id);
            List<SelectListItem> items = new List<SelectListItem>();
            var dis_list = db.District_Data.Where(di => di.DIVISIONID == DivID).OrderBy(d => d.ID).ToList<DistrictModel>();
            foreach (var entity in dis_list)
            {
                items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetUpozilla(string dis_id)
        {
            int DisID = Convert.ToInt32(dis_id);
            List<SelectListItem> items = new List<SelectListItem>();
            var upz_list = db.Upazila_Data.Where(up => up.DISTRICTID == DisID).OrderBy(d => d.ID).ToList<UpazilaModel>();
            foreach (var entity in upz_list)
            {
                items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetProject()
        {
            List<SelectListItem> items = new List<SelectListItem>();
			//var list_ = db.Inspectiondata_Data.ToArray().GroupBy(x_ => x_.PROJECT_ID).Select(a => a.FirstOrDefault()).OrderBy(b => b.Project.NAME).ToList<InspectiondataModel>();
			//var list_ = db.Project_Data.OrderBy(_x => _x.NAME).ToList();
			//foreach (var entity in list_)
   //         {
                //if (entity.PROJECT_ID != 0)
                //{
                //    items.Add(new SelectListItem { Text = entity.Project.NAME, Value = entity.PROJECT_ID.ToString() });
                //}
				//if (entity.ID != 0)
                //{
				//items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
                //}

            //}
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetProjectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var list_ = db.Project_Data.OrderBy(b => b.NAME).ToList();
            foreach (var entity in list_)
            {
                items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

     

     





        [HttpGet]
        public ActionResult GetUsers()
        {
            List<SelectListItem> items = new List<SelectListItem>();
			var list = db.Login_Data.OrderBy(d => d.name).ToList<LoginModel>();
            foreach (var entity in list)
            {
				items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }
    }

    public class ReportWebJson
    {
        public ReportWebJson() { data = new List<ReportWebListJson>(); }
        public List<ReportWebListJson> data { get; set; }
    }
    public class ReportWebListJson
    {
        public string id { get; set; }
        public string user_name { get; set; }
        public string scheme_name { get; set; }
        public string date_ { get; set; }
        public string visit { get; set; }
        public string action { get; set; }
    }
}
