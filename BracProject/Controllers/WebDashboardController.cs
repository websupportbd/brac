﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;
using Newtonsoft.Json;

namespace lged_api.Controllers
{
    //[Authorize]
    public class WebDashboardController : Controller
    {
        private MySqlCon db = new MySqlCon();

        //[Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetTtoalChildren(string searchText)
        {
            int totalChil = db.ChildrenList_Data.Count();
            int boys = db.ChildrenList_Data.Where(ii_ => ii_.gender == "1" && ii_.gender != "").Count();
            int girls = db.ChildrenList_Data.Where(ii_ => ii_.gender == "0" && ii_.gender != "").Count();

            int schoolgoingchildren = db.ChildrenList_Data.Where(ii_ => ii_.school_id != 0 && ii_.age > 5 && ii_.r_3 != "").Count();
            int Notschoolgoingchildren = db.ChildrenList_Data.Where(ii_ => ii_.school_id == 0 && ii_.age > 5 && ii_.r_3 != "").Count();

            int schoolGoingboys = db.ChildrenList_Data.Where(ii_ => ii_.school_id != 0 && ii_.gender == "1").Count();
            int schoolGoinggirls = db.ChildrenList_Data.Where(ii_ => ii_.school_id != 0 && ii_.gender != "1").Count();
            int OutofschoolGoingboys = db.ChildrenList_Data.Where(ii_ => ii_.school_id == 0 && ii_.gender == "1").Count();
            int OutofschoolGoinggirls = db.ChildrenList_Data.Where(ii_ => ii_.school_id == 0 && ii_.gender != "1").Count();
            //total deworming

            int totalDewrming = 0;

            var totaldose = from p in db.DoseList_Data.GroupBy(i => i.children_id)
                            select new
                            {
                                count = p.Count(),

                            };
            //totalDewrming = totaldose.Count();

            totalDewrming = db.ChildrenList_Data.Where(ii_ => ii_.r_3 == "1").Count();

            //var totaldose = db.DoseList_Data.GroupBy(i => i.children_id);
            //foreach (var grp in totaldose)
            //{
            //   // var number = grp.Key;
            //     totalDewrming = grp.Count();
            //}
            int totalschoolGoingchildrenDeworming = 0;


            //var schoolGoingchildrenDeworming = from p in db.DoseList_Data.Where(m => m.ChildrenList.school_id != 0).GroupBy(i => i.children_id)
            //                                   select new
            //                                   {
            //                                       count = p.Count(),

            //                                   };
            //totalschoolGoingchildrenDeworming = schoolGoingchildrenDeworming.Count();

            totalschoolGoingchildrenDeworming = db.ChildrenList_Data.Where(ii_ => ii_.school_id != 0 && ii_.age > 5 && ii_.r_3 == "1").Count();

            int totalOutschoolGoingchildrenDeworming = 0;

            //var outschoolGoingchildrenDeworming = from p in db.DoseList_Data.Where(m => m.ChildrenList.school_id == 0).GroupBy(i => i.children_id)
            //                                      select new
            //                                      {
            //                                          count = p.Count(),

            //                                      };

            //totalOutschoolGoingchildrenDeworming = outschoolGoingchildrenDeworming.Count();

            totalOutschoolGoingchildrenDeworming = db.ChildrenList_Data.Where(ii_ => ii_.school_id == 0 && ii_.age > 5 && ii_.r_3 == "1").Count();

            int totalOhousehold = 0;


            //var household = from p in db.ChildrenList_Data.GroupBy(i => i.hh)
            //                select new
            //                {
            //                    count = p.Count(),

            //                };

            //totalOhousehold = household.Count();
            totalOhousehold = db.ChildrenList_Data.Where(ii_ => ii_.child_num == "1").Count();

            var april2018 = from p in db.DoseList_Data.Where(m => m.date.Year == 2018 && m.date.Month == 4).GroupBy(i => i.children_id)
                            select new
                            {
                                count = p.Count(),

                            };

            int april_2018 = april2018.Count();
            //-------------------------------------------------------------------------------
            var april2018_Camp = from p in db.DoseList_Data.Where(m => m.date.Year == 2018 && m.date.Month == 4 && m.taken_dose_place== "Camp").GroupBy(i => i.children_id)
                            select new
                            {
                                count = p.Count(),

                            };

            int april_2018_Camp = april2018_Camp.Count();

            var april2018_School = from p in db.DoseList_Data.Where(m => m.date.Year == 2018 && m.date.Month == 4 && m.taken_dose_place == "School").GroupBy(i => i.children_id)
                                 select new
                                 {
                                     count = p.Count(),

                                 };

            int april_2018_School = april2018_School.Count();


            var april2018_House = from p in db.DoseList_Data.Where(m => m.date.Year == 2018 && m.date.Month == 4 && m.taken_dose_place == "Home").GroupBy(i => i.children_id)
                                   select new
                                   {
                                       count = p.Count(),

                                   };

            int april_2018_House = april2018_House.Count();

            //---------------------------------------------------------------------------------


            var october2018 = from p in db.DoseList_Data.Where(m => m.date.Year == 2018 && m.date.Month == 10).GroupBy(i => i.children_id)
                              select new
                              {
                                  count = p.Count(),

                              };
            int october_2018 = october2018.Count();

            //----------------------------------------------------------------------------------

            var october2018_camp = from p in db.DoseList_Data.Where(m => m.date.Year == 2018 && m.date.Month == 10 && m.taken_dose_place == "Camp").GroupBy(i => i.children_id)
                              select new
                              {
                                  count = p.Count(),

                              };
            int october_2018_camp = october2018_camp.Count();


            var october2018_School = from p in db.DoseList_Data.Where(m => m.date.Year == 2018 && m.date.Month == 10 && m.taken_dose_place == "School").GroupBy(i => i.children_id)
                                   select new
                                   {
                                       count = p.Count(),

                                   };
            int october_2018_School = october2018_School.Count();

            var october2018_Home = from p in db.DoseList_Data.Where(m => m.date.Year == 2018 && m.date.Month == 10 && m.taken_dose_place == "Home").GroupBy(i => i.children_id)
                                   select new
                                   {
                                       count = p.Count(),

                                   };
            int october_2018_Home = october2018_Home.Count();


            //----------------------------------------------------------------------------------------------------


            var april2019 = from p in db.DoseList_Data.Where(m => m.date.Year == 2019 && (m.date.Month == 3 || m.date.Month == 4)).GroupBy(i => i.children_id)
                            select new
                            {
                                count = p.Count(),

                            };
            int april_2019 = april2019.Count();



            //----------------------------------------------------------------------------------------------------
            var april2019_Camp = from p in db.DoseList_Data.Where(m => m.date.Year == 2019 && (m.date.Month == 3 || m.date.Month == 4) && m.taken_dose_place == "Camp").GroupBy(i => i.children_id)
                                 select new
                                 {
                                     count = p.Count(),

                                 };

            int april_2019_Camp = april2019_Camp.Count();

            var april2019_School = from p in db.DoseList_Data.Where(m => m.date.Year == 2019 && (m.date.Month == 3 || m.date.Month == 4) && m.taken_dose_place == "School").GroupBy(i => i.children_id)
                                   select new
                                   {
                                       count = p.Count(),

                                   };

            int april_2019_School = april2019_School.Count();


            var april2019_House = from p in db.DoseList_Data.Where(m => m.date.Year == 2019 && (m.date.Month == 3 || m.date.Month == 4) && m.taken_dose_place == "Home").GroupBy(i => i.children_id)
                                  select new
                                  {
                                      count = p.Count(),

                                  };

            int april_2019_House = april2019_House.Count();



            //--------------------------------------------------------------------------------------------------------





            var october2019 = from p in db.DoseList_Data.Where(m => m.date.Year == 2019 && m.date.Month == 10).GroupBy(i => i.children_id)
                              select new
                              {
                                  count = p.Count(),

                              };
            //-------------------------------------------------------------------------------------------------------------

            var october2019_camp = from p in db.DoseList_Data.Where(m => m.date.Year == 2019 && m.date.Month == 10 && m.taken_dose_place == "Camp").GroupBy(i => i.children_id)
                                   select new
                                   {
                                       count = p.Count(),

                                   };
            int october_2019_camp = october2019_camp.Count();


            var october2019_School = from p in db.DoseList_Data.Where(m => m.date.Year == 2019 && m.date.Month == 10 && m.taken_dose_place == "School").GroupBy(i => i.children_id)
                                     select new
                                     {
                                         count = p.Count(),

                                     };
            int october_2019_School = october2019_School.Count();

            var october2019_Home = from p in db.DoseList_Data.Where(m => m.date.Year == 2019 && m.date.Month == 10 && m.taken_dose_place == "Home").GroupBy(i => i.children_id)
                                   select new
                                   {
                                       count = p.Count(),

                                   };
            int october_2019_Home = october2019_Home.Count();




            var october2019OutOfSchool = from p in db.DoseList_Data.Where(m => m.date.Year == 2019 && m.date.Month == 10 && m.taken_dose_place != "School").GroupBy(i => i.children_id)
                              select new
                              {
                                  count = p.Count(),

                              };


            int october_2019_OutofSchool = october2019OutOfSchool.Count();




            var october2018OutOfSchool = from p in db.DoseList_Data.Where(m => m.date.Year == 2018 && m.date.Month == 10 && m.taken_dose_place != "School").GroupBy(i => i.children_id)
                                         select new
                                         {
                                             count = p.Count(),

                                         };


            int october_2018_OutofSchool = october2018OutOfSchool.Count();



            var april2019OutOfSchool = from p in db.DoseList_Data.Where(m => m.date.Year == 2019 && m.date.Month == 4 && m.taken_dose_place != "School").GroupBy(i => i.children_id)
                                         select new
                                         {
                                             count = p.Count(),

                                         };


            int april_2019_OutofSchool = april2019OutOfSchool.Count();


            var april2018OutOfSchool = from p in db.DoseList_Data.Where(m => m.date.Year == 2018 && m.date.Month == 4 && m.taken_dose_place != "School").GroupBy(i => i.children_id)
                                       select new
                                       {
                                           count = p.Count(),

                                       };


            int april_2018_OutofSchool = april2018OutOfSchool.Count();




            int october_2019 = october2019.Count();
            int totalNotDeworming = totalChil - totalDewrming;
            int notdewormingSchoolgoingchildren = schoolgoingchildren - totalschoolGoingchildrenDeworming;
            int notdewormingoutofschoolgoingchildren = Notschoolgoingchildren - totalOutschoolGoingchildrenDeworming;

            // var april2018=db.ChildrenList_Data.Where(v=>v.)

            var monthlytarget = db.Monthly_Target.OrderBy(mo => mo.month_name).FirstOrDefault();

            var newchild = db.ChildrenList_Data.Where(cc => cc.children_category_status == 0).Count();
            var Migratedchild = db.ChildrenList_Data.Where(cc => cc.children_category_status == 1).Count();
            var Graduatedchildren = db.ChildrenList_Data.Where(cc => cc.children_category_status == 2).Count();           
            var Losttofollowup = db.ChildrenList_Data.Where(cc => cc.children_category_status == 3).Count();




            var data = new
            {
                success = true,
                children = totalChil,
                Boys = boys,
                Girls = girls,
                school_going_boys = schoolGoingboys,
                school_going_girls = schoolGoinggirls,

                outof_School_GoingBoys = OutofschoolGoingboys,
                outof_School_GoingGirls = OutofschoolGoinggirls,

                TotalSchoolGoingChildren = schoolgoingchildren,
                NotSchoolGoingChildren = Notschoolgoingchildren,
                TotalDewormingChild = totalDewrming,
                TotalNotDewormingChild = totalNotDeworming,

                total_school_children_deworming = totalschoolGoingchildrenDeworming,
                total_outofschool_children_deworming = totalOutschoolGoingchildrenDeworming,
                not_deworming_schoolGoing_children = notdewormingSchoolgoingchildren,
                not_deworming_Outof_schoolGoing_children = notdewormingoutofschoolgoingchildren,

                total_household = totalOhousehold,
                new_child=newchild,
                miagrate_child=Migratedchild,
                graduateChild=Graduatedchildren,
                Lost_to_follow_up=Losttofollowup,

                april_2018_Data_school = april_2018_School,
                april_2018_Data_camp= april_2018_Camp,
                 april_2018_Data_household= april_2018_House,

                october_2018_Data_school = october_2018_School,
                october_2018_Data_camp = october_2018_camp,
                october_2018_Data_household = october_2018_Home,


                april_2019_Data_school = april_2019_School,
                april_2019_Data_camp = april_2019_Camp,
                april_2019_Data_household = april_2019_House,

                october_2019_Data_school = october_2019_School,
                october_2019_Data_camp = october_2019_camp,
                october_2019_Data_household = october_2019_Home,


                october_2019_outof_school = october_2019_OutofSchool,
                april_2019_outof_school = april_2019_OutofSchool,
                october_2018_outof_school = october_2018_OutofSchool,
                april_2018_outof_school = april_2018_OutofSchool,


                todays_target_due = 1010,
                monthly_target = monthlytarget.target,
                monthly_target_fullfil = 80,
                monthly_target_due = 20
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }




    }
}
