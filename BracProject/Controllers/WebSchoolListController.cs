﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebSchoolListController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<SchoolListModel>();
            if (search == null || search == "")
            {
                totalRecord = db.SchoolList_Data.Where(ii_ => ii_.status == 1).Count();
                var Query = db.SchoolList_Data.Where(ii_ => ii_.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<SchoolListModel>();
            }
            else
            {
                totalRecord = db.SchoolList_Data.Where(c => c.school_name.Contains(search) && c.status == 1).Count();
                var Query = db.SchoolList_Data.Where(c => c.school_name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<SchoolListModel>();
            }


            var obj = new SchoolWebJson();
            foreach (var entity in results)
            {
                //int uz_id = db.Union_Data.Where(ii_ => ii_.id == entity.union_id).FirstOrDefault().upz_id;
                //string up_name = db.Upazila_Data.Where(ii_ => ii_.ID == uz_id).FirstOrDefault().NAME;
                obj.data.Add(new SchoolWebListJson
                {
                    id = entity.id,
                    upz_name = entity.Upazila.NAME,
                    union_name = entity.Union.name,
                    name = entity.school_name,
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/school_list/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        public ActionResult Add(SchoolListModel cont)
        {
            cont.status = 1;
            if (ModelState.IsValid)
            {
                try
                {
                    db.SchoolList_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "School add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.SchoolList_Data.Where(c => c.id == id).FirstOrDefault();
            return View(cont);
        }

        [HttpPost]
        public ActionResult Edit(SchoolListModel cont)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.Entry(cont).Property(x => x.status).IsModified = false;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "School update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.SchoolList_Data.Where(at => at.id == id).SingleOrDefault();
            db.SchoolList_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetUnion(string upz_id)
        {
            int DisID = Convert.ToInt32(upz_id);
            List<SelectListItem> items = new List<SelectListItem>();
            var upz_list = db.Union_Data.Where(up => up.upz_id == DisID).OrderBy(d => d.id).ToList();
            foreach (var entity in upz_list)
            {
                items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }
    }


    public class SchoolWebJson
    {
        public SchoolWebJson() { data = new List<SchoolWebListJson>(); }
        public List<SchoolWebListJson> data { get; set; }
    }
    public class SchoolWebListJson
    {
        public int id { get; set; }
        public string upz_name { get; set; }
        public string union_name { get; set; }
        public string name { get; set; }
        public string action { get; set; }
    }
}
