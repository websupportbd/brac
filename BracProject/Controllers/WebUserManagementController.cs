﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BracProject.Controllers;
using BracProject.Models;

namespace BracProject.Controllers
{
    //[Authorize]
    public class WebUserManagementController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<LoginModel>();
            if (search == null || search == "")
            {
                totalRecord = db.Login_Data.Count();
                var Query = db.Login_Data.OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<LoginModel>();
            }
            else
            {
                totalRecord = db.Login_Data.Where(c => c.name.Contains(search)).Count();
                var Query = db.Login_Data.Where(c => c.name.Contains(search)).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<LoginModel>();
            }

            if (results.Count() != 0)
            {
                var obj = new LoginWebJson();
                foreach (var entity in results)
                {
                    obj.data.Add(new LoginWebListJson
                    {
                        id = entity.id,
                        user_name = entity.name,
                        email = entity.user_name,
                        action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                            "<a href='/user_management/Roles/" + entity.id + "' class='btn btn-icon btn-primary btn-square'>" +
                            "<i class='icon-toggle2'></i>" +
                            "</a>" +
                            "<a href='/user_management/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                            "<i class='icon-edit'></i>" +
                            "</a>" +
                            "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                            "<i class='icon-android-delete'></i>" +
                            "</button>" +
                        "</div>"
                    });
                }
                var data = new
                {
                    recordsTotal = results.Count(),
                    recordsFiltered = totalRecord,
                    obj.data
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "User data not found!"
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Roles(int id)
        {
            Session["roleuserid"] = null;
            Session["roleuserid"] = id;
            var cont = db.Login_Data.Where(c => c.id == id).FirstOrDefault<LoginModel>();
            return View(cont);
        }

        [HttpPost]
        public ActionResult RoleList(DataTableRequest req)
        {
            int userid;
            if (Session["roleuserid"] == null)
            {
                return RedirectToAction("../user_management");
            }else{
                userid = Convert.ToInt32(Session["roleuserid"].ToString());
                Session["roleuserid"] = null;
            }

            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<ModuleListModel>();
            if (search == null || search == "")
            {
                totalRecord = db.ModuleList_Data.Count();
                var Query = db.ModuleList_Data.OrderBy(c => c.id).Skip(start).Take(take);
                results = Query.ToList<ModuleListModel>();
            }
            else
            {
                totalRecord = db.ModuleList_Data.Where(c => c.name.Contains(search)).Count();
                var Query = db.ModuleList_Data.Where(c => c.name.Contains(search)).OrderBy(c => c.id).Skip(start).Take(take);
                results = Query.ToList<ModuleListModel>();
            }

            if (results.Count() != 0)
            {
                var obj = new UserRoleWebJson();
                foreach (var entity in results)
                {
                    string view_permission="";
                    string add_permission="";
                    string edit_permission="";
                    string del_permission="";
                    string permision_txt=null;
                    var QueryRoal = db.UserRoleList_Data.Where(ur => ur.module_id == entity.id && ur.user_id == userid);
                    var ResultRoal = QueryRoal.FirstOrDefault<UserRoleListModel>();
                    if (ResultRoal != null)
                    {
                        if (ResultRoal.u_view == 1) { view_permission = "checked"; }
                        if (ResultRoal.u_add == 1) { add_permission = "checked"; }
                        if (ResultRoal.u_edit == 1) { edit_permission = "checked"; }
                        if (ResultRoal.u_delete == 1) { del_permission = "checked"; }
                    }

                    permision_txt = "<div class='switch'>" +
                        "<span style='float:left; width:40px; padding-top:5px; padding-left:10px;'>View:</span> <input id='view_" + entity.id + "' class='user-roal-js cmn-toggle cmn-toggle-round' type='checkbox' "+view_permission+" >" +
                            "<label for='view_" + entity.id + "'></label>" +
                            "<span style='float:left; width:40px; padding-top:5px; padding-left:10px;'>Add:</span> <input id='add_" + entity.id + "' class='user-roal-js cmn-toggle cmn-toggle-round' type='checkbox' "+add_permission+">" +
                            "<label for='add_" + entity.id + "'></label>" +
                            "<span style='float:left; width:40px; padding-top:5px; padding-left:10px;'>Edit:</span> <input id='edit_" + entity.id + "' class='user-roal-js cmn-toggle cmn-toggle-round' type='checkbox' "+edit_permission+">" +
                            "<label for='edit_" + entity.id + "'></label>" +
                            "<span style='float:left; width:50px; padding-top:5px; padding-left:10px;'>Delete:</span> <input id='del_" + entity.id + "' class='user-roal-js cmn-toggle cmn-toggle-round' type='checkbox' "+del_permission+">" +
                            "<label for='del_" + entity.id + "'></label>" +
                            "</div>";
                    
                    obj.data.Add(new UserRoleWebListJson
                    {
                        module_id = entity.id,
                        module_name = entity.name,
                        action = permision_txt
                    });
                }
                var data = new
                {
                    recordsTotal = results.Count(),
                    recordsFiltered = totalRecord,
                    obj.data
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "User data not found!"
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ModifyUserRole(string module_name, string user_id)
        {

            int userid = Convert.ToInt32(user_id);
          
            var module_ = module_name.Split('_');
            string name = module_[0];
            int id = Convert.ToInt32(module_[1]);

            var result = db.UserRoleList_Data.Where(ur => ur.module_id == id && ur.user_id == userid).FirstOrDefault<UserRoleListModel>();
            if (result == null)
            {
                int _view = 0; int _add = 0; int _edit = 0; int _del = 0;
                if (name == "view") { _view = 1; }
                if (name == "add") { _add = 1; }
                if (name == "edit") { _edit = 1; }
                if (name == "del") { _del = 1; }

                var insert_data = new UserRoleListModel();
                insert_data.user_id = userid;
                insert_data.module_id = id;
                insert_data.u_view = _view;
                insert_data.u_add = _add;
                insert_data.u_edit = _edit;
                insert_data.u_delete = _del;
                insert_data.status = 1;
                insert_data.created_at = DateTime.Now;
                db.UserRoleList_Data.Add(insert_data);
                int inspec_data_sucess = db.SaveChanges();

            }else{
                var update_data = db.UserRoleList_Data.Where(ur => ur.module_id == id && ur.user_id == userid).FirstOrDefault<UserRoleListModel>();

                int _view = update_data.u_view; 
                int _add = update_data.u_add; 
                int _edit = update_data.u_edit; 
                int _del = update_data.u_delete;

                if (name == "view") { if (_view == 1){ _view = 0; }else{ _view = 1; } }
                if (name == "add") { if (_add == 1) { _add = 0; } else { _add = 1; } }
                if (name == "edit") { if (_edit == 1) { _edit = 0; } else { _edit = 1; } }
                if (name == "del") { if (_del == 1) { _del = 0; } else { _del = 1; } }

                if (name == "view") { update_data.u_view = _view; }
                if (name == "add") { update_data.u_add = _add; }
                if (name == "edit") { update_data.u_edit = _edit; }
                if (name == "del") { update_data.u_delete = _del; }
                db.SaveChanges();
            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(LoginModel cont)
        {
            string password = FormsAuthentication.HashPasswordForStoringInConfigFile(cont.password, "md5");
            cont.password = password;
            if (ModelState.IsValid)
            {
                try
                {
                    db.Login_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "User add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Session["user_id"] = id;
            var cont = db.Login_Data.Where(c => c.id == id).FirstOrDefault<LoginModel>();
            cont.password = "";
            return View(cont);
        }

        // Attributes edit save function
        [HttpPost]
        public ActionResult Edit(LoginModel cont)
        {
            cont.password = FormsAuthentication.HashPasswordForStoringInConfigFile(cont.password, "md5");
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "User update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            cont.password = "";
            return View(cont);
        }

        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.Login_Data.Where(at => at.id == id).SingleOrDefault<LoginModel>();
            db.Login_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }



    }

    public class LoginWebJson
    {
        public LoginWebJson() { data = new List<LoginWebListJson>(); }
        public List<LoginWebListJson> data { get; set; }
    }
    public class LoginWebListJson
    {
        public int id { get; set; }
        public string user_name { get; set; }
        public string email { get; set; }
        public string action { get; set; }
    }


    public class UserRoleWebJson
    {
        public UserRoleWebJson() { data = new List<UserRoleWebListJson>(); }
        public List<UserRoleWebListJson> data { get; set; }
    }
    public class UserRoleWebListJson
    {
        public int module_id { get; set; }
        public string module_name { get; set; }
        public string action { get; set; }
    }
}
