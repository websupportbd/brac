﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
	public class DashboardController : ApiController
    {
		private MySqlCon db = new MySqlCon();

		// GET: api/dashboard
        public string Get()
        {
            return "value";

        }

		// GET: api/dashboard/5
        public object Get(int id)
        {

            //   var member = db.Member_Data.Where(p => p.id == id && p.type == 1);

           var member = db.Member_Data.Where(p => p.id == id).FirstOrDefault();
            int memberType = 0;
            if (member != null)
            {
                memberType = member.type;
            }
             
            //id = 147;

            int NotSchoolGoingChildren = 0;
            int todayVisit = 0;
            int totalSchool = 0;

            int totalHouseHoldVisit = 0;

            if (id != 0)
            {
                //int totalChil = db.ChildrenList_Data.Where(ii_ => ii_.ss_id == id).Count();
                int totalChil = 0;
                int schoolgoingchildren = 0;
                int Notschoolgoingchildren = 0;
                int totalOhousehold = 0;
                int totalOutschoolGoingchildrenDeworming = 0;
                int complit = 0;
                int due = 0;
                int totalschoolGoingchildrenDeworming = 0;
                int todayCompleted = 0;
                int monthlyCompleted = 0;
                if (memberType == 1) // 1 means PO
                {
                    string date = DateTime.Now.ToString("yyyy-MM-dd");

                    todayVisit = db.ChildrenList_Data.SqlQuery("SELECT * FROM children_list INNER JOIN dose_list ON children_list.id = dose_list.children_id WHERE children_list.po_id = '" + member.id + "' and dose_list.date like '" + date + "%'").Count();
                    schoolgoingchildren = db.ChildrenList_Data.Where(ii_ => ii_.school_id != 0 && ii_.age > 5 && ii_.r_3 != "" && ii_.po_id == member.id).Count();
                    NotSchoolGoingChildren = db.ChildrenList_Data.Where(ii_ => ii_.school_id != 0 && ii_.age > 5 && ii_.r_3 != "" && ii_.po_id == member.id).Count();
                    Notschoolgoingchildren = db.ChildrenList_Data.Where(ii_ => ii_.school_id != 0 && ii_.age > 5 && ii_.r_3 != "" && ii_.po_id == member.id).Count();

                    totalSchool = db.ChildrenList_Data.SqlQuery("SELECT * FROM `children_list` WHERE po_id ='"+member.id+"' GROUP BY school_id").Count();

                    totalChil = db.ChildrenList_Data.Where(ii_ => ii_.po_id == id && ii_.r_3 != "").Count();

                    totalOhousehold = db.ChildrenList_Data.Where(ii_ => ii_.po_id == id && ii_.age > 5 && ii_.r_3 != "" && ii_.school_id != 0 && (ii_.child_num == "0" || ii_.child_num == "1")).Count();
                    totalHouseHoldVisit = db.ChildrenList_Data.SqlQuery("SELECT * FROM children_list INNER JOIN dose_list ON children_list.id = dose_list.children_id WHERE children_list.po_id = '" + member.id + "' GROUP BY hh").Count();

                    //complit = db.DoseList_Data.Where(ii_ => ii_.ChildrenList.po_id == id).Count();
                    complit = db.ChildrenList_Data.SqlQuery("SELECT * FROM children_list INNER JOIN dose_list ON children_list.id = dose_list.children_id WHERE children_list.r_3 = '' and children_list.po_id = '" + member.id + "'").Count();

                    totalOutschoolGoingchildrenDeworming = db.DoseList_Data.Where(m => m.ChildrenList.school_id == 0 && m.ChildrenList.po_id == id).Count();
                    totalschoolGoingchildrenDeworming = db.DoseList_Data.Where(m => m.ChildrenList.school_id != 0 && m.ChildrenList.po_id == id).Count();
                    todayCompleted = db.DoseList_Data.Where(mm => mm.date.Year == DateTime.Now.Year && mm.date.Month == DateTime.Now.Month && mm.date.Day == DateTime.Now.Day && mm.ChildrenList.po_id == id).Count();
                    monthlyCompleted = db.DoseList_Data.Where(mm => mm.date.Year == DateTime.Now.Year && mm.date.Month == DateTime.Now.Month && mm.ChildrenList.po_id == id).Count();

                }
                else if (memberType == 2) // 2 means SK
                {
                    string date = DateTime.Now.ToString("yyyy-MM-dd");

                    todayVisit = db.ChildrenList_Data.SqlQuery("SELECT * FROM children_list INNER JOIN dose_list ON children_list.id = dose_list.children_id WHERE children_list.sk_id = '"+member.id+"' and dose_list.date like '"+ date + "%'").Count();
                    schoolgoingchildren = db.ChildrenList_Data.Where(ii_ => ii_.school_id != 0 && ii_.age > 5 && ii_.r_3 != "" && ii_.sk_id == member.id).Count();
                    NotSchoolGoingChildren = db.ChildrenList_Data.Where(ii_ => ii_.school_id == 0 && ii_.age > 5 && ii_.r_3 != "" && ii_.sk_id == member.id).Count();
                    Notschoolgoingchildren = db.ChildrenList_Data.Where(ii_ => ii_.school_id == 0 && ii_.age > 5 && ii_.r_3 != "" && ii_.sk_id == member.id).Count();

                    totalChil = db.ChildrenList_Data.Where(ii_ => ii_.sk_id == id && ii_.age > 5 && ii_.r_3 != "" && ii_.school_id == 0).Count();

                    totalOhousehold = db.ChildrenList_Data.Where(ii_ => ii_.sk_id == id && ii_.age > 5 && ii_.r_3 != "" && ii_.school_id == 0 && (ii_.child_num == "0" || ii_.child_num == "1")).Count();
                    totalHouseHoldVisit = db.ChildrenList_Data.SqlQuery("SELECT * FROM children_list INNER JOIN dose_list ON children_list.id = dose_list.children_id WHERE children_list.sk_id = '" + member.id + "' GROUP BY hh").Count();

                    //complit = db.DoseList_Data.Where(ii_ => ii_.ChildrenList.sk_id == id).Count();
                    complit = db.ChildrenList_Data.SqlQuery("SELECT * FROM children_list INNER JOIN dose_list ON children_list.id = dose_list.children_id WHERE children_list.r_3 = '' and children_list.sk_id = '" + member.id + "'").Count();

                    totalOutschoolGoingchildrenDeworming = db.DoseList_Data.Where(m => m.ChildrenList.school_id == 0 && m.ChildrenList.sk_id == id).Count();
                    totalschoolGoingchildrenDeworming  = db.DoseList_Data.Where(m => m.ChildrenList.school_id != 0 && m.ChildrenList.sk_id == id).Count();
                    todayCompleted = db.DoseList_Data.Where(mm => mm.date.Year == DateTime.Now.Year && mm.date.Month == DateTime.Now.Month && mm.date.Day == DateTime.Now.Day && mm.ChildrenList.sk_id == id).Count();
                    monthlyCompleted = db.DoseList_Data.Where(mm => mm.date.Year == DateTime.Now.Year && mm.date.Month == DateTime.Now.Month && mm.ChildrenList.sk_id == id).Count();

                }
                else if (memberType == 3) // 2 means SS
                {
                    totalChil = db.ChildrenList_Data.Where(ii_ => ii_.ss_id == id && ii_.age > 5 && ii_.r_3 != "" && ii_.school_id == 0).Count();
                    totalOhousehold = db.ChildrenList_Data.Where(ii_ => ii_.ss_id == id && ii_.age > 5 && ii_.r_3 != "" && ii_.school_id == 0 && (ii_.household_member == "0" || ii_.household_member == "1")).Count();
                    complit = db.DoseList_Data.Where(ii_ => ii_.ChildrenList.ss_id == id).Count();
                    totalOutschoolGoingchildrenDeworming = db.DoseList_Data.Where(m => m.ChildrenList.school_id == 0 && m.ChildrenList.ss_id == id).Count();
                    totalschoolGoingchildrenDeworming = db.DoseList_Data.Where(m => m.ChildrenList.school_id != 0 && m.ChildrenList.ss_id == id).Count();
                    todayCompleted = db.DoseList_Data.Where(mm => mm.date.Year == DateTime.Now.Year && mm.date.Month == DateTime.Now.Month && mm.date.Day == DateTime.Now.Day && mm.ChildrenList.ss_id == id).Count();
                    monthlyCompleted = db.DoseList_Data.Where(mm => mm.date.Year== DateTime.Now.Year && mm.date.Month == DateTime.Now.Month && mm.ChildrenList.ss_id == id).Count();

                }
                //int complit = db.DoseList_Data.Where(ii_ => ii_.ChildrenList.ss_id == id).Count();
                //int due = totalChil - complit;
               
                due = totalChil - complit;
                //int schoolgoingchildren = db.ChildrenList_Data.Where(ii_ => ii_.school_id != 0 && ii_.ss_id == id).Count();
                //int Notschoolgoingchildren = db.ChildrenList_Data.Where(ii_ => ii_.school_id == 0 && ii_.ss_id == id).Count();



                //household = from p in db.ChildrenList_Data.Where(ii_ => ii_.ss_id == id).GroupBy(i => i.hh)
                //               select new
                //               {
                //                   count = p.Count(),

                //               };

                // totalOhousehold = household.Count();



                //var outschoolGoingchildrenDeworming = from p in db.DoseList_Data.Where(m => m.ChildrenList.school_id == 0 && m.ChildrenList.ss_id==id).GroupBy(i => i.children_id)
                //                                      select new
                //                                      {
                //                                          count = p.Count(),

                //                                      };

                //totalOutschoolGoingchildrenDeworming = outschoolGoingchildrenDeworming.Count();

                int notdewormingoutofschoolgoingchildren = Notschoolgoingchildren - totalOutschoolGoingchildrenDeworming;



                //   var monthlyterget = db.Monthly_Target.OrderBy(mm => mm.id).FirstOrDefault();

                //   var childcount = db.ChildrenList_Data.Where(mm => mm.create_date == DateTime.Today.ToString()).ToList();
                // var todayterget = db.ChildrenList_Data.Where(mm => mm.create_date == DateTime.Today.ToString()).ToList();

                //int monthlyterget = totalChil / 5;
                int monthlyterget = NotSchoolGoingChildren / 5;

                //int totalschoolGoingchildrenDeworming = 0;


                //var schoolGoingchildrenDeworming = from p in db.DoseList_Data.Where(m => m.ChildrenList.school_id != 0).GroupBy(i => i.children_id)
                //                                   select new
                //                                   {
                //                                       count = p.Count(),

                //                                   };
                //totalschoolGoingchildrenDeworming = schoolGoingchildrenDeworming.Count();





                int notdewormingSchoolgoingchildren = schoolgoingchildren - totalschoolGoingchildrenDeworming;


                var data = new
                {
                    success = true,
                    children = totalChil,
                    completed = complit,
                    not_completed = due,
                    // po wise.......
                    po_school_list = totalSchool,
                    total_school_children = schoolgoingchildren,
                    school_children_completed = totalschoolGoingchildrenDeworming,
                    school_children_not_completed = notdewormingSchoolgoingchildren,

                    total_not_school_children = Notschoolgoingchildren,
                    school_not_children_completed = totalOutschoolGoingchildrenDeworming,
                    school_not_children_not_completed = notdewormingoutofschoolgoingchildren,
                    total_household = totalOhousehold,
                    total_visit = totalHouseHoldVisit,
                    total_not_visit = totalOhousehold - totalHouseHoldVisit,
                    //todays_target = Convert.ToInt32(monthlyterget.target)/30,
                    //todays_target_fullfil = todayterget.Count(),
                    //todays_target_due =( Convert.ToInt32(monthlyterget.target) / 30)- todayterget.Count(),
                    //monthly_target = monthlyterget.target,
                    //monthly_target_fullfil = childcount.Count(),
                    //monthly_target_due = Convert.ToInt32(monthlyterget.target) - childcount.Count() 
                    todays_target = todayVisit, //monthlyterget/30,
                    todays_target_fullfil = todayCompleted,
                    todays_target_due =(monthlyterget / 30)- todayCompleted,
                    monthly_target = monthlyterget,
                    monthly_target_fullfil = monthlyCompleted,
                    monthly_target_due = monthlyterget - monthlyCompleted
                };
                return data;
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "Data not found!"
                };
                return data;
            }
        }

		// POST: api/dashboard
        public object Post([FromBody]FormDataCollection formbody)
        {
			return "empty";
        }

		// PUT: api/dashboard/5
        public void Put(int id, [FromBody]string value)
        {
        }

		// DELETE: api/dashboard/5
        public void Delete(int id)
        {
        }
    }
}
