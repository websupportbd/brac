﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebVillageController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<VillageModel>();
            if (search == null || search == "")
            {
                totalRecord = db.Village_Data.Count();
                var Query = db.Village_Data.OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList();
            }
            else
            {
                totalRecord = db.Village_Data.Where(c => c.name.Contains(search)).Count();
                var Query = db.Village_Data.Where(c => c.name.Contains(search)).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList();
            }

            var obj = new VillageWebJson();
            foreach (var entity in results)
            {

                string action = null;
                action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/village/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>";

                var status = "";
                if (entity.ISACTIVE == 1)
                {
                    status = "Active";
                }
                else
                {
                    status = "Inactive";
                }

                obj.data.Add(new VillageWebListJson
                {
                    id = entity.id,
                    division = entity.Union.Upazila.District.Division.NAME,
                    district = entity.Union.Upazila.District.NAME,
                    upazila = entity.Union.Upazila.NAME,
                    union = entity.Union.name,
                    name = entity.name,
                    Status=status,
                    action = action
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }





        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Add(VillageModel cont)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Village_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "Village add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Session["uni_id"] = id;
            var cont = db.Village_Data.Where(c => c.id == id).FirstOrDefault();
            return View(cont);
        }


        [HttpPost]
        public ActionResult Edit(VillageModel cont)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Village update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.Village_Data.Where(at => at.id == id).SingleOrDefault();
            db.Village_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }



    }


    public class VillageWebJson
    {
        public VillageWebJson() { data = new List<VillageWebListJson>(); }
        public List<VillageWebListJson> data { get; set; }
    }
    public class VillageWebListJson
    {
        public int id { get; set; }
        public string division { get; set; }
        public string district { get; set; }
        public string upazila { get; set; }
        public string union { get; set; }
        public string name { get; set; }
        public string Status { get; set; }
        public string action { get; set; }
    }
}
