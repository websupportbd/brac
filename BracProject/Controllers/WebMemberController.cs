﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;
using System.Text.RegularExpressions;

namespace BracProject.Controllers
{
    public class WebMemberController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
           
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<MemberModel>();
            if (search == null || search == "")
            {
                totalRecord = db.Member_Data.Where(ii_ => ii_.status == 1).Count();
                var Query = db.Member_Data.Where(ii_ => ii_.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<MemberModel>();
            }
            else if(Regex.Match(search, "[0 - 9]").Success)
            {
                totalRecord = db.Member_Data.Where(c =>  c.mobile_no.Contains(search) && c.status == 1).Count();

                var Query = db.Member_Data.Where(c =>  c.mobile_no.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<MemberModel>();
            }
            else if (Regex.Match(search, "sk").Success)
            {
                totalRecord = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).Count();

                var Query = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<MemberModel>();
            }
            else if (Regex.Match(search, "ss").Success)
            {
                totalRecord = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).Count();

                var Query = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<MemberModel>();
            }
            else if (Regex.Match(search, "po").Success)
            {
                totalRecord = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).Count();

                var Query = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<MemberModel>();
            }
            else if (Regex.Match(search, "SK").Success)
            {
                totalRecord = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).Count();

                var Query = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<MemberModel>();
            }
            else if (Regex.Match(search, "SS").Success)
            {
                totalRecord = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).Count();

                var Query = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<MemberModel>();
            }
            else if (Regex.Match(search, "PO").Success)
            {
                totalRecord = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).Count();

                var Query = db.Member_Data.Where(c => c.MemberType.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<MemberModel>();
            }

            else
            {
                totalRecord = db.Member_Data.Where(c => c.name.Contains(search)   && c.status == 1).Count();

                var Query = db.Member_Data.Where(c => c.name.Contains(search)  && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<MemberModel>();
            }


            var obj = new MemberWebJson();
            foreach (var entity in results)
            {
                string upazila = "";
                string union = "";
                string village = "";

                if (entity.type == 1)
                {
                    var po = new List<AssignPoModel>();

                    var Query = db.AssignPo_Data.Where(c => c.po_id == entity.id).OrderByDescending(c => c.id).Skip(start).Take(take);
                    po = Query.ToList<AssignPoModel>();

                   
                    foreach (var pos in po)
                    {

                        if (pos.uz_ids != null)
                        {

                            string strHeader_Upz = pos.uz_ids;
                            string[] strTempIDS_Upz = strHeader_Upz.Split(',');
                            foreach (var a in strTempIDS_Upz)
                            {
                                int id = Convert.ToInt32(a);
                                var upz = db.Upazila_Data.Where(ii => ii.ID == id).FirstOrDefault();
                                if (upz != null)
                                {
                                    upazila += "Upazila: " + upz.NAME + ",";
                                }
                            }
                        }
                       
                        if (pos.ui_ids != null)
                        {

                            string strHeader_uni = pos.ui_ids;
                            string[] strTempIDS_uni = strHeader_uni.Split(',');
                            foreach (var b in strTempIDS_uni)
                            {
                                int id = Convert.ToInt32(b);
                                var upz = db.Union_Data.Where(ii => ii.id == id).FirstOrDefault();
                                if (upz != null)
                                {
                                    union += "Union: " + upz.name + ",";
                                }
                            }
                        }

                      
                        if (pos.vi_ids != null)
                        {

                            string strHeader_uni = pos.vi_ids;
                            string[] strTempIDS_uni = strHeader_uni.Split(',');
                            foreach (var b in strTempIDS_uni)
                            {
                                int id = Convert.ToInt32(b);
                                var upz = db.Village_Data.Where(ii => ii.id == id).FirstOrDefault();
                                if (upz != null)
                                {
                                    union += "Village: " + upz.name + ",";
                                }
                            }
                        }

                    }
                }


                if (entity.type == 2)
                {
                    var po = new List<AssignSkModel>();

                    var Query = db.AssignSk_Data.Where(c => c.sk_id == entity.id).OrderByDescending(c => c.id).Skip(start).Take(take);
                    po = Query.ToList<AssignSkModel>();


                    foreach (var pos in po)
                    {

                        if (pos.uz_ids != null)
                        {

                            string strHeader_Upz = pos.uz_ids;
                            string[] strTempIDS_Upz = strHeader_Upz.Split(',');
                            foreach (var a in strTempIDS_Upz)
                            {
                                int id = Convert.ToInt32(a);
                                var upz = db.Upazila_Data.Where(ii => ii.ID == id).FirstOrDefault();
                                if (upz != null)
                                {
                                    upazila += "Upazila: " + upz.NAME + ",";
                                }
                            }
                        }

                        if (pos.ui_ids != null)
                        {

                            string strHeader_uni = pos.ui_ids;
                            string[] strTempIDS_uni = strHeader_uni.Split(',');
                            foreach (var b in strTempIDS_uni)
                            {
                                int id = Convert.ToInt32(b);
                                var upz = db.Union_Data.Where(ii => ii.id == id).FirstOrDefault();
                                if (upz != null)
                                {
                                    union += "Union: " + upz.name + ",";
                                }
                            }
                        }


                        if (pos.vi_ids != null)
                        {

                            string strHeader_uni = pos.vi_ids;
                            string[] strTempIDS_uni = strHeader_uni.Split(',');
                            foreach (var b in strTempIDS_uni)
                            {
                                int id = Convert.ToInt32(b);
                                var upz = db.Village_Data.Where(ii => ii.id == id).FirstOrDefault();
                                if (upz != null)
                                {
                                    union += "Village: " + upz.name + ",";
                                }
                            }
                        }

                    }
                }


                if (entity.type == 3)
                {
                    var po = new List<AssignSsModel>();

                    var Query = db.AssignSs_Data.Where(c => c.ss_id == entity.id).OrderByDescending(c => c.id).Skip(start).Take(take);
                    po = Query.ToList<AssignSsModel>();


                    foreach (var pos in po)
                    {

                        

                        if (pos.para_id != null)
                        {

                            string strHeader_uni = pos.para_id.ToString();
                            string[] strTempIDS_uni = strHeader_uni.Split(',');
                            foreach (var b in strTempIDS_uni)
                            {
                                int id = Convert.ToInt32(b);
                                var upz = db.Para_Data.Where(ii => ii.id == id).FirstOrDefault();
                                if (upz != null)
                                {
                                    union += "Para: " + upz.name + ",";
                                }
                            }
                        }


                        if (pos.vill_id != null)
                        {

                            string strHeader_uni =  pos.vill_id.ToString();
                            string[] strTempIDS_uni = strHeader_uni.Split(',');
                            foreach (var b in strTempIDS_uni)
                            {
                                int id = Convert.ToInt32(b);
                                var upz = db.Village_Data.Where(ii => ii.id == id).FirstOrDefault();
                                if (upz != null)
                                {
                                    union += "Village: " + upz.name + ",";
                                }
                            }
                        }

                    }
                }




                   string areas = "";
                    if (upazila != "")
                    {
                        areas = upazila;
                    }
                      if (union != "")
                     {
                    areas = union;
                     }
                     if (village != "")
                     {
                    areas = village;
                     }




                obj.data.Add(new MemberWebListJson
                {
                    id = entity.id,
                    type = entity.MemberType.name,
                    name = entity.name,
                    area=areas,
                    mobileNo=entity.mobile_no,

                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/member_list/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'> Edit</i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'> Delete</i>" +
                        "</button>" +
                        "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult GetMemberType(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var div_list = db.MemberType_Data.OrderBy(d => d.id).ToList<MemberTypeModel>();
            foreach (var entity in div_list)
            {
                items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MemberTypeList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (Session["member_type_id"] == null)
            {
                var _list = db.MemberType_Data.OrderBy(d => d.id).ToList();
                foreach (var entity in _list)
                {
                    items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                }
            }
            else
            {
                int ID = Convert.ToInt32(Session["member_type_id"]);
                Session.Remove("member_type_id");
                var _list = db.MemberType_Data.OrderBy(d => d.id).ToList();
                foreach (var entity in _list)
                {
                    if (entity.id == ID)
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                    }
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        public ActionResult Add(MemberModel cont)
        {
            cont.status = 1;
            cont.created_at = DateTime.Now;
            cont.updated_at = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    db.Member_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "Member add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.Member_Data.Where(c => c.id == id).FirstOrDefault();
            Session["member_type_id"] = cont.type;
            return View(cont);
        }

        [HttpPost]
        public ActionResult Edit(MemberModel cont)
        {

            cont.updated_at = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.Entry(cont).Property(x => x.status).IsModified = false;
                    db.Entry(cont).Property(x => x.created_at).IsModified = false;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Member update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }

        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.Member_Data.Where(at => at.id == id).SingleOrDefault();
            db.Member_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }


    public class MemberWebJson
    {
        public MemberWebJson() { data = new List<MemberWebListJson>(); }
        public List<MemberWebListJson> data { get; set; }
    }
    public class MemberWebListJson
    {
        public int id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string mobileNo { get; set; }
        public string area { get; set; }

        public string action { get; set; }
    }
}
