﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebParaController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<ParaListModel>();
            if (search == null || search == "")
            {
                totalRecord = db.Para_Data.Count();
                var Query = db.Para_Data.OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList();
            }
            else
            {
                totalRecord = db.Para_Data.Where(c => c.name.Contains(search)).Count();
                var Query = db.Para_Data.Where(c => c.name.Contains(search)).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList();
            }

            var obj = new ParaWebJson();
            foreach (var entity in results)
            {

                string action = null;
                action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/para/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>";
                var status = "";
                if (entity.ISACTIVE == 1)
                {
                    status = "Active";
                }
                else
                {
                    status = "Inactive";
                }

                obj.data.Add(new ParaWebListJson
                {
                    id = entity.id,
                    division = entity.ward_list.Upazila.District.Division.NAME,
                    district = entity.ward_list.Upazila.District.NAME,
                    upazila = entity.ward_list.Upazila.NAME,
                    ward = entity.ward_list.name,
                    name = entity.name,
                    Status=status,
                    action = action
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }





        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Add(ParaListModel cont)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Para_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "Para add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Session["para_id"] = id;
            var cont = db.Para_Data.Where(c => c.id == id).FirstOrDefault();
            return View(cont);
        }


        [HttpPost]
        public ActionResult Edit(ParaListModel cont)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Para update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.Para_Data.Where(at => at.id == id).SingleOrDefault();
            db.Para_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }



    }


    public class ParaWebJson
    {
        public ParaWebJson() { data = new List<ParaWebListJson>(); }
        public List<ParaWebListJson> data { get; set; }
    }
    public class ParaWebListJson
    {
        public int id { get; set; }
        public string division { get; set; }
        public string district { get; set; }
        public string upazila { get; set; }
        public string ward { get; set; }
        public string name { get; set; }
        public string Status { get; set; }
        public string action { get; set; }
    }
}
