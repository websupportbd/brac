﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebAssignSsController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index2(int id)
        {
            Session["skid"] = id.ToString();
            var data = ListById();
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<AssignSsModel>();
            if (search == null || search == "")
            {
                totalRecord = db.AssignSs_Data.Count();
                var Query = db.AssignSs_Data.OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<AssignSsModel>();
            }
            else
            {
                totalRecord = db.AssignSs_Data.Where(c => c.SS.name.Contains(search)).Count();
                var Query = db.AssignSs_Data.Where(c => c.SS.name.Contains(search)).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<AssignSsModel>();
            }


            var obj = new SSAssignWebJson();
            foreach (var entity in results)
            {
                string SK_NAME = db.Member_Data.Where(ii_ => ii_.id == entity.sk_id).FirstOrDefault().name;
                obj.data.Add(new SSAssignWebListJson
                {
                    id = entity.id,
                    sk_name = SK_NAME,
                    ss_name = entity.SS.name,
                    village_name = entity.Village.name,
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/assign_ss/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'> Edit</i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'> Delete</i>" +                        
                        "</button>" +
                        
                        "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult ListById()
        {
            int totalRecord = 0;
            int sk_id = Convert.ToInt32(Session["skid"].ToString());

            var results = new List<AssignSsModel>();
            if (sk_id !=0)
            {
                totalRecord = db.AssignSs_Data.Count();
                var Query = db.AssignSs_Data.Where(oo=>oo.sk_id== sk_id).OrderByDescending(c => c.id);
                results = Query.ToList<AssignSsModel>();
            }
           


            var obj = new SSAssignWebJson();
            foreach (var entity in results)
            {
                string SK_NAME = db.Member_Data.Where(ii_ => ii_.id == entity.sk_id).FirstOrDefault().name;
                obj.data.Add(new SSAssignWebListJson
                {
                    id = entity.id,
                    sk_name = SK_NAME,
                    ss_name = entity.SS.name,
                    village_name = entity.Village.name,
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/assign_ss/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'> Edit</i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'> Delete</i>" +
                        "</button>" +
                         "<a href='/children_list/Index2/" + entity.ss_id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-forward'> Assigned</i>" +
                        "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(AssignSsModel cont)
        {

            if (ModelState.IsValid)
            {
                string strHeader = cont.ids;
                string[] strTempIDS = strHeader.Split(',');
                foreach (var a in strTempIDS)
                {
                    var insert_data = new AssignSsModel();
                    insert_data.sk_id = cont.sk_id;
                    insert_data.ss_id = cont.ss_id;
                    insert_data.vill_id = Convert.ToInt32(a);
                    insert_data.status = 1;
                    insert_data.ids = cont.ids;
                    insert_data.created_at = DateTime.Now;
                    insert_data.updated_at = DateTime.Now;
                    db.AssignSs_Data.Add(insert_data);
                    db.SaveChanges();
                }

                ModelState.Clear();
                cont = null;
                ViewBag.Message = "Assign Successfully";
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.AssignSs_Data.Where(c => c.id == id).FirstOrDefault();
            Session["sk_id_k"] = cont.sk_id;
            Session["ss_id_k"] = cont.ss_id;
            return View(cont);
        }

        [HttpPost]
        public ActionResult Edit(AssignSsModel cont)
        {

            cont.updated_at = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.Entry(cont).Property(x => x.status).IsModified = false;
                    db.Entry(cont).Property(x => x.created_at).IsModified = false;
                    db.Entry(cont).Property(x => x.ids).IsModified = false;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.AssignSs_Data.Where(at => at.id == id).SingleOrDefault();
            db.AssignSs_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult skList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (Session["sk_id_k"] == null)
            {
                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 2).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                }
            }
            else
            {
                int poID = Convert.ToInt32(Session["sk_id"]);
                //Session.Remove("sk_id");

                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 2).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    if (poID == entity.id)
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ssList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (Session["ss_id_k"] == null)
            {
                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 3).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    items.Add(new SelectListItem { Text = entity.name + '(' + entity.id + ')', Value = entity.id.ToString() });
                }
            }
            else
            {
                int poID = Convert.ToInt32(Session["ss_id_k"]);
                //Session.Remove("ss_id");

                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 3).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    if (poID == entity.id)
                    {
                        items.Add(new SelectListItem { Text = entity.name + '('+ entity.id+')', Value = entity.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = entity.name + '(' + entity.id + ')', Value = entity.id.ToString() });
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetVillage(string sk_id)
        {
            int DisID = Convert.ToInt32(sk_id);
            List<SelectListItem> items = new List<SelectListItem>();
            var upz_list = db.AssignListSk_Data.Where(up => up.AssignSk.sk_id == DisID).OrderBy(d => d.id).ToList();
            foreach (var entity in upz_list)
            {
                if (entity.village_id !=0 )
                {
                    var details = db.Village_Data.Where(ii => ii.id == entity.village_id).FirstOrDefault();
                    items.Add(new SelectListItem { Text = details.name, Value = details.id.ToString() });
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }
    }

    public class SSAssignWebJson
    {
        public SSAssignWebJson() { data = new List<SSAssignWebListJson>(); }
        public List<SSAssignWebListJson> data { get; set; }
    }
    public class SSAssignWebListJson
    {
        public int id { get; set; }
        public string sk_name { get; set; }
        public string ss_name { get; set; }
        public string village_name { get; set; }
        public string action { get; set; }
    }
}
