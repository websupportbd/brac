﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BracProject.Models;
using System.Net.Http.Formatting;
using System.Data.Entity;

namespace BracProject.Controllers
{
    public class SchoolListController : ApiController
    {
        private static MySqlCon db = new MySqlCon();
        // GET: api/SchoolList
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/SchoolList/5
        public object Get(int id)
        {
            var results = db.SchoolList_Data.OrderByDescending(x=>x.id).ToList();

            if (results.Count() != 0)
            {
                var obj = new ShhoolListJson();
                foreach (var entity in results)
                {
                    obj.schoolList.Add(new ShhoolList
                    {
                        id = entity.id,                       
                        school_name = entity.school_name
                    });
                }

                var data = new
                {
                    success = true,
                    count = results.Count(),
                    obj.schoolList
                };
                return data;
            }
            else
            {
                var data = new
                {
                    success = false,
                    message = "School List Not found!"
                };
                return data;
            }
        }

        // POST: api/SchoolList
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/SchoolList/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/SchoolList/5
        public void Delete(int id)
        {
        }
    }

    public class ShhoolListJson
    {
        public ShhoolListJson() { schoolList = new List<ShhoolList>(); }
        public List<ShhoolList> schoolList { get; set; }
    }
    public class ShhoolList
    {
        public int id { get; set; }
        public string school_name { get; set; }
        
    }

}
