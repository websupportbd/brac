﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http.Formatting;
using System.Web.Http;
using BracProject.Models;
using Newtonsoft.Json;

namespace BracProject.Controllers
{
    public class AreaController : ApiController
    {
        private MySqlCon db = new MySqlCon();

		// GET: api/area
        public object Get()
		{
            return "empty";
		}

		// GET: api/area/5
        public object Get(string userName, int ver)
		{
            int inbox_count = db.Inbox_Data.Where(inb => inb.READSTATUS == 0 && inb.FORMUSERNAME == userName).ToList<InboxModel>().Count();
			int max_version = db.Version_Data.Max(ve => ve.VER);
                          
            if (ver == 0 || ver != max_version)
            {
                var div_list = db.Division_Data.OrderBy(di => di.NAME).ToList<DivisionModel>();
                var dis_list = db.District_Data.OrderBy(ds => ds.NAME).ToList<DistrictModel>();
                var upz_list = db.Upazila_Data.OrderBy(up => up.NAME).ToList<UpazilaModel>();
				//var sk_list = db.SK_Data.OrderBy(up => up.name).ToList<SKModel>();
				var sk_list = db.AssignSk_Data.OrderBy(up => up.SK.name).ToList();
			//	var ss_list = db.SS_Data.OrderBy(up => up.name).ToList<SSModel>();
				var ss_list = db.AssignSs_Data.Select(s =>
                                new
                                {
                                    id = s.ss_id,
                                    sk_id=s.sk_id,
                                    name = s.SS.name
                                }).Distinct().OrderBy(o=> o.name).ToList();

				var village = db.Village_Data.OrderBy(pro => pro.name).ToList();

                var div_obj = new DivisionJson();
                foreach (var div_entity in div_list)
                {               
    				div_obj.division.Add(new DivisionListJson
    				{
    					id = div_entity.ID,
						eng_name = div_entity.NAME,
    					ban_name = div_entity.BANGLANAME,
    					bbscode = div_entity.ID.ToString()
    				});               
                }

                var dis_obj = new DistrictJson();
                foreach (var dis_entity in dis_list)
                {               
    				dis_obj.district.Add(new DistrictListJson
    				{
    					id = dis_entity.ID,
						eng_name = dis_entity.NAME,
    					ban_name = dis_entity.BANGLANAME,
    					bbscode = dis_entity.DIVISIONID.ToString()
    				});               
                }

                var upz_obj = new UpazilaJson();
                foreach (var upz_entity in upz_list)
                {               
    				upz_obj.upazila.Add(new UpazilaListJson
    				{
    					id = upz_entity.ID,
						eng_name = upz_entity.NAME,
    					ban_name = upz_entity.BANGLANAME,
    					bbscode = upz_entity.DISTRICTID.ToString()
    				});               
                }

                var sk_obj = new SKJson();
				foreach (var sk_entity in sk_list)
                {
					sk_obj.sk_list.Add(new SKListJson
    				{
						id = sk_entity.sk_id,
                        po_id = sk_entity.po_id,
						name = sk_entity.SK.name
    				});               
                }

				var ss_obj = new SSJson();
                foreach (var ss_entity in ss_list)
                {
                    ss_obj.ss_list.Add(new SSListJson
                    {
                        id = ss_entity.id,
						sk_id = ss_entity.sk_id,
                        name = ss_entity.name
                    });
                }


                var Village_obj = new VillageJson();
                foreach (var Village_entity in village)
                {
                    Village_obj.village_list.Add(new VillageListJson
                    {
                        id = Village_entity.id,
                        village_name = Village_entity.name
                    });
                }

                //var project_obj = new ProjuctJson();
                //foreach (var pro_entity in projects)
                //{
                //    project_obj.project_list.Add(new ProjuctListJson
                //    {
                //        id = pro_entity.ID,
                //        name = pro_entity.NAME,
                //        code_part1 = pro_entity.CODE_PART1,
                //        project_title = pro_entity.NAME
                //    });
                //}

                if (div_list.Count() != 0 || dis_list.Count() != 0 || upz_list.Count() != 0)
                {
                    var data = new
                    {
                        success = true,
                        div_obj.division,
                        dis_obj.district,
                        upz_obj.upazila,
                        sk_obj.sk_list,
                        ss_obj.ss_list,
                        Village_obj.village_list,
						inbox_count = inbox_count.ToString(),
                        version = max_version.ToString()
                    };
                    return data;
                }
                else
                {
                    var data = new
                    {
                        success = false,
                        message = "Data not found!"
                    };
                    return data;
                }
            }else{
                var data = new
                {
                    success = true,
                    inbox_count = inbox_count.ToString(),
                    version = "cashData",
					message = "Data upto date!"
				};
				return data;
            }
		}

		// POST: api/area
		public void Post([FromBody]string value)
		{
		}

		// PUT: api/area/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE: api/scheme/5
		public void Delete(int id)
		{
		}
    }

	public class DivisionJson
	{
		public DivisionJson() { division = new List<DivisionListJson>(); }
		public List<DivisionListJson> division { get; set; }
	}
	public class DivisionListJson
	{
		public int id { get; set; }
		public string eng_name { get; set; }
		public string ban_name { get; set; }
		public string bbscode { get; set; }
	}

	public class DistrictJson
	{
		public DistrictJson() { district = new List<DistrictListJson>(); }
		public List<DistrictListJson> district { get; set; }
	}
	public class DistrictListJson
	{
		public int id { get; set; }
		public string eng_name { get; set; }
		public string ban_name { get; set; }
		public string bbscode { get; set; }
	}

	public class UpazilaJson
	{
		public UpazilaJson() { upazila = new List<UpazilaListJson>(); }
		public List<UpazilaListJson> upazila { get; set; }
	}
	public class UpazilaListJson
	{
		public int id { get; set; }
		public string eng_name { get; set; }
		public string ban_name { get; set; }
		public string bbscode { get; set; }
	}

	public class SKJson
	{
		public SKJson() { sk_list = new List<SKListJson>(); }
		public List<SKListJson> sk_list { get; set; }
	}
	public class SKListJson
	{
		public int id { get; set; }
		public int po_id { get; set; }
		public string name { get; set; }
	}
    
	public class SSJson
    {
        public SSJson() { ss_list = new List<SSListJson>(); }
        public List<SSListJson> ss_list { get; set; }
    }
    public class SSListJson
    {
        public int id { get; set; }
        public int sk_id { get; set; }
        public string name { get; set; }
    }

	public class ProjuctJson
    {
        public ProjuctJson() { project_list = new List<ProjuctListJson>(); }
        public List<ProjuctListJson> project_list { get; set; }
    }
    public class ProjuctListJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code_part1 { get; set; }
        public string project_title { get; set; }
    }


    public class VillageJson
    {
        public VillageJson() { village_list = new List<VillageListJson>(); }
        public List<VillageListJson> village_list { get; set; }
    }
    public class VillageListJson
    {
        public int id { get; set; }
        public string village_name { get; set; }
    }
}
