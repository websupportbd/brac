﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebSkController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<SKModel>();
            if (search == null || search == "")
            {
                totalRecord = db.SK_Data.Where(ii_ => ii_.status == 1).Count();
                var Query = db.SK_Data.Where(ii_ => ii_.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<SKModel>();
            }
            else
            {
                totalRecord = db.SK_Data.Where(c => c.name.Contains(search) && c.status == 1).Count();
                var Query = db.SK_Data.Where(c => c.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<SKModel>();
            }


            var obj = new SkWebJson();
            foreach (var entity in results)
            {
                obj.data.Add(new SkWebListJson
                {
                    id = entity.id,
                    po_name = entity.PO.name,
                    upazila_name = entity.Upazila.NAME,
                    name = entity.name,
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/sk/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        public ActionResult Add(SKModel cont)
        {
            cont.status = 1;
            cont.created_at = DateTime.Now;
            cont.updated_at = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    db.SK_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "Sk add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }



        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.SK_Data.Where(c => c.id == id).FirstOrDefault();
            Session["po_id"] = cont.po_id;
            return View(cont);
        }

        [HttpPost]
        public ActionResult Edit(SKModel cont)
        {

            cont.updated_at = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.Entry(cont).Property(x => x.status).IsModified = false;
                    db.Entry(cont).Property(x => x.created_at).IsModified = false;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Sk update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.SK_Data.Where(at => at.id == id).SingleOrDefault();
            db.SK_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult poList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (Session["po_id"] == null)
            {
                var buyer_list = db.PO_Data.Where(v => v.status == 1).OrderBy(d => d.name).ToList();
                foreach (var entity in buyer_list)
                {
                    items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                }
            }
            else
            {
                int poID = Convert.ToInt32(Session["po_id"]);
                Session.Remove("po_id");

                var _list = db.PO_Data.Where(v => v.status == 1).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    if (poID==entity.id)
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString(), Selected = true });
                    }else{
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDivision(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var div_list = db.Division_Data.OrderBy(d => d.ID).ToList<DivisionModel>();
            foreach (var entity in div_list)
            {
                items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDistrict(string div_id)
        {
            int DivID = Convert.ToInt32(div_id);
            List<SelectListItem> items = new List<SelectListItem>();
            var dis_list = db.District_Data.Where(di => di.DIVISIONID == DivID).OrderBy(d => d.ID).ToList<DistrictModel>();
            foreach (var entity in dis_list)
            {
                items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetUpozilla(string dis_id)
        {
            int DisID = Convert.ToInt32(dis_id);
            List<SelectListItem> items = new List<SelectListItem>();
            var upz_list = db.Upazila_Data.Where(up => up.DISTRICTID == DisID).OrderBy(d => d.ID).ToList<UpazilaModel>();
            foreach (var entity in upz_list)
            {
                items.Add(new SelectListItem { Text = entity.NAME, Value = entity.ID.ToString() });
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }


    }


    public class SkWebJson
    {
        public SkWebJson() { data = new List<SkWebListJson>(); }
        public List<SkWebListJson> data { get; set; }
    }
    public class SkWebListJson
    {
        public int id { get; set; }
        public string po_name { get; set; }
        public string upazila_name { get; set; }
        public string name { get; set; }
        public string action { get; set; }
    }
}
