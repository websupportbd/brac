﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
	public class KhanaUpdateController : ApiController
	{


		private MySqlCon db = new MySqlCon();

		// GET: api/contact
		public IEnumerable<string> Get()
		{
			return new string[] { "empty" };
		}

		// GET: api/contact/5
		public object Get(int id)
		{
			var Query = db.ChildrenList_Data.Where(c => c.id == id);
			var details = Query.FirstOrDefault<ChildrenListModel>();
			string from_details_html="";

			from_details_html += db.UpdateTable_Data.Where(ii_ => ii_.id == 1).FirstOrDefault().details;


			if (details != null)
			{
				      
				string khana_no, khana_member, children_name, mother_name, father_name, age, gender_text, school_name, class_name, drinking_water_name, six_months_medicine_text, is_toylet_text;
				khana_no = details.hh;
				khana_member = details.household_member;
				children_name = details.children_name;
				mother_name = details.mother_name;
				father_name = details.father_name;
				age = details.age.ToString();

				from_details_html += "<span></span><section><div class='fb-text form-group field-text-1538330107247'><label for='text-1538330107247' class='fb-text-label'>১. খানা নং  </label><input value='" + khana_no + "' class='form-control' name='text-1538330107247' id='khana_no' type='text'></div></section>";
				from_details_html += "<span></span><section><div class='fb-number form-group field-number-1538330123622'><label for='number-1538330123622' class='fb-number-label'>২. খানা সদস্য সংখ্যা </label><input value='" + khana_member + "' class='form-control' name='number-1538330123622' id='members' type='number'></div></section>";
				from_details_html += "<span></span><section><div class='fb-text form-group field-text-1538330135990'><label for='text-1538330135990' class='fb-text-label'>৩. শিশুর নাম </label><input value='" + children_name + "' class='form-control' name='text-1538330135990' id='children_name' type='text'></div></section>";
				from_details_html += "<span></span><section><div class='fb-text form-group field-text-1538330151604'><label for='text-1538330151604' class='fb-text-label'>৪. মাতার নাম</label><input value='" + mother_name + "' class='form-control' name='text-1538330151604' id='mother_name' type='text'></div></section>";
				from_details_html += "<span></span><section><div class='fb-text form-group field-text-1538330165137'><label for='text-1538330165137' class='fb-text-label'>৫. পিতার নাম</label><input value='" + father_name + "' type='text' class='form-control' name='text-1538330165137' id='father_name'></textarea></div></section>";
				from_details_html += "<span></span><section><div class='fb-number form-group field-number-1538330215903'><label for='number-1538330215903' class='fb-number-label'>৬. বয়স </label><input value='" + age + "' class='form-control' name='number-1540172600411' id='age' type='number'></div></section>";

				from_details_html += "<span></span><section><div class='fb-select form-group field-select-1538330243560'><label for='select-1538330243560' class='fb-select-label'>৭. লিঙ্গ </label><select class='form-control' name='select-1538330243560' id='gender'>";
				if(details.gender == "1")
				{
					from_details_html += "<option value='1' selected='true' id='select-1538330243560-0'>ছেলে </option><option value='0' id='select-1538330243560-1'>মেয়ে </option>";
				}else{
					from_details_html += "<option value='1' id='select-1538330243560-0'>ছেলে </option><option value='0' selected='true' id='select-1538330243560-1'>মেয়ে </option>";
				}
				from_details_html += "</select></div></section>";

				from_details_html += "<span></span><section><div class='fb-select form-group field-select-1538330288282'><label for='select-1538330288282' class='fb-select-label'>৮. প্রতিষ্ঠানের নাম  </label><select class='form-control' name='select-1538330288282' id='school'><option value='0' selected='true' id='select-1538330288282-0'></option><option value='1' id='select-1538330288282-1'>Changmari Brac School</option><option value='2' id='select-1538330288282-2'>Natun Kuri </option><option value='3' id='select-1538330288282-3'>Cant. Public School &amp; College</option></select></div></section>";

				from_details_html += "<span></span><section><div class='fb-select form-group field-select-1538330553300'><label for='select-1538330553300' class='fb-select-label'>৯. শ্রেণী </label><select class='form-control' name='select-1538330553300' id='class'>";
				for (int i = 1; i < 11; i++)
                {
					int class_n = Convert.ToInt32(details.class_id);
					if(class_n == i)
					{
						from_details_html += "<option value='"+i+"' selected='true' id='select-1538330553300-0'></option>";
					}else{
						from_details_html += "<option value='" + i + "' id='select-1538330553300-0'></option>";
					}
                }
				from_details_html += "</select></div></section>";


				from_details_html += "<span></span><section><div class='fb-select form-group field-select-1538330634852'><label for='select-1538330634852' class='fb-select-label'>১০. গত ৬ মাসে কৃমিনাশক বড়ি খেয়েছে কি না ?  </label><select class='form-control' name='select-1538330634852' id='eat_capsul'>";
				if (details.six_months_medicine == 1)
                {
					from_details_html += "<option value='1' selected='true' id='select-1538330634852-1'>হ্যা</option>";
                }
                else
                {
					from_details_html += "<option value='0' id='select-1538330634852-2'>না </option>";
                }
                from_details_html += "</select></div></section>";

				from_details_html += "<span></span><section><div class='fb-select form-group field-select-1538330676348'><label for='select-1538330676348' class='fb-select-label'>১১. স্যানিটারি টয়লেট আছে কিনা ?  </label><select class='form-control' name='select-1538330676348' id='toylet'>";
				if (details.is_toylet == 1)
                {
                    from_details_html += "<option value='1' selected='true' id='select-1538330634852-1'>হ্যা</option>";
                }
                else
                {
                    from_details_html += "<option value='0' id='select-1538330634852-2'>না </option>";
                }
                from_details_html += "</select></div></section>";


				from_details_html += "<span></span><section><div class='fb-select form-group field-select-1538330738677'><label for='select-1538330738677' class='fb-select-label'>১২. খাবার পানির উৎস  </label><select class='form-control' name='select-1538330738677' id='water'>";
				if (details.is_toylet == 1)
                {
					from_details_html += "<option value='1' selected='true' id='select-1538330738677-1'>টিউবয়েল </option>";
                }
                else
                {
                    from_details_html += "<option value='0' id='select-1538330634852-2'></option>";
                }
                from_details_html += "</select></div></section>";



				from_details_html += db.UpdateTable_Data.Where(ii_ => ii_.id == 2).FirstOrDefault().details;

				var data = new
				{
					success = true,
					result = new[]
					{
						new { name = "১. খানা নং", value = details.hh }                  
					},
					from_details = from_details_html
				};
				return data;
			}
			else
			{
				var data = new
				{
					success = false,
					message = "Khana Details not found!"
				};
				return data;
			}
		}

		// POST: api/contact
		public object Post([FromBody]FormDataCollection formbody)
		{
			return "";

		}

		// PUT: api/contact/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE: api/contact/5
		public void Delete(int id)
		{
		}

	}
}