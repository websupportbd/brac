﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebAssignSkController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index2(int id)
        {
            Session["poid"] = id.ToString();
            var data = ListById();
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {

           
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<AssignSkModel>();
            if (search == null || search == "")
            {
                totalRecord = db.AssignSk_Data.Where(ii_ => ii_.status == 1).Count();
                var Query = db.AssignSk_Data.Where(ii_ => ii_.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<AssignSkModel>();
            }
            else
            {
                totalRecord = db.AssignSk_Data.Where(c => c.SK.name.Contains(search) && c.status == 1).Count();
                var Query = db.AssignSk_Data.Where(c => c.SK.name.Contains(search) && c.status == 1).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList<AssignSkModel>();
            }


            var obj = new SKAssignWebJson();
            foreach (var entity in results)
            {

                string union = "";
                if (entity.ui_ids != null)
                {

                    string strHeader_uni = entity.ui_ids;
                    string[] strTempIDS_uni = strHeader_uni.Split(',');
                    foreach (var b in strTempIDS_uni)
                    {
                        int id = Convert.ToInt32(b);
                        var upz = db.Union_Data.Where(ii => ii.id == id).FirstOrDefault();
                        if (upz != null)
                        {
                            union += "Union: " + upz.name + ",";
                        }
                    }
                }

                string village = "";
                if (entity.vi_ids != null)
                {

                    string strHeader_uni = entity.vi_ids;
                    string[] strTempIDS_uni = strHeader_uni.Split(',');
                    foreach (var b in strTempIDS_uni)
                    {
                        int id = Convert.ToInt32(b);
                        var upz = db.Village_Data.Where(ii => ii.id == id).FirstOrDefault();
                        if (upz != null)
                        {
                            union += "Village: " + upz.name + ",";
                        }
                    }
                }

                string areas = "";
                if (union != "")
                {
                    areas = union;
                }
                if (village != "")
                {
                    areas = village;
                }


                string PO_NAME = db.Member_Data.Where(ii_ => ii_.id == entity.po_id).FirstOrDefault().name;
                obj.data.Add(new SKAssignWebListJson
                {
                    id = entity.id,
                    po_name = PO_NAME,
                    sk_name = entity.SK.name,
                    village_name = areas,
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/assign_sk/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'> Edit</i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'> Delete</i>" +
                        "</button>" +
                         
                        "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }


        public ActionResult ListById()
        {
            int totalRecord = 0;
            int po_id = Convert.ToInt32(Session["poid"].ToString());

            var results = new List<AssignSkModel>();
            if (po_id != 0)
            {
                totalRecord = db.AssignSk_Data.Where(ii_ => ii_.status == 1).Count();
                var Query = db.AssignSk_Data.Where(ii_ => ii_.status == 1 && ii_.po_id== po_id).OrderByDescending(c => c.id);
                results = Query.ToList<AssignSkModel>();
            }
           


            var obj = new SKAssignWebJson();
            foreach (var entity in results)
            {

                string union = "";
                if (entity.ui_ids != null)
                {

                    string strHeader_uni = entity.ui_ids;
                    string[] strTempIDS_uni = strHeader_uni.Split(',');
                    foreach (var b in strTempIDS_uni)
                    {
                        int id = Convert.ToInt32(b);
                        var upz = db.Union_Data.Where(ii => ii.id == id).FirstOrDefault();
                        if (upz != null)
                        {
                            union += "Union: " + upz.name + ",";
                        }
                    }
                }

                string village = "";
                if (entity.vi_ids != null)
                {

                    string strHeader_uni = entity.vi_ids;
                    string[] strTempIDS_uni = strHeader_uni.Split(',');
                    foreach (var b in strTempIDS_uni)
                    {
                        int id = Convert.ToInt32(b);
                        var upz = db.Village_Data.Where(ii => ii.id == id).FirstOrDefault();
                        if (upz != null)
                        {
                            union += "Village: " + upz.name + ",";
                        }
                    }
                }

                string areas = "";
                if (union != "")
                {
                    areas = union;
                }
                if (village != "")
                {
                    areas = village;
                }


                string PO_NAME = db.Member_Data.Where(ii_ => ii_.id == entity.po_id).FirstOrDefault().name;
                obj.data.Add(new SKAssignWebListJson
                {
                    id = entity.id,
                    po_name = PO_NAME,
                    sk_name = entity.SK.name,
                    village_name = areas,
                    action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/assign_sk/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'> Edit</i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'> Delete</i>" +
                        "</button>" +
                         "<a href='/assign_ss/Index2/" + entity.sk_id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-forward'> Assigned</i>" +
                        "</a>" +
                        "</div>"
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(AssignSkModel cont)
        {

            if (ModelState.IsValid)
            {
                var insert_data = new AssignSkModel();
                insert_data.po_id = cont.po_id;
                insert_data.sk_id = cont.sk_id;
                insert_data.status = 1;
                insert_data.ui_ids = cont.ui_ids;
                insert_data.vi_ids = cont.vi_ids;
                insert_data.created_at = DateTime.Now;
                insert_data.updated_at = DateTime.Now;
                db.AssignSk_Data.Add(insert_data);
                db.SaveChanges();

                if (cont.ui_ids != null)
                {
                    string strHeader_Uni = cont.ui_ids;
                    string[] strTempIDS_Uni = strHeader_Uni.Split(',');
                    foreach (var b in strTempIDS_Uni)
                    {
                        var insert_data_list = new AssignListSkModel();
                        insert_data_list.sk_list_id = insert_data.id;
                        insert_data_list.union_id =Convert.ToInt32(b);
                        insert_data_list.status = 1;
                        insert_data_list.created_at = DateTime.Now;
                        insert_data_list.updated_at = DateTime.Now;
                        db.AssignListSk_Data.Add(insert_data_list);
                        db.SaveChanges();
                    }
                }

                if (cont.vi_ids != null)
                {
                    string strHeader_Vil = cont.vi_ids;
                    string[] strTempIDS_Vil = strHeader_Vil.Split(',');
                    foreach (var c in strTempIDS_Vil)
                    {
                        var insert_data_list = new AssignListSkModel();
                        insert_data_list.sk_list_id = insert_data.id;
                        insert_data_list.village_id = Convert.ToInt32(c);
                        insert_data_list.status = 1;
                        insert_data_list.created_at = DateTime.Now;
                        insert_data_list.updated_at = DateTime.Now;
                        db.AssignListSk_Data.Add(insert_data_list);
                        db.SaveChanges();
                    }
                }

                ModelState.Clear();
                cont = null;
                ViewBag.Message = "Assign Successfully";
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var cont = db.AssignSk_Data.Where(c => c.id == id).FirstOrDefault();
            Session["po_id_k"] = cont.po_id;
            Session["sk_id"] = cont.sk_id;
            Session["sk_ass_id"] = cont.id;
            return View(cont);
        }

        [HttpPost]
        public ActionResult Edit(AssignSkModel cont)
        {

            cont.updated_at = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.Entry(cont).Property(x => x.status).IsModified = false;
                    db.Entry(cont).Property(x => x.created_at).IsModified = false;
                    //db.Entry(cont).Property(x => x.ids).IsModified = false;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.AssignSk_Data.Where(at => at.id == id).SingleOrDefault();
            db.AssignSk_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult skList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (Session["sk_id"] == null)
            {
                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 2).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                }
            }
            else
            {
                int poID = Convert.ToInt32(Session["sk_id"]);
                //Session.Remove("sk_id");

                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 2).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    if (poID == entity.id)
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult poList(string searchText)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (Session["po_id_k"] == null)
            {
                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 1).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                }
            }
            else
            {
                int poID = Convert.ToInt32(Session["po_id_k"]);
                //Session.Remove("po_id");

                var _list = db.Member_Data.Where(v => v.status == 1 && v.type == 1).OrderBy(d => d.name).ToList();
                foreach (var entity in _list)
                {
                    if (poID == entity.id)
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = entity.name, Value = entity.id.ToString() });
                    }

                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetUpozilla(string po_id)
        {
            int DisID = Convert.ToInt32(po_id);
            List<SelectListItem> items = new List<SelectListItem>();

            var upz_list = db.AssignListPo_Data.Where(up => up.AssignPo.po_id == DisID).ToList();
            foreach (var entity in upz_list)
            {
                if(entity.union_id != 0)
                {
                    var list = db.Union_Data.Where(ii => ii.id == entity.union_id).FirstOrDefault();
                    items.Add(new SelectListItem { Text = list.name, Value = list.id.ToString() });
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetVillage(string po_id)
        {
            int DisID = Convert.ToInt32(po_id);
            List<SelectListItem> items = new List<SelectListItem>();

            var upz_list = db.AssignListPo_Data.Where(up => up.AssignPo.po_id == DisID).ToList();
            foreach (var entity in upz_list)
            {
                if (entity.village_id != 0)
                {
                    var list = db.Village_Data.Where(ii => ii.id == entity.village_id).FirstOrDefault();
                    items.Add(new SelectListItem { Text = list.name, Value = list.id.ToString() });
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetUnionList(string dis)
        {
            //int DisID = Convert.ToInt32(dis);
            int poID = Convert.ToInt32(Session["po_id_k"]);
            int skAssID = Convert.ToInt32(Session["sk_ass_id"]);

            string[] strTempIDS = null;
            if (skAssID != 0)
            {
                var details = db.AssignSk_Data.Where(ii => ii.id == skAssID).FirstOrDefault();
                if (details.ui_ids != null)
                {
                    if (details != null)
                    {
                        string strHeader = details.ui_ids;
                        strTempIDS = strHeader.Split(',');
                    }
                }
            }

            List<SelectListItem> items = new List<SelectListItem>();

            var upz_list = db.AssignListPo_Data.Where(up => up.AssignPo.po_id == poID).ToList();
            foreach (var entity in upz_list)
            {
                if (entity.union_id != 0)
                {
                    var list = db.Union_Data.Where(ii => ii.id == entity.union_id).FirstOrDefault();
                    bool select = false;
                    if (strTempIDS != null)
                    {
                        foreach (var a in strTempIDS)
                        {
                            int id = Convert.ToInt32(a);
                            if (id == entity.union_id)
                            {
                                select = true;
                            }
                        }
                    }

                    if (select)
                    {
                        items.Add(new SelectListItem { Text = list.name, Value = list.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = list.name, Value = list.id.ToString() });
                    }
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetVillageList(string po_id)
        {
            //int DisID = Convert.ToInt32(dis);
            int poID = Convert.ToInt32(Session["po_id_k"]);
            int skAssID = Convert.ToInt32(Session["sk_ass_id"]);

            string[] strTempIDS = null;
            if (skAssID != 0)
            {
                var details = db.AssignSk_Data.Where(ii => ii.id == skAssID).FirstOrDefault();
                if (details.vi_ids != null)
                {
                    if (details != null)
                    {
                        string strHeader = details.vi_ids;
                        strTempIDS = strHeader.Split(',');
                    }
                }
            }


            List<SelectListItem> items = new List<SelectListItem>();

            var upz_list = db.AssignListPo_Data.Where(up => up.AssignPo.po_id == poID).ToList();
            foreach (var entity in upz_list)
            {
                if (entity.village_id != 0)
                {
                    var list = db.Village_Data.Where(ii => ii.id == entity.village_id).FirstOrDefault();
                    bool select = false;
                    if (strTempIDS != null)
                    {
                        foreach (var a in strTempIDS)
                        {
                            int id = Convert.ToInt32(a);
                            if (id == entity.village_id)
                            {
                                select = true;
                            }
                        }
                    }

                    if (select)
                    {
                        items.Add(new SelectListItem { Text = list.name, Value = list.id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem { Text = list.name, Value = list.id.ToString() });
                    }
                }
            }


            return Json(items, JsonRequestBehavior.AllowGet);
        }



    }

    public class SKAssignWebJson
    {
        public SKAssignWebJson() { data = new List<SKAssignWebListJson>(); }
        public List<SKAssignWebListJson> data { get; set; }
    }
    public class SKAssignWebListJson
    {
        public int id { get; set; }
        public string po_name { get; set; }
        public string sk_name { get; set; }
        public string village_name { get; set; }
        public string action { get; set; }
    }
}
