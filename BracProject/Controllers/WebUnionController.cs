﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BracProject.Models;

namespace BracProject.Controllers
{
    public class WebUnionController : Controller
    {
        private MySqlCon db = new MySqlCon();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataTableRequest req)
        {
            int totalRecord = 0;
            int start = req.start;
            int take = req.length;
            string search = req.search.value;

            var results = new List<UnionModel>();
            if (search == null || search == "")
            {
                totalRecord = db.Union_Data.Count();
                var Query = db.Union_Data.OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList();
            }
            else
            {
                totalRecord = db.Union_Data.Where(c => c.name.Contains(search)).Count();
                var Query = db.Union_Data.Where(c => c.name.Contains(search)).OrderByDescending(c => c.id).Skip(start).Take(take);
                results = Query.ToList();
            }

            var obj = new UnionWebJson();
            foreach (var entity in results)
            {

                string action = null;
                action = "<div class='btn-group' role='group' aria-label='Basic example'> " +
                        "<a href='/union/Edit/" + entity.id + "' class='btn btn-icon btn-success btn-square'>" +
                        "<i class='icon-edit'></i>" +
                        "</a>" +
                        "<button val='" + entity.id + "' id='del' type='button' class='btn btn-icon btn-danger btn-square'>" +
                        "<i class='icon-android-delete'></i>" +
                        "</button>" +
                        "</div>";


                var status = "";
                if (entity.ISACTIVE == 1)
                {
                    status = "Active";
                }
                else
                {
                    status = "Inactive";
                }
                obj.data.Add(new UnionWebListJson
                {
                    id = entity.id,
                    division = entity.Upazila.District.Division.NAME,
                    district = entity.Upazila.District.NAME,
                    upazila = entity.Upazila.NAME,
                    name = entity.name,
                    Status = status,
                    action = action
                });
            }
            var data = new
            {
                recordsTotal = results.Count(),
                recordsFiltered = totalRecord,
                obj.data
            };
            return Json(data, JsonRequestBehavior.AllowGet);

        }





        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Add(UnionModel cont)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Union_Data.Add(cont);
                    db.SaveChanges();
                    ModelState.Clear();
                    cont = null;
                    ViewBag.Message = "Union add Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }

            return View(cont);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Session["uni_id"] = id;
            var cont = db.Union_Data.Where(c => c.id == id).FirstOrDefault();
            return View(cont);
        }


        [HttpPost]
        public ActionResult Edit(UnionModel cont)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(cont).State = EntityState.Modified;
                    db.SaveChanges();
                    ModelState.Clear();
                    ViewBag.Message = "Union update Successfully";
                }
                catch (Exception e)
                {
                    ViewBag.Message = e.Message;
                }
            }
            return View(cont);
        }


        [HttpDelete]
        public ActionResult Del(int id)
        {
            var del = db.Union_Data.Where(at => at.id == id).SingleOrDefault();
            db.Union_Data.Remove(del);
            db.SaveChanges();
            var data = new
            {
                success = true,
                message = "Delete successfully!"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }



    }


    public class UnionWebJson
    {
        public UnionWebJson() { data = new List<UnionWebListJson>(); }
        public List<UnionWebListJson> data { get; set; }
    }
    public class UnionWebListJson
    {
        public int id { get; set; }
        public string division { get; set; }
        public string district { get; set; }
        public string upazila { get; set; }
        public string name { get; set; }
        public string Status { get; set; }
        public string action { get; set; }
    }
}