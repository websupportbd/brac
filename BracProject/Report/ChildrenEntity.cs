﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BracProject.Report
{
    public class ChildrenEntity
    {
        public string ChildrenId { get; set; }
        public string ChildrenName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }

        public string ChildrenAge{ get; set; }
        public string Gender { get; set; }
        public string SchoolGoing { get; set; }
        public string VillageName { get; set; }

        public string UnionName { get; set; }
    }
}