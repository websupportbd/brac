﻿$(document).ready(function() {

    skList("");
    function skList(search_text) {
        $.ajax({
            type: "post",
            url: "/ss/skList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#sks').html(div_data);
            }
        });
    }

    $.ajax({
        type: "post",
        url: "/ss/GetVillage",
        data: { div_id: $('#sk_id').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var dis_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#village_id').val() == data[i].Value) {
                    dis_data = dis_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#villages').html(dis_data);
        }
    });

    $('#sks').change(function() {
        $.ajax({
            type: "post",
            url: "/ss/GetVillage",
            data: { div_id: $('#sks').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#villages').html(dis_data);
            }
        });
    });



});
