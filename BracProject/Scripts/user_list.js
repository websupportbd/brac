﻿$(document).ready(function() {
    $('#userlist').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/user_management/List",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "id" },
            { "data": "user_name" },
            { "data": "email" },
            { "data": "action" }
        ]
    });


    $("#userlist").on('click', '#del', function() {
        var att_id = $(this).attr('val');
        deleteAtt(att_id);
    });

    function deleteAtt(att_id) {
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this user?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                    url: "/user_management/Del/" + att_id,
                    type: "DELETE"
                })
                .done(function(data) {
                    swal("Deleted!", "Your file was successfully deleted!", "success");
                    location.reload();
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }
});
