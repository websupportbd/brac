﻿$(document).ready(function() {
    $('#userlist').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/usermanagement/List",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "id" },
            { "data": "user_name" },
            { "data": "email" },
            { "data": "action" }
        ]
    });
});