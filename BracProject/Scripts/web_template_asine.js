﻿$(document).ready(function() {
    $('#list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/template_asine/List",
            "type": "POST",
            "datatype": "json"
            },
        "language": {
            "search": "Search by Template Name:"
          },
        "order": [[3,"asc"]],
        "ordering": true,
        "columns": [
            { "data": "project_name" },
            { "data": "contact_name" },
            { "data": "scheme_name" },
            { "data": "temp_name" },
            { "data": "action" }
        ]
    });


    $("#list").on('click', '#del', function() {
        var att_id = $(this).attr('val');
        deleteAtt(att_id);
    });

    function deleteAtt(att_id) {
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                    url: "/template_asine/Del/" + att_id,
                    type: "DELETE"
                })
                .done(function(data) {
                    swal("Deleted!", "Your file was successfully deleted!", "success");
                    location.reload();
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }
});