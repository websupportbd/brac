﻿$(document).ready(function () {


    $.ajax({
        type: "get",
        url: "/staffreport/GetSKList",
        datatype: "json",
        traditional: true,
        success: function(data) {
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#sklist').html(div_data);
        }
    });

    var table = $('#_list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/staffreport/List",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "sl" },
            { "data": "date" },
            { "data": "ss_name" },
            { "data": "type" },
            { "data": "update_field" },
            { "data": "hh_no" },
            { "data": "children_name" }
        ],
    });

   
    $("#submit").click(function () {

        $.ajax({
            type: "post",
            url: "/staffreport/Summary",
            data: { skID: $('#sklist').val(), Fromdate: $('#from_date').val(), Todate: $('#to_date').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var table = "<style> table, th, td { border: 1px solid black; border-collapse: collapse; } th, td { padding: 5px; text-align: left; } </style> <table style='width:100%'> <tr> <th>Date</th> <th>Registration</th> <th>House Hold Visit</th> <th>Deworming</th> </tr> <tr> <td>"+data.sdate+"</td><td>"+data.totalReg+"</td> <td>"+data.totalVisit+"</td><td>"+data.totalDewo+"</td></tr> </table>";
                $('#summaryTable').html(table);

            }
        });

        var sk_id = "";
        var date = "";
        if ( $('#sklist').val() != ""){
            var skid = "sk:" + $('#sklist').val() + ";";
        }
        if ( $('#from_date').val() != ""){
            var from_date = "from_date:" + $('#from_date').val() + ";";
        }
        if ( $('#to_date').val() != ""){
            var to_date = "to_date:" + $('#to_date').val() + ";";
        }
        var all = skid + from_date + to_date;
        table.search(all).draw();
    });

     
});
