﻿$(document).ready(function() {
    $("#btnTotalDewarming").click(function () {
        window.location = '/WebDewarmingReport/ReportTotalDewarmingChildren';
    });
    $("#btnTotalDewarmingExcel").click(function () {
        window.location = '/WebDewarmingReport/ExportToExcelForDeworming';
    });

    $("#btnNotDewarmingPDF").click(function () {
        window.location = '/WebDewarmingReport/ReportTotalNotDewarmingChildren';
    });

    $("#btnTotalSchoolPDF").click(function () {
        window.location = '/WebDewarmingReport/ReportTotalSchoolGoingChildren';
    });

    $("#btnNotSchoolPDF").click(function () {
        window.location = '/WebDewarmingReport/ReportTotalNotSchoolGoingChildren';
    });

});