﻿$(document).ready(function() {

    poList("");
    function poList(search_text) {
        $.ajax({
            type: "post",
            url: "/sk/poList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#pos').html(div_data);
            }
        });
    }

    divList("");
    function divList(search_text) {
        $.ajax({
            type: "post",
            url: "/sk/GetDivision",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if ($('#division_id').val() == data[i].Value) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#divisions').html(div_data);
            }
        });
    }

    $.ajax({
        type: "post",
        url: "/sk/GetDistrict",
        data: { div_id: $('#division_id').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var dis_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#district_id').val() == data[i].Value) {
                    dis_data = dis_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#districts').html(dis_data);
        }
    });

    $.ajax({
        type: "post",
        url: "/sk/GetUpozilla",
        data: { dis_id: $('#district_id').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var upz_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#upazila_id').val() == data[i].Value) {
                    upz_data = upz_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#upazilas').html(upz_data);
        }
    });

    $('#divisions').change(function() {
        $.ajax({
            type: "post",
            url: "/sk/GetDistrict",
            data: { div_id: $('#divisions').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#districts').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#upazilas').html(empty_data);
            }
        });
    });

    $('#districts').change(function() {
        $.ajax({
            type: "post",
            url: "/sk/GetUpozilla",
            data: { dis_id: $('#districts').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#upazilas').html(upz_data);
            }
        });
    }); 


    


});
