﻿

$(document).ready(function() {
    $.ajax({
        type: "get",
        url: "/temlate_default/GetComponent",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#COMPONENT_ID').val() == data[i].Value) {
                    _data = _data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#Component').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/temlate_default/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#PROJECT_ID').val() == data[i].Value) {
                    _data = _data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }

            }
            $('#Project').html(_data);
        }
    });


 

    $('#TYPE').change(function() {
        var s_value = $("#TYPE").val();
        if (s_value == 1) {
            $("#project_list_").show();
            $("#is_default_").show();
           
        } else {
            $("#project_list_").hide();
            $("#is_default_").hide();
           
        }
    });

    $.ajax({
            type: "post",
            url: "/temlate_default/GetTempList",
            data: { comp: $('#COMPONENT_ID').val(), pro: $('#PROJECT_ID').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
            $(".dualList").bootstrapDualListbox('setRemoveAllLabel', true);
            $(".dualList").html("");
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    }
                }

                $(".dualList").bootstrapDualListbox('refresh', true);
            }
        });

    $('#Component').change(function() {
        $.ajax({
            type: "post",
            url: "/temlate_default/GetTempList",
            data: { comp: $('#Component').val(), pro: $('#Project').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
            $(".dualList").bootstrapDualListbox('setRemoveAllLabel', true);
            $(".dualList").html("");
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    }
                }

                $(".dualList").bootstrapDualListbox('refresh', true);
            }
        });
    });


    $("#submit").click(function() {
        var ids = $(".dualList").val();
        $("#TEMPLATE_IDS").val(ids);
    });

});