﻿$(document).ready(function() {


    $.ajax({
        type: "post",
        url: "/assign_sk/GetUnionList",
        data: { po_id: $('#po_id').val(), },
        datatype: "json",
        traditional: true,
        success: function(data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList2").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList2").append(option);
                }
            }
            $("#dualList2").bootstrapDualListbox('setRemoveAllLabel', true);
            $("#dualList2").bootstrapDualListbox('refresh', true);
        }
    });


    $.ajax({
        type: "post",
        url: "/assign_sk/GetVillageList",
        data: { po_id: $('#po_id').val(), },
        datatype: "json",
        traditional: true,
        success: function(data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList3").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList3").append(option);
                }
            }
            $("#dualList3").bootstrapDualListbox('setRemoveAllLabel', true);
            $("#dualList3").bootstrapDualListbox('refresh', true);
        }
        });


    skList("");
    function skList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_sk/skList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#sks').html(div_data);
            }
        });
    }

    poList("");
    function poList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_sk/poList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#pos').html(div_data);
            }
        });
    }




    $.ajax({
        type: "post",
        url: "/assign_sk/GetVillage",
        data: { po_id: $('#po_id').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var upz_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#vill_id').val() == data[i].Value) {
                    upz_data = upz_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#villages').html(upz_data);
        }
    });

    $('#pos').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_sk/GetVillage",
            data: { po_id: $('#po_id').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#villages').html(upz_data);
            }
        });
    }); 
    


});
