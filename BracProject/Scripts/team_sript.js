﻿
$(document).ready(function() {
    $('#_list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/team_mang/List",
            "type": "POST",
            "datatype": "json"
        },
        "language": {
    		"search": "Search by Team Name:"
  		},
        "columns": [
            { "data": "title" },
            { "data": "convenor" },
            { "data": "action" }
        ],
        "autoWidth": false
    });
});