﻿$(document).ready(function() {
    $('#userRoalList').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/usermanagement/RoleList",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "module_id" },
            { "data": "module_name" },
            { "data": "action" }
        ]
    });

    $("#userRoalList").on('click', '.user-roal-js', function() {
        var module_name = $(this).attr('id')
        $.ajax({
            type: "post",
            url: "/usermanagement/modifyuserrole",
            data: { module_name: module_name, user_id: $('#id').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;

            }
        });
    });

});