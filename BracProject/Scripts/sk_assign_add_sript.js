﻿$(document).ready(function() {

    skList("");
    function skList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_sk/skList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#sks').html(div_data);
            }
        });
    }

    poList("");
    function poList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_sk/poList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#pos').html(div_data);
            }
        });
    }

    $('#pos').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_sk/GetUpozilla",
            data: { po_id: $('#pos').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                $("#dualList2").bootstrapDualListbox('setRemoveAllLabel', true);
                $("#dualList2").html("");
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $("#dualList2").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $("#dualList2").append(option);
                    }
                }
                $("#dualList2").bootstrapDualListbox('refresh', true);             
            }
        });
    });

    $('#dualList2').on('change', function () {
        var union_ids = [];
        var ids = $("#dualList2").val();
        var objs = new Object();
        objs.union_id = ids
        union_ids.push(objs);
        var obj = JSON.stringify(union_ids).replace(/&/g, "^");
        var jsonParam = 'obj:' + obj;
        var url = "/assign_po/GetVillage/";
        AjaxManager.SendJson2(url, jsonParam, onSuccess, onFailed)
        function onSuccess(data) {
            $("#dualList3").bootstrapDualListbox('setRemoveAllLabel', true);
            $("#dualList3").html("");
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList3").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList3").append(option);
                }
            }
            $("#dualList3").bootstrapDualListbox('refresh', true);
        }
        function onFailed(error) {
            AjaxManager.MsgBox('error', 'center', 'Error', error.statusText,
                [{
                    addClass: 'btn btn-primary',
                    text: 'Ok',
                    onClick: function ($noty) {
                        $noty.close();
                    }
                }]);
        }

    });

    //function getVillage() {
    //    $.ajax({
    //        type: "post",
    //        url: "/assign_sk/GetVillage",
    //        data: { po_id: $('#pos').val() },
    //        datatype: "json",
    //        traditional: true,
    //        success: function(data) {
    //            $("#dualList3").bootstrapDualListbox('setRemoveAllLabel', true);
    //            $("#dualList3").html("");
    //            for (var i = 0; i < data.length; i++) {
    //                if (data[i].Selected) {
    //                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
    //                    $("#dualList3").append(option);
    //                } else {
    //                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
    //                    $("#dualList3").append(option);
    //                }
    //            }
    //            $("#dualList3").bootstrapDualListbox('refresh', true);
    //        }
    //    });
    //}
    

    var demo2 = $('.dualList').bootstrapDualListbox({
        nonSelectedListLabel: 'Village List',
        selectedListLabel: 'Selected Village',
        preserveSelectionOnMove: 'moved',
        moveOnSelect: false
    });

    //$("label[for*='bootstrap-duallistbox-nonselected-list_dualList']").html("Village List");
    //$("label[for*='bootstrap-duallistbox-selected-list_dualList']").html("Selected Village");

    
    $("#submit").click(function() {
        var ids2 = $("#dualList2").val();
        var ids3 = $("#dualList3").val();
        $("#ui_ids").val(ids2);
        $("#vi_ids").val(ids3);
    });

    var AjaxManager = {

        SendJson2: function (serviceUrl, jsonParams, successCallback, errorCallback) {
            jQuery.ajax({
                url: serviceUrl,
                async: false,
                type: "POST",
                data: "{" + jsonParams + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: successCallback,
                error: errorCallback
            });
        },
        MsgBox: function (messageBoxType, displayPosition, messageBoxHeaderText, messageText, buttonsArray) {
            var n = noty({
                textHeader: messageBoxHeaderText,
                text: messageText,
                type: messageBoxType,
                modal: true,
                dismissQueue: true,
                layout: displayPosition,
                theme: 'defaultTheme',
                buttons: buttonsArray
            });
            $(".btn-primary").focus();

        },


    };

});
