﻿$(document).ready(function() {

    skList("");
    function skList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_ss/skList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#sks').html(div_data);
            }
        });
    }

    ssList("");
    function ssList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_ss/ssList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#sss').html(div_data);
            }
        });
    }

    $('#sks').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_ss/GetVillage",
            data: { sk_id: $('#sks').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                $(".dualList").bootstrapDualListbox('setRemoveAllLabel', true);
                $(".dualList").html("");
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    }
                }
                $(".dualList").bootstrapDualListbox('refresh', true);
            }
        });
    }); 
    

    var demo2 = $('.dualList').bootstrapDualListbox({
        nonSelectedListLabel: 'Village List',
        selectedListLabel: 'Selected Village',
        preserveSelectionOnMove: 'moved',
        moveOnSelect: false
    });

    $("label[for*='bootstrap-duallistbox-nonselected-list_dualList']").html("Village List");
    $("label[for*='bootstrap-duallistbox-selected-list_dualList']").html("Selected Village");

    
    $("#submit").click(function() {
        var ids = $(".dualList").val();
        $("#ids").val(ids);
    });

});
