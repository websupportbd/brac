﻿$(document).ready(function() {
    $('#list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/scheme/List",
            "type": "POST",
            "datatype": "json"
        },
        "language": {
            "search": "Search by Scheme Name:"
          },
        "order": [[2,"asc"]],
        "ordering": true,
        "columns": [
        { "data": "upazila" },
            { "data": "contact_name" },
            { "data": "scheme_name" },
            { "data": "comp_name" },
            { "data": "action" }
        ]
    });


    $("#list").on('click', '#del', function() {
        var _id = $(this).attr('val');
        deleteCat(_id);
    });

    function deleteCat(cat_id) {
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                    url: "/scheme/Del/" + cat_id,
                    type: "DELETE"
                })
                .done(function(data) {
                    swal("Deleted!", "Successfully deleted!", "success");
                    location.reload();
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }

});