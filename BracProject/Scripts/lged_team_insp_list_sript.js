﻿
$(document).ready(function() {

    $(".datecontrol").datepicker({ dateFormat: "dd-mm-yy" }).val();


    $.ajax({
        type: "get",
        url: "/report_send/GetDivision",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Division').html(div_data);
        }
    });

    $('#Division').change(function() {
        $.ajax({
            type: "post",
            url: "/report_send/GetDistrict",
            data: { div_id: $('#Division').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#District').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#Upozilla').html(empty_data);
            }
        });
    });

    $('#District').change(function() {
        $.ajax({
            type: "post",
            url: "/report_send/GetUpozilla",
            data: { dis_id: $('#District').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Upozilla').html(upz_data);
            }
        });
    });

    $.ajax({
        type: "get",
        url: "/report_send/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Project').html(_data);
        }
    });

    $('#Project').change(function() {
        $.ajax({
            type: "post",
            url: "/report_send/GetContact",
            data: { div: $('#Division').val(), dis: $('#District').val(), upz: $('#Upozilla').val(), proj: $('#Project').val(), comp: $('#Component').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Contact').html(upz_data);
            }
        });
    });

    $('#Contact').change(function() {
        $.ajax({
            type: "post",
            url: "/report_send/GetScheme",
            data: { contactID: $('#Contact').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Scheme').html(upz_data);
            }
        });
    });

   var table = $('#_list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/lged_team_inspection/List",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "package" },
            { "data": "scheme_name" },
            { "data": "team_name" },
            { "data": "inspection_type" },
            { "data": "defect_type" },
            { "data": "defect_count" },
            { "data": "date" },
            { "data": "action" }
        ],
        "autoWidth": false
    });

    $.ajax({
        type: "get",
        url: "/lged_team_inspection/GetTeamList",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Team').html(div_data);
        }
    });

    $("#pdf_download").click(function() {
        var team_id = $("#Team").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        if(team_id == "" || from_date =="" || to_date ==""){
            alert("Please fillup Team Name, From Date and To Date");
        }else{
            window.location.href = "/lged_team_inspection/GetPdf?team_id="+team_id+"&fromDate="+from_date+"&toDate="+to_date;
        }
    });

    $("#submit").click(function() {
        var divID = "";
        var disID = "";
        var upzID = "";
        var projID = "";
        var cont = "";
        var sche = "";

        if ($('#Division').val() != "") {
            var divID = "div:" + $('#Division').val() + ";";
        }
        if ($('#District').val() != "") {
            var disID = "dis:" + $('#District').val() + ";";
        }
        if ($('#Upozilla').val() != "") {
            var upzID = "upz:" + $('#Upozilla').val() + ";";
        }
        if ($('#Project').val() != "") {
            var projID = "pro:" + $('#Project').val() + ";";
        }
        if ($('#Contact').val() != "") {
            var cont = "cont:" + $('#Contact').val() + ";";
        }
        if ($('#Scheme').val() != "") {
            var sche = "scheme:" + $('#Scheme').val() + ";";
        }
       

        var all = divID + disID + upzID + projID + cont + sche;
        table.search(all).draw();
    });
});