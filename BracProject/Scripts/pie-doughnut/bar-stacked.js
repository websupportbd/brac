


$(window).on("load", function () {
    var april2018school = 0;
    var april2018camp = 0;
    var april2018household = 0;

    var october2018school = 0;
    var october2018camp = 0;
    var october2018household = 0;

    var april2019school = 0;
    var april2019camp = 0;
    var april2019household = 0;

    var october2019school = 0;
    var october2019camp = 0;
    var october2019household = 0;


    getAllChldList("");
    function getAllChldList(search_text) {
        $.ajax({
            type: "post",
            url: "/dashboard/GetTtoalChildren",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function (data) {
                if (data != null) {

                    ///chart section--------------------------------------------------------------------------------

               


                    //chart1
                     april2018school = data.april_2018_Data_school;
                     april2018camp = data.april_2018_Data_camp;
                     april2018household = data.april_2018_Data_household;

                     october2018school = data.october_2018_Data_school;
                     october2018camp = data.october_2018_Data_camp;
                     october2018household = data.october_2018_Data_household;
                  

                     april2019school = data.april_2019_Data_school;
                     april2019camp = data.april_2019_Data_camp;
                     april2019household = data.april_2019_Data_household;


                     october2019school = data.october_2019_Data_school;
                     october2019camp = data.october_2019_Data_camp;
                     october2019household = data.october_2019_Data_household;

                    var ctx = $("#column-stacked");

                    // Chart Options
                    var chartOptions = {
                        title: {
                            display: false,
                            text: "Chart.js Column Chart - Stacked"
                        },
                        tooltips: {
                            mode: 'label'
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        responsiveAnimationDuration: 500,
                        scales: {
                            xAxes: [{
                                stacked: true,
                                display: true,
                                gridLines: {
                                    color: "#f3f3f3",
                                    drawTicks: false,
                                },
                                scaleLabel: {
                                    display: true,
                                }
                            }],
                            yAxes: [{
                                stacked: true,
                                display: true,
                                gridLines: {
                                    color: "#f3f3f3",
                                    drawTicks: false,
                                },
                                scaleLabel: {
                                    display: true,
                                }
                            }]
                        }
                    };

                    // Chart Data
                    var chartData = {
                        // labels: ["April'2018", "October'2018", "April'2019", "October'2019"],
                        labels: ["April'2018", "October'2018", "April'2019", "October'2019"],
                        datasets: [{
                            label: "At School",
                            data: [april2018school, october2018school, april2019school, october2019school],
                            backgroundColor: "#7C09AB",
                            // hoverBackgroundColor: "green(0,191,165,.8)",
                            borderColor: "transparent"
                        }, {
                            label: "At Camp",
                            data: [april2018camp, october2018camp, april2019camp, october2019camp],
                            backgroundColor: "#A8055C",
                            // hoverBackgroundColor: "green(29,233,182,.8)",
                            borderColor: "transparent"
                        },
                        {
                            label: "At Household",
                            data: [april2018household, october2018household, april2019household, october2019household],
                            backgroundColor: "#E8C90D",
                            // hoverBackgroundColor: "green(100,255,218,.8)",
                            borderColor: "transparent"
                        }]
                    };

                    var config = {
                        type: 'bar',

                        // Chart Options
                        options: chartOptions,

                        data: chartData
                    };

                    // Create the chart
                    var lineChart = new Chart(ctx, config);




                }
            }
        });
    }




});



















$(window).on("load", function () {

    // Get the context of the Chart canvas element we want to select


});