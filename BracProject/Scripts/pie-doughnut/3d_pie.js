﻿var one = 0;
var two = 0;
var three = 0;
var four = 0;
var five = 0;
var six = 0;

var first = 0;
var second = 0;

var one1 = 0;
var two1 = 0;
var three1 = 0;
var four1 = 0;
google.load('visualization', '1.0', { 'packages': ['corechart'] });

$(window).on("load", function () {



    getAllChldList("");
    function getAllChldList(search_text) {
        $.ajax({
            type: "post",
            url: "/dashboard/GetTtoalChildren",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function (data) {
                if (data != null) {



                    //chart3
                    one = data.TotalSchoolGoingChildren;
                    two = data.NotSchoolGoingChildren;


                    //chart4
                    three = data.school_going_girls;
                    four = data.outof_School_GoingGirls;
                    five = data.school_going_boys;
                    six = data.outof_School_GoingBoys;
                    drawPie3d3();
                    drawPie3d4();
                    // Load the Visualization API and the corechart package.
                    
                }
            }
        });
    }
    

    getAllChldList1("");

    function getAllChldList1(search_text) {
        $.ajax({
            type: "post",
            url: "/dashboard/GetTtoalChildren",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function (data) {
                if (data != null) {

                    ///chart section--------------------------------------------------------------------------------

                    //chart1
                    first = data.TotalDewormingChild;
                    second = data.TotalNotDewormingChild;
                    //chart2
                    one1 = data.total_school_children_deworming;
                    two1 = data.total_outofschool_children_deworming;
                    three1 = data.not_deworming_schoolGoing_children;
                    four1 = data.not_deworming_Outof_schoolGoing_children;

                    drawPie3d1();
                    drawPie3d2()

                    //Get the context of the Chart canvas element we want to select
 /*                   var ctx = $("#simple-pie-chart");




                    // Chart Options
                    var chartOptions = {
                        responsive: true,
                        maintainAspectRatio: false,
                        responsiveAnimationDuration: 500,
                    };

                    // Chart Data
                    var chartData = {
                        labels: [
                            "Total child deworming ",
                            "Total child not receiving deworming "],
                        datasets: [{
                            label: "My First dataset",
                            data: [first, second],
                            backgroundColor: ["#A8055C", "#7C09AB"], //,"#FF847C","#E84A5F"
                        }]
                    };
                    var config = {
                        type: 'pie',
                        // Chart Options
                        options: chartOptions,
                        data: chartData
                    };

                    // Create the chart
                    var pieSimpleChart = new Chart(ctx, config);
                    

                    var chart = $("#simplechart1");

                    // Chart Options
                    var chartOptionsNew = {
                        responsive: true,
                        maintainAspectRatio: false,
                        responsiveAnimationDuration: 500,
                    };


                    // Chart Data
                    var Data = {
                        labels: ["Deworming School going Children ",
                            "Deworming out of school children",
                            " Not Deworming School going Children",
                            "Not Deworming out of school children"
                        ],
                        datasets: [{
                            label: "My First dataset",
                            data: [one1, two1, three1, four1],
                            backgroundColor: ["#A8055C", "#E8900D", "#0DB3E8", "#E8C90D"],
                        }]
                    };
                    var configeration = {
                        type: 'pie',

                        // Chart Options
                        options: chartOptionsNew,
                        data: Data
                    };

                    // Create the chart
                    var pieSimpleChart = new Chart(chart, configeration);
                    */
                    
                }
            }
        });
    }
});



//Hiden Chart Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawPie3d);
// Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
function drawPie3d() {

    // Create the data table.
    var data = google.visualization.arrayToDataTable([

        ['Task', 'Hours per Day'],
        ['Out of school children-girls vhghghj', three],
        ['Out of school children-boys', four],
        ['School going  children-girls', five],
        ['School going  children-boys', six],

    ]);


    // Set chart options
    var options_pie3d = {

        is3D: true,
        height: 400,
        fontSize: 12,
        colors: ["#E8C90D", "#A8055C", "#0DB3E8", "#E8900D"],
        chartArea: {
            left: '5%',
            width: '90%',
            height: 350
        },
    };

    // Instantiate and draw our chart, passing in some options.
    var pie3d = new google.visualization.PieChart(document.getElementById('pie-3d'));
    pie3d.draw(data, options_pie3d);

}




//Deworming Chart 1 Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawPie3d);
// Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
function drawPie3d1() {

    // Create the data table.
    var data = google.visualization.arrayToDataTable([

        ['Task', 'Hours per Day'],
        ['Total child deworming  ', first],
        ['Total child not receiving deworming', second],
        

    ]);


    // Set chart options
    var options_pie3d = {

        is3D: true,
        height: 400,
        fontSize: 12,
        colors: ["#E8C90D", "#A8055C", "#0DB3E8", "#E8900D"],
        chartArea: {
            left: '20%',
            width: '100%',
            height: 550
        },
    };

    // Instantiate and draw our chart, passing in some options.
    var pie3d = new google.visualization.PieChart(document.getElementById('simple-pie-chart'));
    pie3d.draw(data, options_pie3d);

}

//Deworming Chart 2  Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawPie3d);
// Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
function drawPie3d2() {

    // Create the data table.
    var data = google.visualization.arrayToDataTable([
       
        ['Task', 'Hours per Day'],
        ['Deworming School going Children', one1],
        ['Deworming out of school children', two1],
        ['Not Deworming School going Children', three1],
        ['Not Deworming out of school children', four1],
       
    ]);


    // Set chart options
    var options_pie3d = {
      
        is3D: true,
        height: 400,
        fontSize: 12,
        colors: ["#E8C90D", "#A8055C", "#0DB3E8", "#E8900D"],
        chartArea: {
            left: '5%',
            width: '90%',
            height: 350
        },
    };

    // Instantiate and draw our chart, passing in some options.
    var pie3d = new google.visualization.PieChart(document.getElementById('simplechart1'));
    pie3d.draw(data, options_pie3d);

}


//Deworming Chart 3  Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawPie3d);
// Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
function drawPie3d3() {

    // Create the data table.
    var data = google.visualization.arrayToDataTable([

        ['Task', 'Hours per Day'],
        ['School going Children', one],
        ['Out of school children', two],

    ]);


    // Set chart options
    var options_pie3d = {

        is3D: true,
        height: 400,
        fontSize: 12,
        colors: ["#E8C90D", "#A8055C", "#0DB3E8", "#E8900D"],
        chartArea: {
            left: '5%',
            width: '90%',
            height: 350
        },
    };

    // Instantiate and draw our chart, passing in some options.
    var pie3d = new google.visualization.PieChart(document.getElementById('simple-doughnut-chart'));
    pie3d.draw(data, options_pie3d);

}


//Deworming Chart 4  Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawPie3d);
// Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
function drawPie3d4() {

    // Create the data table.
    var data = google.visualization.arrayToDataTable([

        ['Task', 'Hours per Day'],
        ['School going  children-girls', three],
        ['Out of school children-girls', four],
        ['Out of school children-boys', five],
        ['School going  children-boys', six],

    ]);


    // Set chart options
    var options_pie3d = {

        is3D: true,
        height: 400,
        fontSize: 12,
        colors: ["#E8C90D", "#A8055C", "#0DB3E8", "#E8900D"],
        chartArea: {
            left: '5%',
            width: '90%',
            height: 350
        },
    };

    // Instantiate and draw our chart, passing in some options.
    var pie3d = new google.visualization.PieChart(document.getElementById('simple-doughnut-chart_1'));
    pie3d.draw(data, options_pie3d);

}




// Resize chart
// ------------------------------

$(function () {

    // Resize chart on menu width change and window resize
    $(window).on('resize', resize);
    $(".menu-toggle").on('click', resize);

    // Resize function
    function resize() {
        drawPie3d();
    }
});