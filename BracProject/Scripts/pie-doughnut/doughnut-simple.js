/*=========================================================================================
    File Name: doughnut.js
    Description: Chartjs simple doughnut chart
    ----------------------------------------------------------------------------------------
    Item Name: Robust - Responsive Admin Theme
    Version: 1.2
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Doughnut chart
// ------------------------------
$(window).on("load", function(){
/*
    var one = 0;
    var two =0;
    var three = 0;
    var four = 0;
    var five =0;
    var six = 0;



    getAllChldList("");
    function getAllChldList(search_text) {
        $.ajax({
            type: "post",
            url: "/dashboard/GetTtoalChildren",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function (data) {
                if (data != null) {
                   


                    //chart3
                    one=data.TotalSchoolGoingChildren;
                    two=data.NotSchoolGoingChildren;


                    //chart4
                    three=data.school_going_girls;
                    four=data.outof_School_GoingGirls;
                    five=data.school_going_boys;
                    six=data.outof_School_GoingBoys;


                    var ctx = $("#simple-doughnut-chart");




                    // Chart Optionsvar
                    var chartOptions = {
                        responsive: true,
                        maintainAspectRatio: false,
                        responsiveAnimationDuration: 500,
                    };

                    // Chart Data
                    var chartData = {
                        labels: ["School going Children (" + ((one * 100) / (one + two)).toFixed() + "%)", "Out of school children (" + ((two * 100) / (one + two)).toFixed() + "%)"],
                        //text: ["School going Children (" + ((one * 100) / (one + two)).toFixed() + "%)", "Out of school children (" + ((two * 100) / (one + two)).toFixed() + "%)"],
                        datasets: [{
                            label: "My First dataset",
                            data: [one, two],
                            backgroundColor: ["#7C09AB", "#E8C90D"],
                        }]
                    };

                    var config = {
                        type: 'doughnut',

                        // Chart Options
                        options: chartOptions,

                        data: chartData
                    };

                    // Create the chart
                    var doughnutSimpleChart = new Chart(ctx, config);
                   




                    var chart1 = $("#simple-doughnut-chart_1");

                    // Chart Options
                    var chartOptions1 = {
                        responsive: true,
                        maintainAspectRatio: false,
                        responsiveAnimationDuration: 500,
                    };

                    // Chart Data
                    var chartData1 = {
                        labels: ["Out of school children-girls", "Out of school children-boys", "School going  children-girls", "School going  children-boys"],
                        datasets: [{
                            label: "My First dataset",
                            data: [three, four, five, six],
                            backgroundColor: ["#E8C90D", "#A8055C", "#0DB3E8", "#E8900D"],
                        }]
                    };

                    var config1 = {
                        type: 'doughnut',

                        // Chart Options
                        options: chartOptions1,

                        data: chartData1
                    };

                    // Create the chart
                    var doughnutSimpleChart = new Chart(chart1, config1);
                     */

                }
            }
        });
    }


    //Get the context of the Chart canvas element we want to select
   


});