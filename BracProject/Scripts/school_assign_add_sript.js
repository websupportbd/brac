﻿$(document).ready(function() {

    poList("");
    function poList(search_text) {
        $.ajax({
            type: "post",
            url: "/school_assign/poList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#pos').html(div_data);
            }
        });
    }

    $('#pos').change(function() {
        $.ajax({
            type: "post",
            url: "/school_assign/GetSchool",
            data: { po_id: $('#pos').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                $(".dualList").bootstrapDualListbox('setRemoveAllLabel', true);
                $(".dualList").html("");
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    }
                }
                $(".dualList").bootstrapDualListbox('refresh', true);
            }
        });
    }); 
    

    var demo2 = $('.dualList').bootstrapDualListbox({
        nonSelectedListLabel: 'School List',
        selectedListLabel: 'Selected School',
        preserveSelectionOnMove: 'moved',
        moveOnSelect: false
    });

    $("label[for*='bootstrap-duallistbox-nonselected-list_dualList']").html("School List");
    $("label[for*='bootstrap-duallistbox-selected-list_dualList']").html("Selected School");

    
    $("#submit").click(function() {
        var ids = $(".dualList").val();
        $("#ids").val(ids);
    });

});
