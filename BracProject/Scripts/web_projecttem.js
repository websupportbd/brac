﻿$(document).ready(function() {

    $(".datecontrol").datepicker({ dateFormat: "dd-mm-yy" }).val();

    var table = $('#list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/projecttem/List",
            "type": "POST",
            "datatype": "json"
        },
        "language": {
            "search": "Search by Template Name:"
        },
        "order": [
            [1, "asc"]
        ],
        "ordering": true,
        "columns": [
            { "data": "type" },
            { "data": "temp_name" },
            { "data": "action" }
        ]
    });

    $.ajax({
        type: "get",
        url: "/contact/GetComponent",
        datatype: "json",
        traditional: true,
        success: function(data) {
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Component').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/report/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Project').html(_data);
        }
    });


    $("#submit").click(function() {
        var comp = "";
        var projID = "";
        var typeID = "";
        var from_date = "";
        var to_date = "";

        if ($('#Component').val() != "") {
            var comp = "comp:" + $('#Component').val() + ";";
        }
        if ($('#Project').val() != "") {
            var projID = "pro:" + $('#Project').val() + ";";
        }
        if ($('#Type').val() != "") {
            var typeID = "typ:" + $('#Type').val() + ";";
        }
        /*if ($('#from_date').val() != "") {
            var from_date = "from_date:" + $('#from_date').val() + ";";
        }
        if ($('#to_date').val() != "") {
            var to_date = "to_date:" + $('#to_date').val() + ";";
        }*/

        var all = comp + projID + typeID + from_date + to_date;
        table.search(all).draw();
    });

    $("#list").on('click', '#del', function() {
        var _id = $(this).attr('val');
        deleteCat(_id);
    });

    function deleteCat(cat_id) {
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                    url: "/projecttem/Del/" + cat_id,
                    type: "DELETE"
                })
                .done(function(data) {
                    swal("Deleted!", "Successfully deleted!", "success");
                    location.reload();
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }

});