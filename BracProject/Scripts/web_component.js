﻿

$(document).ready(function() {
    $('#list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/component/List",
            "type": "POST",
            "datatype": "json"
            },
        "language": {
            "search": "Search by Component Name:"
          },
        "order": [[1,"asc"]],
        "ordering": true,
        "columns": [
            { "data": "project_name" },
            { "data": "comp_name" },
            { "data": "comp_title" }
        ]
    });
});