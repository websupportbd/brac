﻿
$(document).ready(function() {
    $.ajax({
        type: "get",
        url: "/temlate_default/GetComponent",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Component').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/temlate_default/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Project').html(_data);
        }
    });

    $("#project_list_").hide();
    $("#is_default_").hide();
 

    $('#TYPE').change(function() {
        var s_value = $("#TYPE").val();
        if (s_value == 1) {
            $("#project_list_").show();
            $("#is_default_").show();
           
        } else {
            $("#project_list_").hide();
            $("#is_default_").hide();
           
        }
    });

    $('#Component').change(function() {
        $.ajax({
            type: "post",
            url: "/temlate_default/GetTemp",
            data: { comp: $('#Component').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
            $(".dualList").bootstrapDualListbox('setRemoveAllLabel', true);
            $(".dualList").html("");
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    }
                }

                $(".dualList").bootstrapDualListbox('refresh', true);
            }
        });
    });



    $("#submit").click(function() {
        var ids = $(".dualList").val();
        $("#TEMPLATE_IDS").val(ids);
    });

});