﻿$(document).ready(function () {
    var id = 0;
    $('#_list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/member_list/List",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "type" },
            { "data": "name" },
            { "data": "area" },
             { "data": "mobileNo" },
            { "data": "action" }
        ],
        "autoWidth": false
    });

    $("#_list").on('click', '#del', function() {
        var att_id = $(this).attr('val');
        deleteAtt(att_id);
    });

    function deleteAtt(att_id) {
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                    url: "/member_list/Del/" + att_id,
                    type: "DELETE"
                })
                .done(function(data) {
                    swal("Deleted!", "Your file was successfully deleted!", "success");
                    location.reload();
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }

    //memberType("");
    //function memberType(search_text) {
    //    $.ajax({
    //        type: "post",
    //        url: "/member_list/GetMemberType",
    //        data: { searchText: search_text },
    //        datatype: "json",
    //        traditional: true,
    //        success: function (data) {
    //            var div_data = '<option value="0">--Select--</option>';
    //            for (var i = 0; i < data.length; i++) {
                    
    //                    div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                   
    //            }
    //            $('#membertype').html(div_data);
    //        }
    //    });
    //}


    //$('#membertype').change(function () {
    //    $.ajax({
    //        type: "post",
    //        url: "/ss/GetVillage",
    //        data: { div_id: $('#sks').val() },
    //        datatype: "json",
    //        traditional: true,
    //        success: function (data) {
    //            var dis_data = '<option value="">--Select--</option>';
    //            for (var i = 0; i < data.length; i++) {
    //                dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
    //            }
    //            $('#villages').html(dis_data);
    //        }
    //    });
    //});

});