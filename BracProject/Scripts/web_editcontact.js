﻿$(document).ready(function() {

    $(".datecontrol").datepicker({ dateFormat: "dd-mm-yy" }).val();

    $("._year").datepicker({
        dateFormat: 'MM yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('MM yy', new Date(year, month, 1)));
        }
    });
    $("._year").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });

    $.ajax({
        type: "get",
        url: "/contact/GetDivision",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#DIV_ID').val() == data[i].Value) {
                    div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#Division').html(div_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/contact/GetFinancialYear",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#FINANCIAL_YEAR').val() == data[i].Value) {
                    div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#financial_years').html(div_data);
        }
    });

    $.ajax({
        type: "post",
        url: "/contact/GetDistrict",
        data: { div_id: $('#DIV_ID').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var dis_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#DIS_ID').val() == data[i].Value) {
                    dis_data = dis_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#District').html(dis_data);
        }
    });

    $.ajax({
        type: "post",
        url: "/contact/GetUpozilla",
        data: { dis_id: $('#DIS_ID').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var upz_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#UP_ID').val() == data[i].Value) {
                    upz_data = upz_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#Upozilla').html(upz_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/contact/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#PROJECT_ID').val() == data[i].Value) {
                    _data = _data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#Project').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/contact/GetComponent",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#COMPONENT_ID').val() == data[i].Value) {
                    _data = _data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#Component').html(_data);
        }
    });

    $('#Division').change(function() {
        $.ajax({
            type: "post",
            url: "/contact/GetDistrict",
            data: { div_id: $('#Division').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#District').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#Upozilla').html(empty_data);
            }
        });
    });

    $('#District').change(function() {
        $.ajax({
            type: "post",
            url: "/contact/GetUpozilla",
            data: { dis_id: $('#District').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Upozilla').html(upz_data);
            }
        });
    });


});