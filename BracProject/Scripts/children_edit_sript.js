﻿
$(document).ready(function() {

    var div_data = '<option value="">--Select--</option>';
    
  if ($('#ChildrenCategory').val() == 0) {
        div_data = div_data + '<option selected=true  value=0> New Children </option>';    
        div_data = div_data + '<option   value=2> Graduated </option>';
        div_data = div_data + '<option   value=1> Migrated </option>';
        div_data = div_data + '<option   value=3> Lost to follow up </option>';
    }
    else if ($('#ChildrenCategory').val() == 1) {
        div_data = div_data + '<option selected=true  value=1> Migrated </option>';
        div_data = div_data + '<option   value=0> New Children </option>';            
        div_data = div_data + '<option   value=2> Graduated </option>';
        div_data = div_data + '<option   value=3> Lost to follow up </option>';
    }
    else if ($('#ChildrenCategory').val() == 2) {
        div_data = div_data + '<option selected=true  value=2> Graduated </option>';
        div_data = div_data + '<option   value=0> New Children </option>';
        div_data = div_data + '<option   value=1> Migrated </option>';    ;       
        div_data = div_data + '<option   value=3> Lost to follow up </option>';
    }
    else if ($('#ChildrenCategory').val() == 3) {
        div_data = div_data + '<option selected=true  value=3> Lost to follow up </option>';
        div_data = div_data + '<option   value=1> Migrated </option>';
        div_data = div_data + '<option   value=0> New Children </option>';      
        div_data = div_data + '<option   value=2> Graduated </option>';
       
    }
  $('#ChildrenCategory').html(div_data);


    var status_date = '<option value="">--Select--</option>';
    if ($('#status').val() == 1) {
        status_date = status_date + '<option  selected=true value=1> Active </option>';
        status_date = status_date + '<option  value=0> InActive </option>';
    }
    else {
        status_date = status_date + '<option selected=true  value=0> InActive </option>';
        status_date = status_date + '<option   value=1> Active </option>';
    }
    $('#Status').html(status_date);



    var side_effect = '<option value="">--Select--</option>';
    if ($('#sideeffect').val() == 1) {
        side_effect = side_effect + '<option  selected=true value=1> YES </option>';
        side_effect = side_effect + '<option  value=0> NO </option>';
    }
    else {
        side_effect = side_effect + '<option selected=true  value=0> NO </option>';
        side_effect = side_effect + '<option   value=1> YES </option>';
    }
    $('#sideeffect').html(side_effect);




    var dirnkingwater = '<option value="">--Select--</option>';
    if ($('#drinking_water_id').val() == 1) {
        dirnkingwater = dirnkingwater + '<option  selected=true value=1> Yes </option>';
        dirnkingwater = dirnkingwater + '<option  value=0> No </option>';
    }
    else {
        dirnkingwater = dirnkingwater + '<option selected=true  value=0> No </option>';
        dirnkingwater = dirnkingwater + '<option   value=1> Yes </option>';
    }
    $('#DrinkingWater').html(dirnkingwater);


    var toylet = '<option value="">--Select--</option>';
    if ($('#is_toylet').val() == 1) {
        toylet = toylet + '<option  selected=true value=1> Yes </option>';
        toylet = toylet + '<option  value=0> No </option>';
    }
    else {
        toylet = toylet + '<option selected=true  value=0> No </option>';
        toylet = toylet + '<option   value=1> Yes </option>';
    }
    $('#Toylet').html(toylet);


    var medicine= '<option value="">--Select--</option>';
    if ($('#six_months_medicine').val() == 1) {
        medicine = medicine + '<option  selected=true value=1> Yes </option>';
        medicine = medicine + '<option  value=0> No </option>';
    }
    else {
        medicine = medicine + '<option selected=true  value=0> No </option>';
        medicine = medicine + '<option   value=1> Yes </option>';
    }
    $('#Medicine').html(medicine);


    var gender = '<option value="">--Select--</option>';
    if ($('#gender').val() == 1) {
        gender = gender + '<option  selected=true value=1> Boy </option>';
        gender = gender + '<option  value=0> Girl </option>';
    }
    else {
        gender = gender + '<option selected=true  value=0> Girl </option>';
        gender = gender + '<option   value=1> Boy </option>';
    }
    $('#Gender').html(gender);





    schoolList("");
    function schoolList(search_text) {
        $.ajax({
            type: "post",
            url: "/children_list/GetSchoolList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function (data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if ($('#school_id').val() == data[i].Value) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#School').html(div_data);
            }
        });
    }


    classList("");
    function classList(search_text) {
        $.ajax({
            type: "post",
            url: "/children_list/GetClassList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function (data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if ($('#class_id').val() == data[i].Value) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#Class').html(div_data);
            }
        });
    }


    unionList("");
    function unionList(search_text) {
        $.ajax({
            type: "post",
            url: "/children_list/GetUnion",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function (data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if ($('#union_id').val() == data[i].Value) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#Union').html(div_data);
            }
        });
    }


    villageList("");
    function villageList(search_text) {
        $.ajax({
            type: "post",
            url: "/children_list/GetVillage",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function (data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if ($('#village_id').val() == data[i].Value) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#Village').html(div_data);
            }
        });
    }


    sslList("");
    function sslList(search_text) {
        $.ajax({
            type: "post",
            url: "/children_list/GetSSList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function (data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if ($('#ss_id').val() == data[i].Value) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#SSLIST').html(div_data);
            }
        });
    }


});

