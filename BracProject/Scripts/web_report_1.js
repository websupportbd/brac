﻿$(document).ready(function() {

    $(".datecontrol").datepicker({ dateFormat: "dd-mm-yy" }).val();

    $.ajax({
        type: "get",
        url: "/report/GetDivision",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Division').html(div_data);
        }
    });

    $('#Division').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetDistrict",
            data: { div_id: $('#Division').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#District').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#Upozilla').html(empty_data);
            }
        });
    });

    $('#District').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetUpozilla",
            data: { dis_id: $('#District').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Upozilla').html(upz_data);
            }
        });
    });

    $.ajax({
        type: "get",
        url: "/report/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Project').html(_data);
        }
    });

    $('#Project').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetContact",
            data: { div: $('#Division').val(), dis: $('#District').val(), upz: $('#Upozilla').val(), proj: $('#Project').val(), comp: $('#Component').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Contact').html(upz_data);
            }
        });
    });

    $('#Contact').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetScheme",
            data: { contactID: $('#Contact').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Scheme').html(upz_data);
            }
        });
    });


    $.ajax({
        type: "get",
        url: "/report_user/GetOffice",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Office').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/report_user/GetDesignation",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Designation').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/report/GetUsers",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Users').html(_data);
        }
    });

    var table = $('#schemeReport').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/report/List",
            "type": "POST",
            "datatype": "json"
        },
        "language": {
            "search": "Search by Name:"
        },
        "columns": [

            { "data": "user_name" },
            { "data": "scheme_name" },
            { "data": "date_" },
            { "data": "visit" },
            { "data": "action" }
        ]
    });

    $("#submit").click(function() {
        var divID = "";
        var disID = "";
        var upzID = "";
        var projID = "";
        var cont = "";
        var sche = "";
        var office = "";
        var dsignation = "";
        var users = "";
        var from_date = "";
        var to_date = "";

        if ($('#Division').val() != "") {
            var divID = "div:" + $('#Division').val() + ";";
        }
        if ($('#District').val() != "") {
            var disID = "dis:" + $('#District').val() + ";";
        }
        if ($('#Upozilla').val() != "") {
            var upzID = "upz:" + $('#Upozilla').val() + ";";
        }
        if ($('#Project').val() != "") {
            var projID = "pro:" + $('#Project').val() + ";";
        }
        if ($('#Contact').val() != "") {
            var cont = "cont:" + $('#Contact').val() + ";";
        }
        if ($('#Scheme').val() != "") {
            var sche = "scheme:" + $('#Scheme').val() + ";";
        }
        /*if ($('#Office').val() != "") {
            var office = "office:" + $('#Office').val() + ";";
        }
        if ($('#Designation').val() != "") {
            var dsignation = "dsignation:" + $('#Designation').val() + ";";
        }*/
        if ($('#Users').val() != "") {
            var users = "users:" + $('#Users').val() + ";";
        }
        if ($('#from_date').val() != "") {
            var from_date = "from_date:" + $('#from_date').val() + ";";
        }
        if ($('#to_date').val() != "") {
            var to_date = "to_date:" + $('#to_date').val() + ";";
        }

        var all = divID + disID + upzID + projID + cont + sche + office + dsignation + users + from_date + to_date;
        table.search(all).draw();
    });

});