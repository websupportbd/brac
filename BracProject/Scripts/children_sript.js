﻿$(document).ready(function () {
   
    var table = $('#_list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/children_list/List",
            "type": "POST",
            "datatype": "json"
        },
        "language": {
            "search": "Search by unique id:"
        },

        "columns": [
            {
                'render': function (data, type, full, meta) {
                    return '<input type="checkbox" class="check_rowForCompany" name="id[]" value="'
                        + $('<div/>').text(data).html() + '">';
                }
            },
            { "data": "unique_id" },
            { "data": "children_name" },
            { "data": "father_name" },
            { "data": "children_age" },
            { "data": "gender" },
            { "data": "school_going" },
            { "data": "villegeName" },
            { "data": "unionName" },
            { "data": "householdMember" },
            { "data": "childrenNumber" },
            { "data": "schoolName" },
            { "data": "takenMedicinePlace" },
            { "data": "mother_name" },
            { "data": "school_verification" },



            { "data": "action" }

        ],



    });

    var dt = $('table').DataTable();

   // dt.columns(0).visible($(this).is(':checked', false));
    //dt.columns([1, 1]).visible($(this).is(':checked', false));
    //dt.columns([1, 1]).visible($(this).is(':checked', true));
    //dt.columns([2, 2]).visible($(this).is(':checked', false));
    //dt.columns([3, 3]).visible($(this).is(':checked', false));
    //dt.columns([4, 4]).visible($(this).is(':checked', false));
    //dt.columns([5, 5]).visible($(this).is(':checked', false));
    //dt.columns([6, 6]).visible($(this).is(':checked', false));
    //dt.columns([7, 7]).visible($(this).is(':checked', false));
    //dt.columns([8, 8]).visible($(this).is(':checked', false));
    //dt.columns([9, 9]).visible($(this).is(':checked', false));
    //dt.columns([10, 10]).visible($(this).is(':checked', false));
    //dt.columns([11, 11]).visible($(this).is(':checked', false));
    //dt.columns([12, 12]).visible($(this).is(':checked', false));
    //dt.columns([13, 13]).visible($(this).is(':checked', false));

    if ($('#chkUniqueId').is(':checked')) {
        //dt.columns([1, 1]).visible($(this).is(':checked', true));
    } else {
        dt.columns([1, 1]).visible($(this).is(':checked', true));
        dt.columns([2, 2]).visible($(this).is(':checked', false));
        dt.columns([3, 3]).visible($(this).is(':checked', false));
        dt.columns([4, 4]).visible($(this).is(':checked', false));
        dt.columns([5, 5]).visible($(this).is(':checked', false));
        dt.columns([6, 6]).visible($(this).is(':checked', false));
        dt.columns([7, 7]).visible($(this).is(':checked', false));
        dt.columns([8, 8]).visible($(this).is(':checked', false));
        dt.columns([9, 9]).visible($(this).is(':checked', false));
        dt.columns([10, 10]).visible($(this).is(':checked', false));
        dt.columns([11, 11]).visible($(this).is(':checked', false));
        dt.columns([12, 12]).visible($(this).is(':checked', false));
        dt.columns([13, 13]).visible($(this).is(':checked', false));
    }

    $('#chkSelectAll').on('click', function () {
        if (this.checked) {
            $('.checkbox').each(function () {
                this.checked = true;
                dt.columns(0).visible($(this).is(':checked'));
                dt.columns([1, 1]).visible($(this).is(':checked'));
                dt.columns([2, 2]).visible($(this).is(':checked'));
                dt.columns([3, 3]).visible($(this).is(':checked'));
                dt.columns([4, 4]).visible($(this).is(':checked'))
                dt.columns([5, 5]).visible($(this).is(':checked'))
                dt.columns([6, 6]).visible($(this).is(':checked'))
                dt.columns([7, 7]).visible($(this).is(':checked'))
                dt.columns([8, 8]).visible($(this).is(':checked'))
                dt.columns([9, 9]).visible($(this).is(':checked'))
                dt.columns([10, 10]).visible($(this).is(':checked'))
                dt.columns([11, 11]).visible($(this).is(':checked'))
                dt.columns([12, 12]).visible($(this).is(':checked'))
                dt.columns([13, 13]).visible($(this).is(':checked'))
            });
        } else {
            $('.checkbox').each(function () {
                this.checked = false;
                dt.columns(0).visible($(this).is(':checked', false));
                dt.columns([1, 1]).visible($(this).is(':checked', false));
                dt.columns([2, 2]).visible($(this).is(':checked', false));
                dt.columns([3, 3]).visible($(this).is(':checked', false));
                dt.columns([4, 4]).visible($(this).is(':checked', false));
                dt.columns([5, 5]).visible($(this).is(':checked', false));

                dt.columns([6, 6]).visible($(this).is(':checked', false));
                dt.columns([7, 7]).visible($(this).is(':checked', false));
                dt.columns([8, 8]).visible($(this).is(':checked', false));
                dt.columns([9, 9]).visible($(this).is(':checked', false));
                dt.columns([10, 10]).visible($(this).is(':checked', false));
                dt.columns([11, 11]).visible($(this).is(':checked', false));
                dt.columns([12, 12]).visible($(this).is(':checked', false));
                dt.columns([13, 13]).visible($(this).is(':checked', false));
            });
        }
    });

    //var x1 = document.getElementById("chkUniqueId").checked;
    //if (x1 == true) {
    //    $("#chkUniqueId").prop( "checked", true );
    //}

    $('#chkUniqueId').change(function () {
        dt.columns([1, 1]).visible($(this).is(':checked'))
    });

    $('#chkChildrenName').change(function () {
        dt.columns([2, 2]).visible($(this).is(':checked'))
    })
    $('#chkFatherName').change(function () {
        dt.columns([3, 3]).visible($(this).is(':checked'))
    })
    $('#chkAge').change(function () {
        dt.columns([4, 4]).visible($(this).is(':checked'))
    })
    $('#chkGender').change(function () {
        dt.columns([5, 5]).visible($(this).is(':checked'))
    })
    $('#chkSchoolGoing').change(function () {
        dt.columns([6, 6]).visible($(this).is(':checked'))
    })

    $('#chkVillageName').change(function () {
        dt.columns([7, 7]).visible($(this).is(':checked'))
    })
    $('#chkUnionName').change(function () {
        dt.columns([8, 8]).visible($(this).is(':checked'))
    })
    $('#chkHouseHold').change(function () {
        dt.columns([9, 9]).visible($(this).is(':checked'))
    })
    $('#chkChildrenNumber').change(function () {
        dt.columns([10, 10]).visible($(this).is(':checked'))
    })
    $('#chkSchoolName').change(function () {
        dt.columns([11, 11]).visible($(this).is(':checked'))
    })
    $('#chkMedicinePlace').change(function () {
        dt.columns([12, 12]).visible($(this).is(':checked'))
    })
    $('#chkMotherName').change(function () {
        dt.columns([13, 13]).visible($(this).is(':checked'))
    })

 
    $('#example-select-all').click(function (e) {
        var $cb = $(this);
        var gridSummary = $("#_list").data();
        var selectedItem = gridSummary.dataItem(gridSummary.select());
    });
 


    $("#submit").click(function() {
        var children_name = "";
        var father_name = "";
        var age = "";
        var gender = "";
        
        if ( $('#children_name').val() != ""){
            var children_name = "cn:" + $('#children_name').val() + ";";
        }
        if ( $('#father_name').val() != ""){
            var father_name = "fn:" + $('#father_name').val() + ";";
        }
        if ( $('#age').val() != ""){
            var age = "age:" + $('#age').val() + ";";
        }
        if ( $('#gender').val() != ""){
            var gender = "gender:" + $('#gender').val() + ";";
        }

        var all = children_name + father_name + age + gender;
        table.search(all).draw();
    });

    $("#btnExport").click(function () {

       window.location = '/WebChildren/ExportToExcel';
       
    //    table.$('input[type="checkbox"]').each(function () {
    //        // If checkbox doesn't exist in DOM
    //        if (!$.contains(document, this)) {
    //            // If checkbox is checked
    //            if (this.checked) {
    //                // Create a hidden element 
    //                $(form).append(
    //                   $('<input>')
    //                      .attr('type', 'hidden')
    //                      .attr('name', this.name)
    //                      .val(this.value)
    //                );
    //            }
    //        }
    //    });
    });

   

    

 
    //$("#chkUniqueId").prop("checked", false);
    //$("#chkChildrenName").prop("checked", false);
    //$("#chkFatherName").prop("checked", false);
    //$("#chkAge").prop("checked", false);
    //$("#chkGender").prop("checked", false);
    //$("#chkSchoolGoing").prop("checked", false);
    //$("#chkVillageName").prop("checked", false);
    //$("#chkUnionName").prop("checked", false);
    //$("#chkHouseHold").prop("checked", false);
    //$("#chkChildrenNumber").prop("checked", false);
    //$("#chkSchoolName").prop("checked", false);
    //$("#chkMedicinePlace").prop("checked", false);
    //$("#chkMotherName").prop("checked", false);
    //$("#chkSelectAll").prop("checked", false);
 
    
});
