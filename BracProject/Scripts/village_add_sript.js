﻿$(document).ready(function() {

    divList("");
    function divList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_po/GetDivision",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if ($('#DIVISIONID').val() == data[i].Value) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#divisions').html(div_data);
            }
        });
    }

    $('#divisions').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_po/GetDistrict",
            data: { div_id: $('#divisions').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#districts').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#upozillas').html(empty_data);
            }
        });
    });

    $('#districts').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_po/GetUpozilla",
            data: { dis_id: $('#districts').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#upozillas').html(upz_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#unions').html(empty_data);
            }
        });
    });     


$('#upozillas').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_po/GetUnionListV",
            data: { dis_id: $('#upozillas').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#unions').html(upz_data);
            }
        });
    }); 

});

