﻿$(document).ready(function() {


    memberTypeList("");
    function memberTypeList(search_text) {
        $.ajax({
            type: "post",
            url: "/member_list/MemberTypeList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#m_type').html(div_data);
            }
        });
    }
});
