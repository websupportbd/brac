﻿$(document).ready(function() {

    $(".datecontrol").datepicker({ dateFormat: "dd-mm-yy" }).val();



    $.ajax({
        type: "get",
        url: "/contact/GetDivision",
        datatype: "json",
        traditional: true,
        success: function(data) {
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Division').html(div_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/contact/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Project').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/contact/GetComponent",
        datatype: "json",
        traditional: true,
        success: function(data) {
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Component').html(_data);
        }
    });

    $('#Division').change(function() {
        $.ajax({
            type: "post",
            url: "/contact/GetDistrict",
            data: { div_id: $('#Division').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#District').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#Upozilla').html(empty_data);
            }
        });
    });

    $('#District').change(function() {
        $.ajax({
            type: "post",
            url: "/contact/GetUpozilla",
            data: { dis_id: $('#District').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Upozilla').html(upz_data);
            }
        });
    });

    $.ajax({
        type: "get",
        url: "/contact/GetFinancialYear",
        datatype: "json",
        traditional: true,
        success: function(data) {
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#financial_years').html(div_data);
        }
    });

});