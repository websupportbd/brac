﻿$(document).ready(function() {
    $.ajax({
        type: "get",
        url: "/projecttem/GetComponent",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#COMPONENT_ID').val() == data[i].Value) {
                    _data = _data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#Component').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/projecttem/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#PROJECT_ID').val() == data[i].Value) {
                    _data = _data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#Project').html(_data);
        }
    });

    $('#IS_GENERIC').change(function() {
        var s_value = $("#IS_GENERIC").val();
        if (s_value == 0) {
            $("#project_list_").show();
            $("#is_default_").show();
        } else {
            $("#project_list_").hide();
            $("#is_default_").hide();
        }


    });

});