﻿$(document).ready(function() {



    poList("");
    function poList(search_text) {
        $.ajax({
            type: "post",
            url: "/school_assign/poList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#pos').html(div_data);
            }
        });
    }




    $.ajax({
        type: "post",
        url: "/school_assign/GetSchool",
        data: { po_id: $('#po_id').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var upz_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#school_id').val() == data[i].Value) {
                    upz_data = upz_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#schools').html(upz_data);
        }
    });

    $('#pos').change(function() {
        $.ajax({
            type: "post",
            url: "/school_assign/GetSchool",
            data: { po_id: $('#po_id').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#schools').html(upz_data);
            }
        });
    }); 
    


});
