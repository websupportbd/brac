﻿$(document).ready(function() {
    $('#userRoalList').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "pageLength": 40,
        "ajax": {
            "url": "/user_management/RoleList",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            
            { "data": "module_name" },
            { "data": "action" }
        ]
    });

    $("#userRoalList").on('click', '.user-roal-js', function() {
        var module_name = $(this).attr('id')
        $.ajax({
            type: "post",
            url: "/user_management/modifyuserrole",
            data: { module_name: module_name, user_id: $('#id').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;

            }
        });
    });

});