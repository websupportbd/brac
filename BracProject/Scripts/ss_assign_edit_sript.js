﻿
$(document).ready(function() {

    skList("");
    function skList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_ss/skList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#sks').html(div_data);
            }
        });
    }

    ssList("");
    function ssList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_ss/sslist",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#sss').html(div_data);
            }
        });
    }




    $.ajax({
        type: "post",
        url: "/assign_ss/GetVillage",
        data: { sk_id: $('#sk_id').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var upz_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#vill_id').val() == data[i].Value) {
                    upz_data = upz_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#villages').html(upz_data);
        }
    });

    $('#sks').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_ss/GetVillage",
            data: { sk_id: $('#sk_id').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#villages').html(upz_data);
            }
        });
    }); 
    


});
