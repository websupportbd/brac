﻿$(document).ready(function() {
    var table = $('#schemeReport').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/quick_report/List",
            "type": "POST",

            "datatype": "json"
        },
        "language": {
            "search": "Search by Scheme Name:"
        },
        "columns": [
      
            { "data": "user_name" },
            { "data": "scheme_name" },
            { "data": "date_" },
            { "data": "visit" },
            { "data": "action" }
        ]
    });

    function load (){
        table.draw();
    }

    var tid = setInterval(load, 9000);
   

});