﻿window.onload = function() {

    $(".datecontrol").datepicker({ dateFormat: "dd-mm-yy" }).val();

    $("#pie_chart").hide();
    $("#scheme_grap").hide();
    $("#bar_chart").hide();

    function grap_total_visit() {
        var dataPoints = [];
        var chart = new CanvasJS.Chart("totalVisit", {
            width: 940,
            animationEnabled: true,
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Total Visit (Date)"
            },
            axisY: {
                title: "Visit",
                logarithmic: true
            },
            data: [{
                type: "column",

                yValueFormatString: "#,##0.#",
                toolTipContent: "<b>{label}: {y}</b> visit",
                dataPoints: dataPoints
            }]
        });

        function addData(data) {
            for (var i = 0; i < data.length; i++) {
                dataPoints.push({
                    label: data[i].label,
                    y: data[i].y
                });
            }
            chart.render();
        }
        $.getJSON("/dashboard/totalVisit", addData);
    }

    function grap_total_scheme() {
        var dataPoints = [];
        var chart = new CanvasJS.Chart("schemeVisit", {
            width: 940,
            animationEnabled: true,
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Total Visit (Scheme-wise)"
            },
            axisY: {
                title: "Visit",

                includeZero: true
            },
            data: [{
                type: "column",

                yValueFormatString: "#,##0.#",
                toolTipContent: "<b>{label}: {y}</b> visit",
                dataPoints: dataPoints
            }]
        });

        function addData(data) {
            for (var i = 0; i < data.length; i++) {
                dataPoints.push({
                    label: data[i].label,
                    y: data[i].y
                });
            }
            chart.render();
        }
        $.getJSON("/dashboard/schemeVisit", addData);
    }

    $("#submit_s").click(function() {
        var from_date = "";
        var to_date = "";

        if ($('#from_date_s').val() != "") {
            var from_date = "from_date:" + $('#from_date_s').val() + ";";
        }
        if ($('#to_date_s').val() != "") {
            var to_date = "to_date:" + $('#to_date_s').val() + ";";
        }

        var all = from_date + to_date;

        $.ajax({
            type: "post",
            url: "/dashboard/schemeVisitSetAllIDs",
            data: { all_ids: all },
            datatype: "json",
            traditional: true,
            success: function(data) {
                grap_total_scheme();
            }
        });
    });



    grap_total_visit();
    grap_total_scheme();


    $.ajax({
        type: "get",
        url: "/report/GetDivision",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Division').html(div_data);
        }
    });

    $('#Division').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetDistrict",
            data: { div_id: $('#Division').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#District').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#Upozilla').html(empty_data);
            }
        });
    });

    $('#District').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetUpozilla",
            data: { dis_id: $('#District').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Upozilla').html(upz_data);
            }
        });
    });

    $.ajax({
        type: "get",
        url: "/report/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Project').html(_data);
        }
    });

    $('#Project').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetContact",
            data: { div: $('#Division').val(), dis: $('#District').val(), upz: $('#Upozilla').val(), proj: $('#Project').val(), comp: $('#Component').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Contact').html(upz_data);
            }
        });
    });

    $('#Contact').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetScheme",
            data: { contactID: $('#Contact').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Scheme').html(upz_data);
            }
        });
    });

    $('#Scheme').change(function() {
        $.ajax({
            type: "post",
            url: "/dashboard/SetSchemeID",
            data: { scheme_id: $('#Scheme').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var scheme_name = $('#Scheme option:selected').text();
                $('#scheme_name').val(scheme_name);
                grap_total_per_scheme();
            }
        });
    });


    $.ajax({
        type: "get",
        url: "/report/GetDivision",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Division1').html(div_data);
        }
    });

    $('#Division1').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetDistrict",
            data: { div_id: $('#Division1').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#District1').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#Upozilla1').html(empty_data);
            }
        });
    });

    $('#District1').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetUpozilla",
            data: { dis_id: $('#District1').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Upozilla1').html(upz_data);
            }
        });

        $.ajax({
            type: "post",
            url: "/report/GetContact",
            data: { div: $('#Division1').val(), dis: $('#District1').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Contact1').html(upz_data);
            }
        });
    });

    $('#Upozilla1').change(function() {


        $.ajax({
            type: "post",
            url: "/report/GetContact",
            data: { div: $('#Division1').val(), dis: $('#District1').val(), upz: $('#Upozilla1').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Contact1').html(upz_data);
            }
        });
    });

    $.ajax({
        type: "get",
        url: "/report/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Project1').html(_data);
        }
    });

    $('#Project1').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetContact",
            data: { div: $('#Division1').val(), dis: $('#District1').val(), upz: $('#Upozilla1').val(), proj: $('#Project1').val(), comp: $('#Component1').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Contact1').html(upz_data);
            }
        });

        $.ajax({
            type: "post",
            url: "/report/GetContact",
            data: { div: $('#Division1').val(), dis: $('#District1').val(), upz: $('#Upozilla1').val(), proj: $('#Project1').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Contact1').html(upz_data);
            }
        });

    });

    $('#Contact1').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetScheme",
            data: { contactID: $('#Contact1').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Scheme1').html(upz_data);
            }
        });
    });


    $("#submit").click(function() {
        var divID = "";
        var disID = "";
        var upzID = "";
        var projID = "";
        var cont = "";
        var sche = "";
        var from_date = "";
        var to_date = "";

        if ($('#Division1').val() != "") {
            var divID = "div:" + $('#Division1').val() + ";";
        }
        if ($('#District1').val() != "") {
            var disID = "dis:" + $('#District1').val() + ";";
        }
        if ($('#Upozilla1').val() != "") {
            var upzID = "upz:" + $('#Upozilla1').val() + ";";
        }
        if ($('#Project1').val() != "") {
            var projID = "pro:" + $('#Project1').val() + ";";
        }
        if ($('#Contact1').val() != "") {
            var cont = "cont:" + $('#Contact1').val() + ";";
        }
        if ($('#Scheme1').val() != "") {
            var sche = "scheme:" + $('#Scheme1').val() + ";";
        }
        if ($('#from_date1').val() != "") {
            var from_date = "from_date:" + $('#from_date1').val() + ";";
        }
        if ($('#to_date1').val() != "") {
            var to_date = "to_date:" + $('#to_date1').val() + ";";
        }

        var all = divID + disID + upzID + projID + cont + sche + from_date + to_date;

        $.ajax({
            type: "post",
            url: "/dashboard/SetAllIDs",
            data: { all_ids: all },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var scheme_name = $('#Scheme1 option:selected').text();
                $('#scheme_name1').val(scheme_name);
                grap_total_designation();
            }
        });
    });

    $.ajax({
        type: "get",
        url: "/dashboard/DefaulsScheme1ID",
        datatype: "json",
        traditional: true,
        success: function(data) {
            var scheme_name = data;
            $('#scheme_name1').val(scheme_name);
            grap_total_designation();
        }
    });

    $.ajax({
        type: "get",
        url: "/dashboard/DefaulsSchemeID",
        datatype: "json",
        traditional: true,
        success: function(data) {
            var scheme_name = data;
            $('#scheme_name').val(scheme_name);
            grap_total_per_scheme();
        }
    });

    function grap_total_designation() {
        var dataPoints = [];
        var scheme_name = $('#scheme_name1').val();

        var chart = new CanvasJS.Chart("desinationVisit", {
             width: 940,
            animationEnabled: true,
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Total Visits (Designation-wise)"
            },

            data: [{
                type: "pie",
                startAngle: 240,
                yValueFormatString: "(#,##0 visit,",
                zValueFormatString: "##0.00\"%\")",
                indexLabel: "{label} {y} {z}",
                dataPoints: dataPoints
            }]
        });

        function addData(data) {
            for (var i = 0; i < data.length; i++) {
                dataPoints.push({
                    label: data[i].label,
                    y: data[i].y,
                    z: data[i].z
                });
            }
            chart.render();
        }
        $.getJSON("/dashboard/designationVisit", addData);
    }

    function grap_total_per_scheme() {
        var dataPoints = [];
        var scheme_name = $('#scheme_name').val();
        var chart = new CanvasJS.Chart("per_schemeVisit", {
             width: 940,
            animationEnabled: true,
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Scheme Visit (Date-wise)"
            },
            subtitles: [{
                text: scheme_name,
                fontSize: 12
            }],
            axisY: {
                title: "Visit",
                includeZero: true
            },
            data: [{
                type: "column",

                yValueFormatString: "#,##0.#",
                toolTipContent: "<b>{label}: {y}</b> visit",
                dataPoints: dataPoints
            }]
        });

        function addData(data) {
            for (var i = 0; i < data.length; i++) {
                dataPoints.push({
                    label: data[i].label,
                    y: data[i].y
                });
            }
            chart.render();
        }
        $.getJSON("/dashboard/perSchemeVisit", addData);
    }

    $("#p_chart").click(function() {
        $("#pie_chart").show();
        $("#scheme_grap").hide();
        $("#bar_chart").hide();


        $("#p_chart_image_id").addClass("p_chart_image_hover");

        $("#b_chart_02_image_id").removeClass("b_chart_02_image_hover");
        $("#b_chart_01_image_id").removeClass("b_chart_01_image_hover");

    });
    $("#b_chart_01").click(function() {
        $("#pie_chart").hide();
        $("#scheme_grap").show();
        $("#bar_chart").hide();


        $("#p_chart_image_id").removeClass("p_chart_image_hover");
        $("#b_chart_02_image_id").removeClass("b_chart_02_image_hover");

        $("#b_chart_01_image_id").addClass("b_chart_01_image_hover");


    });
    $("#b_chart_02").click(function() {
        $("#pie_chart").hide();
        $("#scheme_grap").hide();
        $("#bar_chart").show();

        $("#b_chart_02_image_id").addClass("b_chart_02_image_hover");

        $("#p_chart_image_id").removeClass("p_chart_image_hover");
        $("#b_chart_01_image_id").removeClass("b_chart_01_image_hover");


    });

}