﻿


$(document).ready(function() {
    $('#_list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/assign_ss/ListById",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "sk_name" },
            { "data": "ss_name" },
            { "data": "village_name" },
            { "data": "action" }
        ],
        "autoWidth": false
    });

    $("#_list").on('click', '#del', function() {
        var att_id = $(this).attr('val');
        deleteAtt(att_id);
    });

    function deleteAtt(att_id) {
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                    url: "/assign_ss/Del/" + att_id,
                    type: "DELETE"
                })
                .done(function(data) {
                    swal("Deleted!", "Your file was successfully deleted!", "success");
                    location.reload();
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }

});
