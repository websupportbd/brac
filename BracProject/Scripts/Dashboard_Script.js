﻿

$(document).ready(function () {

    getAllChldList("");
    function getAllChldList(search_text) {
        $.ajax({
            type: "post",
            url: "/dashboard/GetTtoalChildren",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function (data) {
                if (data != null) {
                    $('#lblTotalChildren').html(data.children);                   
                    $('#chtotal').html(data.children);
                    $('#lblSchoolGoingChildren').html(data.TotalSchoolGoingChildren);
                    $('#lblOutOfSchoolGoingChildren').html(data.NotSchoolGoingChildren);
                    $('#lblTotalDeworming').html(data.TotalDewormingChild);
                    $('#lblTotalNotDeworming').html(data.TotalNotDewormingChild);
                    $('#lblBoys').html(data.Boys);
                    $('#lblGirls').html(data.Girls);

                    $('#lblDewormingSchoolChildren').html(data.total_school_children_deworming);
                    $('#lblTotalNotDewormingSchoolChildren').html(data.total_outofschool_children_deworming);

                    $('#lblTotalhousehold').html(data.total_household);
                    $('#lblmonthlyTarget').html(data.monthly_target);
                    $('#lblmonthlyTargetDue').html(data.monthly_target);

                    $('#lblnewchild').html(data.new_child);
                    $('#lblmiagratedchild').html(data.miagrate_child);
                    $('#lblgraduatechild').html(data.graduateChild);
                    $('#lbllostfollowupchild').html(data.Lost_to_follow_up);
                    ///chart section--------------------------------------------------------------------------------

                    //chart1
                   // $('#lblTotalChildDeworming').val(data.TotalDewormingChild);
                   // $('#lblTotalChildNotDeworming').val(data.TotalNotDewormingChild);
                    //chart2
                   // $('#lblDewormingSchoolGoing').val(data.TotalDewormingChild);
                   // $('#lblDewormingOutofSchool').val(data.TotalNotDewormingChild);
                   // $('#lblNotDewormingSchoolGoing').val(data.TotalDewormingChild);
                   // $('#lblNotDewormingOutOfSchool').val(data.TotalNotDewormingChild);


                    //chart3
                    $('#lblSchoolGoing').val(data.TotalSchoolGoingChildren);
                    $('#lblOutofSchool').val(data.NotSchoolGoingChildren);



                    //chart4
                    $('#lblSchoolGoingGirl').val(data.school_going_girls);
                    $('#lblOutofSchoolGirl').val(data.outof_School_GoingGirls);
                    $('#lblSchoolGoingBoy').val(data.school_going_boys);
                    $('#lblOutOfSchoolBoy').val(data.outof_School_GoingBoys);


                    $('#lblapril2018').html(data.april_2018_Data_school);
                    $('#lbloctober2018').html(data.october_2018_Data_school);
                    $('#lblapril2019').html(data.april_2019_Data_school);
                    $('#lbloctober2019').html(data.october_2019_Data_school);



                    $('#lblapril2018outofchildren').html(data.april_2018_outof_school);
                    $('#lbloctober2018outofchildren').html(data.october_2018_outof_school);
                    $('#lblapril2019outofchildren').html(data.april_2019_outof_school);
                    $('#lbloctober2019outofchildren').html(data.october_2019_outof_school);



                }
            }
        });
    }

});