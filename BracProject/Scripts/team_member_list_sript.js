﻿

$(document).ready(function() {
    $('#_list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/team_mang/MemberList",
            "type": "POST",
            "datatype": "json"
        },
        "language": {
    		"search": "Search by Name:"
  		},
        "columns": [
            { "data": "title" },
            { "data": "designation" },
            { "data": "project_name" },
            { "data": "sso_login_id" },
            { "data": "convenor" },
            { "data": "action" }
        ],
        "autoWidth": false
    });
});