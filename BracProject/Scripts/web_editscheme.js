﻿$(document).ready(function() {


    $.ajax({
        type: "get",
        url: "/scheme/GetComponent",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#COMPONENT_ID').val() == data[i].Value) {
                    _data = _data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#Component').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/scheme/GetUpozilla",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var upz_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#UP_ID').val() == data[i].Value) {
                    upz_data = upz_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }else{
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#Upozilla').html(upz_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/contact/GetUnit",
        datatype: "json",
        traditional: true,
        success: function(data) {
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#UNIT').val() == data[i].Value) {
                    _data = _data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }else{
                    _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }

            }
            $('#Unit').html(_data);
        }
    });

    $.ajax({
        type: "post",
        url: "/scheme/GetTempList",
        data: { comp: $('#COMPONENT_ID').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $(".dualList").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $(".dualList").append(option);
                }
            }
            $(".dualList").bootstrapDualListbox('setRemoveAllLabel', true);
            $(".dualList").bootstrapDualListbox('refresh', true);
        }
    });


    $.ajax({
        type: "post",
        url: "/contact/GetPaurashava",
        data: { upz_id: $('#UP_ID').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var dis_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#PAURASHAVA_ID').val() == data[i].Value) {
                    dis_data = dis_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }else{
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }

            }
            $('#Paurashava').html(dis_data);
        }
    });


    $('#Component').change(function() {
        $.ajax({
            type: "post",
            url: "/scheme/GetTemp",
            data: { comp: $('#Component').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
            $(".dualList").bootstrapDualListbox('setRemoveAllLabel', true);
            $(".dualList").html('');
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $(".dualList").append(option);
                    }
                }

                $(".dualList").bootstrapDualListbox('refresh', true);
            }
        });
    });

    $('#Upozilla').change(function() {
        $.ajax({
            type: "post",
            url: "/contact/GetPaurashava",
            data: { upz_id: $('#Upozilla').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Paurashava').html(dis_data);
            }
        });
    });

    var demo2 = $('.dualList').bootstrapDualListbox({
                nonSelectedListLabel: 'Inspection Template',
                selectedListLabel: 'Selected Template',
                preserveSelectionOnMove: 'moved',
                moveOnSelect: false
            });

    $("label[for*='bootstrap-duallistbox-nonselected-list_dualList']").html("Inspection Template");
    $("label[for*='bootstrap-duallistbox-selected-list_dualList']").html("Selected Template");


    $("#submit").click(function() {
        var ids = $(".dualList").val();
        $("#TEMPLATE_IDS").val(ids);
    });

});