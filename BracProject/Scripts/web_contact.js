﻿$(document).ready(function() {

    $(".datecontrol").datepicker({ dateFormat: "dd-mm-yy" }).val();

    var table =  $('#contactlist').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/contact/List",
            "type": "POST",
            "datatype": "json"
            },
        "language": {
            "search": "Search by Package ID:"
          },
        "order": [[1,"asc"]],
        "ordering": true,
        "columns": [
            { "data": "project_name" },
            { "data": "div_name" },
            { "data": "dis_name" },
            { "data": "contact_name" },
            { "data": "action" }
        ]
    });



    $.ajax({
        type: "get",
        url: "/report/GetDivision",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Division').html(div_data);
        }
    });

    $('#Division').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetDistrict",
            data: { div_id: $('#Division').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#District').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#Upozilla').html(empty_data);
            }
        });
    });

    $('#District').change(function() {
        $.ajax({
            type: "post",
            url: "/report/GetUpozilla",
            data: { dis_id: $('#District').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Upozilla').html(upz_data);
            }
        });
    });

    $.ajax({
        type: "get",
        url: "/report/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Project').html(_data);
        }
    });

   


    $("#submit").click(function() {


        var divID = "";
        var disID = "";
        var upzID = "";
        var projID = "";
 
        var from_date = "";
        var to_date = "";

        if ( $('#Division').val() != ""){
            var divID = "div:" + $('#Division').val() + ";";
        }
        if ( $('#District').val() != ""){
            var disID = "dis:" + $('#District').val() + ";";
        }
        /*if ( $('#Upozilla').val() != ""){
            var upzID = "upz:" + $('#Upozilla').val() + ";";
        }*/
        if ( $('#Project').val() != ""){
            var projID = "pro:" + $('#Project').val() + ";";
        }
   
        /*if ($('#from_date').val() != "") {
            var from_date = "from_date:" + $('#from_date').val() + ";";
        }
        if ($('#to_date').val() != "") {
            var to_date = "to_date:" + $('#to_date').val() + ";";
        }*/

        var all = divID + disID + upzID + projID + from_date + to_date;
        table.search(all).draw();
    });

    $("#contactlist").on('click', '#del', function() {
        var _id = $(this).attr('val');
        deleteCat(_id);
    });

    function deleteCat(cat_id) {
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                    url: "/contact/Del/" + cat_id,
                    type: "DELETE"
                })
                .done(function(data) {
                    swal("Deleted!", "Successfully deleted!", "success");
                    location.reload();
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }

});