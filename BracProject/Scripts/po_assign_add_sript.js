﻿$(document).ready(function() {

    poList("");
    function poList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_po/poList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#pos').html(div_data);
            }
        });
    }

    divList("");
    function divList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_po/GetDivision",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#divisions').html(div_data);
            }
        });
    }

    $('#divisions').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_po/GetDistrict",
            data: { div_id: $('#divisions').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#districts').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#upazilas').html(empty_data);
            }
        });
    });

    $('#districts').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_po/GetUpozilla",
            data: { dis_id: $('#districts').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                $("#dualList1").bootstrapDualListbox('setRemoveAllLabel', true);
                $("#dualList1").html("");
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $("#dualList1").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $("#dualList1").append(option);
                    }
                }
                $("#dualList1").bootstrapDualListbox('refresh', true);
               // getUnions();
            }
        });
    });

    $('#dualList1').on('change', function () {

        if ($("#rdbUnion").is(':checked')==true)
        {
            union();
        }
        else if ($("#rdbWard").is(':checked')==true)
        {
            ward();
        }
        else
        {
            alert('Please select Union/Ward');
            
        }
       
       

    });

    $('#dualList2').on('change', function () {

        if ($("#rdbUnion").is(':checked') == true) {
            villege();
        }
        else if ($("#rdbWard").is(':checked') == true) {
            para();
        }
        else {
            alert('Please select Union/Ward');

           
        }
       
    });
    
    var demo2 = $('#dualList1').bootstrapDualListbox({
        nonSelectedListLabel: 'Upazila List',
        selectedListLabel: 'Selected Upazila',
        preserveSelectionOnMove: 'moved',
        moveOnSelect: false
    });

    function union() {
        var upozela_ids = [];
        var ids = $("#dualList1").val();
        var objs = new Object();
        objs.upozela_id = ids
        upozela_ids.push(objs);
        var obj = JSON.stringify(upozela_ids).replace(/&/g, "^");
        var jsonParam = 'obj:' + obj;
        var url = "/assign_po/GetUnion/";
        AjaxManager.SendJson2(url, jsonParam, onSuccess, onFailed)
        function onSuccess(data) {
            $("#dualList2").bootstrapDualListbox('setRemoveAllLabel', true);
            $("#dualList2").html("");
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList2").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList2").append(option);
                }
            }
            $("#dualList2").bootstrapDualListbox('refresh', true);
        }
        function onFailed(error) {
            AjaxManager.MsgBox('error', 'center', 'Error', error.statusText,
                [{
                    addClass: 'btn btn-primary',
                    text: 'Ok',
                    onClick: function ($noty) {
                        $noty.close();
                    }
                }]);
        }
    }

    function ward() {
        var upozela_ids = [];
        var ids = $("#dualList1").val();
        var objs = new Object();
        objs.upozela_id = ids
        upozela_ids.push(objs);
        var obj = JSON.stringify(upozela_ids).replace(/&/g, "^");
        var jsonParam = 'obj:' + obj;
        var url = "/assign_po/GetWard/";
        AjaxManager.SendJson2(url, jsonParam, onSuccess, onFailed)
        function onSuccess(data) {
            $("#dualList2").bootstrapDualListbox('setRemoveAllLabel', true);
            $("#dualList2").html("");
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList2").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList2").append(option);
                }
            }
            $("#dualList2").bootstrapDualListbox('refresh', true);
        }
        function onFailed(error) {
            AjaxManager.MsgBox('error', 'center', 'Error', error.statusText,
                [{
                    addClass: 'btn btn-primary',
                    text: 'Ok',
                    onClick: function ($noty) {
                        $noty.close();
                    }
                }]);
        }
    }

    function villege()
    {
        var union_ids = [];
        var ids = $("#dualList2").val();
        var objs = new Object();
        objs.union_id = ids
        union_ids.push(objs);
        var obj = JSON.stringify(union_ids).replace(/&/g, "^");
        var jsonParam = 'obj:' + obj;
        var url = "/assign_po/GetVillage/";
        AjaxManager.SendJson2(url, jsonParam, onSuccess, onFailed)
        function onSuccess(data) {
            $("#dualList3").bootstrapDualListbox('setRemoveAllLabel', true);
            $("#dualList3").html("");
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList3").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList3").append(option);
                }
            }
            $("#dualList3").bootstrapDualListbox('refresh', true);
        }
        function onFailed(error) {
            AjaxManager.MsgBox('error', 'center', 'Error', error.statusText,
                [{
                    addClass: 'btn btn-primary',
                    text: 'Ok',
                    onClick: function ($noty) {
                        $noty.close();
                    }
                }]);
        }

    }

    function para() {
        var ward_ids = [];
        var ids = $("#dualList2").val();
        var objs = new Object();
        objs.ward_id = ids
        ward_ids.push(objs);
        var obj = JSON.stringify(ward_ids).replace(/&/g, "^");
        var jsonParam = 'obj:' + obj;
        var url = "/assign_po/GetPara/";
        AjaxManager.SendJson2(url, jsonParam, onSuccess, onFailed)
        function onSuccess(data) {
            $("#dualList3").bootstrapDualListbox('setRemoveAllLabel', true);
            $("#dualList3").html("");
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList3").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList3").append(option);
                }
            }
            $("#dualList3").bootstrapDualListbox('refresh', true);
        }
        function onFailed(error) {
            AjaxManager.MsgBox('error', 'center', 'Error', error.statusText,
                [{
                    addClass: 'btn btn-primary',
                    text: 'Ok',
                    onClick: function ($noty) {
                        $noty.close();
                    }
                }]);
        }

    }

    //$("label[for*='bootstrap-duallistbox-nonselected-list_dualList1']").html("Upazila List");
    //$("label[for*='bootstrap-duallistbox-selected-list_dualList1']").html("Selected Upazila");


    $("#submit").click(function() {
        var ids1 = $("#dualList1").val();
        var ids2 = $("#dualList2").val();
        var ids3 = $("#dualList3").val();
        $("#uz_ids").val(ids1);
        if ($("#rdbUnion").is(':checked') == true) {
            $("#ui_ids").val(ids2);
            $("#vi_ids").val(ids3);
        }
        else if ($("#rdbWard").is(':checked') == true) {
            $("#wa_ids").val(ids2);
            $("#pa_ids").val(ids3);
        }
       
    });

    var AjaxManager = {

        SendJson2: function (serviceUrl, jsonParams, successCallback, errorCallback) {
            jQuery.ajax({
                url: serviceUrl,
                async: false,
                type: "POST",
                data: "{" + jsonParams + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: successCallback,
                error: errorCallback
            });
        },
        MsgBox: function (messageBoxType, displayPosition, messageBoxHeaderText, messageText, buttonsArray) {
            var n = noty({
                textHeader: messageBoxHeaderText,
                text: messageText,
                type: messageBoxType,
                modal: true,
                dismissQueue: true,
                layout: displayPosition,
                theme: 'defaultTheme',
                buttons: buttonsArray               
            });
            $(".btn-primary").focus();
          
        },


    };


    $("#rdbUnion").on('change', function () {
        union();
        villege();
        $("#lblUnion").html( "Union");
        $("#lblVillage").html("Villege");

    });
    $("#rdbWard").on('change', function () {
        ward();
        para();
        $("#lblUnion").html("Ward");
        $("#lblVillage").html("Para");

    });

      

});

