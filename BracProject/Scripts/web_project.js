﻿
$(document).ready(function() {
    $('#list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/project/List",
            "type": "POST",
            "datatype": "json"
            },
        "order": [[1,"asc"]],
        "ordering": true,
        "language": {
            "search": "Search by Short Name:"
          },
        "columns": [
            { "data": "code1" },
            { "data": "project_name" },
            { "data": "project_title" },
            { "data": "action" }
        ]
    });
});