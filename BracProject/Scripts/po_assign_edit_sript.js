﻿$(document).ready(function() {



    $.ajax({
        type: "post",
        url: "/assign_po/GetUpazilaList",
        data: { dis: $('#dis_id').val(), },
        datatype: "json",
        traditional: true,
        success: function(data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList1").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList1").append(option);
                }
            }
            $("#dualList1").bootstrapDualListbox('setRemoveAllLabel', true);
            $("#dualList1").bootstrapDualListbox('refresh', true);
        }
    });


    $.ajax({
        type: "post",
        url: "/assign_po/GetUnionList",
        data: { dis: $('#dis_id').val(), },
        datatype: "json",
        traditional: true,
        success: function(data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList2").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList2").append(option);
                }
            }
            $("#dualList2").bootstrapDualListbox('setRemoveAllLabel', true);
            $("#dualList2").bootstrapDualListbox('refresh', true);
        }
    });


    $.ajax({
        type: "post",
        url: "/assign_po/GetVillageList",
        data: { dis: $('#dis_id').val(), },
        datatype: "json",
        traditional: true,
        success: function(data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Selected) {
                    var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList3").append(option);
                } else {
                    var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    $("#dualList3").append(option);
                }
            }
            $("#dualList3").bootstrapDualListbox('setRemoveAllLabel', true);
            $("#dualList3").bootstrapDualListbox('refresh', true);
        }
    });

    poList("");
    function poList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_po/poList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#pos').html(div_data);
            }
        });
    }

    divList("");
    function divList(search_text) {
        $.ajax({
            type: "post",
            url: "/assign_po/GetDivision",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if ($('#div_id').val() == data[i].Value) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#divisions').html(div_data);
            }
        });
    }

    $.ajax({
        type: "post",
        url: "/assign_po/GetDistrict",
        data: { div_id: $('#div_id').val() },
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var dis_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                if ($('#dis_id').val() == data[i].Value) {
                    dis_data = dis_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                } else {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
            }
            $('#districts').html(dis_data);
        }
    });


    $('#divisions').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_po/GetDistrict",
            data: { div_id: $('#divisions').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#districts').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#upazilas').html(empty_data);
            }
        });
    });

    $('#districts').change(function() {
        $.ajax({
            type: "post",
            url: "/assign_po/GetUpozilla",
            data: { dis_id: $('#districts').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                $("#dualList1").bootstrapDualListbox('setRemoveAllLabel', true);
                $("#dualList1").html("");
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $("#dualList1").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $("#dualList1").append(option);
                    }
                }
                $("#dualList1").bootstrapDualListbox('refresh', true);
                getUnions();
            }
        });
    }); 
    

    function getUnions() {
        $.ajax({
            type: "post",
            url: "/assign_po/GetUnion",
            data: { dis_id: $('#districts').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                $("#dualList2").bootstrapDualListbox('setRemoveAllLabel', true);
                $("#dualList2").html("");
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $("#dualList2").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $("#dualList2").append(option);
                    }
                }
                $("#dualList2").bootstrapDualListbox('refresh', true);
                getVillage();
            }
        });
    }


    function getVillage() {
        $.ajax({
            type: "post",
            url: "/assign_po/GetVillage",
            data: { dis_id: $('#districts').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                $("#dualList3").bootstrapDualListbox('setRemoveAllLabel', true);
                $("#dualList3").html("");
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        var option = '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $("#dualList3").append(option);
                    } else {
                        var option = '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                        $("#dualList3").append(option);
                    }
                }
                $("#dualList3").bootstrapDualListbox('refresh', true);
            }
        });
    }


    


});
