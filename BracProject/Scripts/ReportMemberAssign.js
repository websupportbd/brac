﻿
$(document).ready(function() {

    memberTypeList("");
    function memberTypeList(search_text) {
        $.ajax({
            type: "post",
            url: "/member_list/MemberTypeList",
            data: { searchText: search_text },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var div_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Selected) {
                        div_data = div_data + '<option selected=true value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    } else {
                        div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                    }
                }
                $('#Type').html(div_data);
            }
        });
    }


    $('#Type').change(function() {
        $.ajax({
            type: "post",
            url: "/report_1/GetMember",
            data: { _id: $('#Type').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Member').html(dis_data);
            }
        });
    });




    var table = $('#_list').DataTable({
        "processing": true,
        "serverSide": true,
        "search": true,
        "ajax": {
            "url": "/report_1/List",
            "type": "POST",
            "datatype": "json"
        },
        "language": {
            "search": "Search:"
        },
        "columns": [
            { "data": "Member" },
            { "data": "Assign" }
        ]
    });


    $("#submit").click(function() {
        var type = "";
        var member = "";

        if ( $('#Type').val() != ""){
            var type = "ty:" + $('#Type').val() + ";";
        }
        if ( $('#Member').val() != ""){
            var member = "me:" + $('#Member').val() + ";";
        }

        var all = type + member;
        table.search(all).draw();
    }); 

});
