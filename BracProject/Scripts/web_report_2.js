﻿$(document).ready(function() {

    $.ajax({
        type: "get",
        url: "/report_user/GetDivision",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var div_data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                div_data = div_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Division').html(div_data);
        }
    });

    $('#Division').change(function() {
        $.ajax({
            type: "post",
            url: "/report_user/GetDistrict",
            data: { div_id: $('#Division').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var dis_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    dis_data = dis_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#District').html(dis_data);
                var empty_data = '<option value="">--Select--</option>';
                $('#Upozilla').html(empty_data);
            }
        });
    });

    $('#District').change(function() {
        $.ajax({
            type: "post",
            url: "/report_user/GetUpozilla",
            data: { dis_id: $('#District').val() },
            datatype: "json",
            traditional: true,
            success: function(data) {;
                var upz_data = '<option value="">--Select--</option>';
                for (var i = 0; i < data.length; i++) {
                    upz_data = upz_data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
                }
                $('#Upozilla').html(upz_data);
            }
        });
    });

    $.ajax({
        type: "get",
        url: "/report_user/GetProject",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Project').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/report_user/GetOffice",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Office').html(_data);
        }
    });

    $.ajax({
        type: "get",
        url: "/report_user/GetDesignation",
        datatype: "json",
        traditional: true,
        success: function(data) {;
            var _data = '<option value="">--Select--</option>';
            for (var i = 0; i < data.length; i++) {
                _data = _data + '<option value=' + data[i].Value + '>' + data[i].Text + '</option>';
            }
            $('#Designation').html(_data);
        }
    });



    $("#submit").click(function() {
        $('#userReport').DataTable({
            "processing": true,
            "serverSide": true,
            "search": true,
            "ajax": {
                "url": "/report_user/List",
                "type": "POST",
                "data": {
                    divID: $('#Division').val(),
                    disID: $('#District').val(),
                    upzID: $('#Upozilla').val(),
                    projID: $('#Project').val(),
                    officeID: $('#Office').val(),
                    designID: $('#Designation').val()
                },
                "datatype": "json"
            },
            "columns": [
                
                { "data": "user_name" },
                { "data": "scheme_name" },
                { "data": "date_" },
                { "data": "visit" },
                { "data": "action" }
            ]
        });
    });

});